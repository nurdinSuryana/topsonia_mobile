import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:topsonia_mobile/models/error_model.dart';
import 'package:topsonia_mobile/theme.dart';

class AddKeranjangService {
  Future<ErrorModel> addkeranjangs({
    required String token,
    required String idProduk,
    required String qty,
    required String apikey,
    required String idKonsumen,
  }) async {
    var url = '$urlApi/keranjang/add';
    var headers = {
      'Content-Type': 'application/json',
      'Topsonia-X-Key': apikey
    };
    var body = jsonEncode({
      'token': token,
      'produk': idProduk,
      'qty': qty,
      'id_konsumen': idKonsumen
    });

    var response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: body,
    );

    var data = jsonDecode(response.body);
    if (response.statusCode == 200) {
      ErrorModel error = ErrorModel.fromJson(data);
      return error;
    } else {
      ErrorModel error = ErrorModel.error();
      return error;
    }
  }
}
