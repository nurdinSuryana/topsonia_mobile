import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:topsonia_mobile/models/detail_keranjang_model.dart';
import 'package:topsonia_mobile/theme.dart';

class DetailKeranjangService {
  Future<List<DetailKeranjangModel>> getDetail({
    required String idKonsumen,
    required String apikey,
    required String token,
    String? kodeTrx,
    String? idp,
  }) async {
    var url = '$urlApi/keranjang/get_keranjang';
    var headers = {
      'Content-Type': 'application/json',
      'Topsonia-X-Key': apikey
    };
    var body = jsonEncode({
      'token': token,
      'id_konsumen': idKonsumen,
      'kode_transaksi': kodeTrx,
      'id_penjualan': idp,
    });

    var response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: body,
    );
    //print(response.body);

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['produk'];
      List<DetailKeranjangModel> products = [];
      // print(jsonDecode(response.body));
      for (var item in data) {
        products.add(DetailKeranjangModel.fromJson(item));
      }
      return products;
    } else {
      throw Exception('Gagal');
    }
  }
}
