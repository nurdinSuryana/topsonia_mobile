import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:topsonia_mobile/models/orders_report_model.dart';
import 'package:topsonia_mobile/theme.dart';

class OrdersReportService {
  Future<List<OrdersReportModel>> getDetail({
    required String idKonsumen,
    required String apikey,
    required String token,
    String? kondisi,
    String? kodeTrx,
    String? start,
    String? end,
  }) async {
    var url = '$urlApi/checkout/orders_report';
    var headers = {
      'Content-Type': 'application/json',
      'Topsonia-X-Key': apikey
    };
    var body = jsonEncode({
      'token': token,
      'id_konsumen': idKonsumen,
      'kondisi': kondisi,
      'kode_transaksi': kodeTrx,
      'start': start,
      'end': end,
    });
    var response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: body,
    );

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['order'];
      List<OrdersReportModel> ordersReport = [];
      // print(jsonDecode(response.body));
      for (var item in data) {
        ordersReport.add(OrdersReportModel.fromJson(item));
      }
      return ordersReport;
    } else {
      throw Exception('Gagal');
    }
  }
}
