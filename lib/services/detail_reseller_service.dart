import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:topsonia_mobile/models/detail_reseller_model.dart';
import 'package:topsonia_mobile/theme.dart';

class DetailResellerService {
  Future<List<DetailResellerModel>> getDetail({
    required String idKonsumen,
    required String apikey,
    required String token,
  }) async {
    var url = '$urlApi/keranjang/reseller';
    var headers = {
      'Content-Type': 'application/json',
      'Topsonia-X-Key': apikey
    };
    var body = jsonEncode({'token': token, 'id_konsumen': idKonsumen});
    var response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: body,
    );

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['reseller'];
      List<DetailResellerModel> detailReseller = [];
      // print(jsonDecode(response.body));
      for (var item in data) {
        detailReseller.add(DetailResellerModel.fromJson(item));
      }
      return detailReseller;
    } else {
      throw Exception('Gagal');
    }
  }
}
