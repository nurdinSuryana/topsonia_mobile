import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:topsonia_mobile/models/category_product_model.dart';
import 'package:topsonia_mobile/theme.dart';

class CategoryProductService {
  Future<List<CategoryProductModel>> getCategory({
    required String apikey,
  }) async {
    var url = '$urlApi/product/category';
    var headers = {
      'Content-Type': 'application/json',
      'Topsonia-X-Key': apikey
    };

    var response = await http.get(
      Uri.parse(url),
      headers: headers,
    );
    // print(response.body);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['category'];
      List<CategoryProductModel> products = [];
      // ignore: avoid_print
      products.add(CategoryProductModel.all());
      for (var item in data) {
        products.add(CategoryProductModel.fromJson(item));
      }
      return products;
    } else {
      throw Exception('Gagal Register');
    }
  }
}
