import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:topsonia_mobile/pages/cart_page.dart';
import 'package:topsonia_mobile/pages/checkout_page.dart';
import 'package:topsonia_mobile/pages/checkout_success_page.dart';
import 'package:topsonia_mobile/pages/detail_chat_page.dart';
import 'package:topsonia_mobile/pages/edit_profile.dart';
import 'package:topsonia_mobile/pages/home/main_page.dart';
import 'package:topsonia_mobile/pages/home/updateApp_page.dart';
import 'package:topsonia_mobile/pages/orders_report_page.dart';
import 'package:topsonia_mobile/pages/sign_in_page.dart';
import 'package:topsonia_mobile/pages/sign_up_page.dart';
import 'package:topsonia_mobile/pages/splash_page.dart';
import 'package:topsonia_mobile/provider/add_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/alamat_penerima_provider.dart';
import 'package:topsonia_mobile/provider/auth_provider.dart';
import 'package:topsonia_mobile/provider/category_product_provider.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/detail_reseller_provider.dart';
import 'package:topsonia_mobile/provider/intruksi_payment_provider.dart';
import 'package:topsonia_mobile/provider/kurirdata_provider.dart';
import 'package:topsonia_mobile/provider/orders_report_provider.dart';
import 'package:topsonia_mobile/provider/payment_provider.dart';
import 'package:topsonia_mobile/provider/pmb_manual_transfer_provider.dart';
import 'package:topsonia_mobile/provider/product_provider.dart';
import 'package:topsonia_mobile/provider/proses_checkout_provider.dart';
import 'package:topsonia_mobile/provider/provinsi_provider.dart';
import 'package:topsonia_mobile/provider/register_provider.dart';
import 'package:topsonia_mobile/provider/remove_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/update_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/wishlist_provider.dart';
import 'package:topsonia_mobile/theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => AuthProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => ProductProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => DetailKeranjangProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => AddKeranjangProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => RemoveKeranjangProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => UpdateKeranjangProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => CategoryProductProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => DetailResellerProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => AlamatPenerimaProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => KurirDataProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => PaymentProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => ProsesCheckoutProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => OrdersReportProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => IntruksiPaymentProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => PmbManualTransferProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => ProvinsiProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => RegisterProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => WishlistProvider(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        routes: {
          '/': (context) => const SplashPage(),
          '/sign-in': (context) => const SignInPage(),
          '/sign-up': (context) => const SignUpPage(),
          '/update-Page': (context) => const UpdateAppPage(),
          '/home': (context) => const MainPage(),
          '/detail-chat': (context) => const DetailChatPage(),
          '/edit-profile': (context) => const EditprofilPage(),
          '/cart': (context) => const CartPage(),
          '/checkout': (context) => const CheckoutPage(),
          '/checkout-success': (context) => const CheckoutSuccesPage(),
          '/orders-report': (context) => const OrdersReportPage(),
        },
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Text(
        "Hellow World",
        style: secondaryTextStyle.copyWith(fontSize: 30),
      ),
    ));
  }
}
