import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/global_model.dart';
import 'package:topsonia_mobile/models/orders_report_model.dart';
import 'package:topsonia_mobile/pages/rating_product_page.dart';
import 'package:topsonia_mobile/pages/tracking_page.dart';
import 'package:topsonia_mobile/provider/alamat_penerima_provider.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:topsonia_mobile/widgets/detaill_transaksi_produk.dart';
import 'package:http/http.dart' as http;

class DetailTransaksiPage extends StatefulWidget {
  final OrdersReportModel orders;
  final String kondisi;
  final String idp;
  const DetailTransaksiPage(this.orders, this.kondisi, this.idp, {Key? key})
      : super(key: key);
  @override
  State<DetailTransaksiPage> createState() => _DetailTransaksiPageState();
}

class _DetailTransaksiPageState extends State<DetailTransaksiPage> {
  bool isLoading2 = false;
  bool isLoadingb = false;
  @override
  Widget build(BuildContext context) {
    AlamatPenerimaProvider detailAlamat =
        Provider.of<AlamatPenerimaProvider>(context);
    final keranjangProvider =
        Provider.of<DetailKeranjangProvider>(context).detailKeranjang;

    PreferredSizeWidget header() {
      return AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.pop(context),
        ),
        backgroundColor: backgroundColor1,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Detail Transaksi',
          style: primaryTextStyle.copyWith(
            fontSize: 12.sp,
          ),
        ),
      );
    }

    void terimaBarang(BuildContext context) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(
            'Keluar ?',
            style: TextStyle(fontSize: 18.sp),
          ),
          content: Text(
            'Apakah Barang Ini Sudah Anda Terima',
            style: TextStyle(fontSize: 16.sp),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text(
                'Tidak',
                style: TextStyle(fontSize: 14.sp),
              ),
            ),
            TextButton(
              onPressed: () async {
                var url = '$urlApi/tracking/terimaPesanan';
                var headers = {
                  'Content-Type': 'application/json',
                  'Topsonia-X-Key': keyApi
                };
                var body = jsonEncode({
                  'token': '',
                  'id_penjualan': widget.idp.toString(),
                });
                var response = await http.post(
                  Uri.parse(url),
                  headers: headers,
                  body: body,
                );
                if (response.statusCode == 200) {
                  Navigator.pushNamed(context, '/orders-report');
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      backgroundColor: secondaryColor,
                      content: Text(
                        'Gagal Periksa Koneksi Internet Anda',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 12.sp),
                      ),
                    ),
                  );
                }
              },
              child: Text(
                'Ya',
                style: TextStyle(fontSize: 14.sp),
              ),
            ),
          ],
        ),
      );
    }

    Widget pic() {
      return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.sp),
        ),
        elevation: 2,
        margin: EdgeInsets.only(
          top: 8.sp,
        ),
        child: Container(
          padding: EdgeInsets.all(15.sp),
          decoration: BoxDecoration(
            color: backgroundColor1,
            borderRadius: BorderRadius.circular(10.sp),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Alamat Pengiriman',
                style: primaryTextStyle.copyWith(
                  fontSize: 10.sp,
                  fontWeight: semiBold,
                ),
              ),
              const Divider(
                thickness: 1,
                color: Colors.black,
              ),
              SizedBox(height: 4.sp),
              Row(
                children: [
                  SizedBox(
                    width: 9.sp,
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Penerima     :  (${detailAlamat.alamatPenerima.namaAlamat}) ${detailAlamat.alamatPenerima.pic}',
                          style: primaryTextStyle.copyWith(
                            fontWeight: medium,
                            fontSize: 9.sp,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(
                          height: 2.sp,
                        ),
                        Text(
                          'Alamat         :  ${detailAlamat.alamatPenerima.alamatLengkap}',
                          style: primaryTextStyle.copyWith(
                            fontWeight: medium,
                            fontSize: 9.sp,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 5,
                        ),
                        SizedBox(
                          height: 2.sp,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }

    Widget payment() {
      return Card(
        margin: EdgeInsets.only(
          top: 19.sp,
        ),
        elevation: 2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.sp),
        ),
        child: Container(
          padding: EdgeInsets.all(20.sp),
          decoration: BoxDecoration(
            color: backgroundColor1,
            borderRadius: BorderRadius.circular(12),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Total',
                style: primaryTextStyle.copyWith(
                  fontSize: 11.sp,
                  fontWeight: medium,
                ),
              ),
              SizedBox(
                height: 8.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Total Belanja',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 8.sp,
                    ),
                  ),
                  Text(
                    'Rp. ${rupiah(widget.orders.totalBelanja)}',
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 9.sp,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Diskon',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 8.sp,
                    ),
                  ),
                  Text(
                    'Rp. ${rupiah(widget.orders.diskonBelanja)}',
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 9.sp,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'CashBack',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 8.sp,
                    ),
                  ),
                  Text(
                    'Rp. ${rupiah(widget.orders.cashbackBelanja)}',
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 9.sp,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Pengiriman',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 8.sp,
                    ),
                  ),
                  Text(
                    int.parse(widget.orders.ongkir.toString()) > 0
                        ? 'Rp. ${rupiah(widget.orders.ongkir)}'
                        : 'Rp.0',
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 9.sp,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Potongan Pengiriman',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 8.sp,
                    ),
                  ),
                  Text(
                    'Rp. ${rupiah(widget.orders.potonganOngkir)}',
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 9.sp,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Biaya Administrator',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 8.sp,
                    ),
                  ),
                  Text(
                    'Rp. ${rupiah(widget.orders.mdr)}',
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 9.sp,
                    ),
                  ),
                ],
              ),
              const Divider(
                thickness: 1,
                color: Colors.white10,
              ),
              SizedBox(
                height: 7.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Total',
                    style: priceTextStyle.copyWith(
                      fontSize: 9.sp,
                      fontWeight: semiBold,
                    ),
                  ),
                  Text(
                    'Rp. ${rupiah(widget.orders.totalBayar)}',
                    style: priceTextStyle.copyWith(
                      fontSize: 9.sp,
                      fontWeight: semiBold,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }

    Widget content() {
      return ListView(
        padding: EdgeInsets.symmetric(
          horizontal: 19.sp,
        ),
        children: [
          pic(),
          DetailTransaksiProduk(
            widget.kondisi,
            widget.idp,
          ),
          {'6', '8'}.contains(widget.kondisi) ? payment() : const SizedBox(),
          SizedBox(
            height: 19.sp,
          ),
        ],
      );
    }

    Widget footDikirim() {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.all(
          19.sp,
        ),
        child: Row(
          children: [
            Expanded(
              child: Container(
                height: 11.5.w,
                child: TextButton(
                  onPressed: () => terimaBarang(context),
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(9.sp),
                    ),
                    backgroundColor: primaryColor,
                  ),
                  child: Text(
                    'Diterima',
                    style: primaryTextStyle.copyWith(
                      fontSize: 11.sp,
                      fontWeight: semiBold,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 8.sp,
            ),
            Expanded(
              child: Container(
                height: 11.5.w,
                child: TextButton(
                  onPressed: () async {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            TrackingPage(widget.orders.awb.toString()),
                        // TrackingPage(),
                      ),
                    );
                  },
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(9.sp),
                    ),
                    backgroundColor: primaryColor,
                  ),
                  child: Text(
                    'Lacak Barang',
                    style: primaryTextStyle.copyWith(
                      fontSize: 11.sp,
                      fontWeight: semiBold,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      );
    }

    Future bintang() {
      return showDialog(
        context: context,
        builder: (BuildContext context) => SizedBox(
          width: 150.w,
          child: AlertDialog(
            backgroundColor: backgroundColor2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.sp),
            ),
            content: Container(
              width: 150.w,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.close,
                          size: 20.sp,
                          color: primaryTextColor,
                        ),
                      ),
                    ),
                    Container(
                      child: Center(
                        child: Text(
                          'Rating',
                          style: primaryTextStyle.copyWith(
                            fontSize: 14.sp,
                          ),
                        ),
                      ),
                    ),
                    Column(
                        children: keranjangProvider
                            .where((x) => x.namaReseller!
                                .toLowerCase()
                                .contains(widget.orders.namaReseller
                                    .toString()
                                    .toLowerCase()))
                            .map(
                              (e) => Card(
                                elevation: 1,
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          width: 15.w,
                                          height: 45.sp,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10.sp),
                                            image: DecorationImage(
                                              image: NetworkImage(
                                                '${urlFile}foto_produk/${e.gambar.toString().split(';').first}',
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Column(
                                            children: [
                                              Center(
                                                child: Text(
                                                  e.namaProduk.toString(),
                                                  style:
                                                      primaryTextStyle.copyWith(
                                                    fontSize: 10.sp,
                                                  ),
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ),
                                              RatingBar.builder(
                                                initialRating: 0,
                                                minRating: 1,
                                                direction: Axis.horizontal,
                                                allowHalfRating: true,
                                                itemCount: 5,
                                                itemPadding:
                                                    EdgeInsets.symmetric(
                                                        horizontal: 4.sp),
                                                // ignore: prefer_const_constructors
                                                itemBuilder: (context, _) =>
                                                    Icon(
                                                  Icons.star,
                                                  color: Colors.amber,
                                                ),
                                                onRatingUpdate: (rating) {
                                                  print(rating);
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(10.sp),
                                      child: Card(
                                        color: backgroundColor2,
                                        elevation: 2,
                                        child: TextFormField(
                                          onChanged: (text) {
                                            // alamatLengkap = text;
                                          },
                                          // controller: alamatLengkapController,
                                          minLines: 4,
                                          maxLines: null,
                                          style: primaryTextStyle.copyWith(
                                              fontSize: 10.sp),
                                          decoration: InputDecoration.collapsed(
                                              hintText: '',
                                              hintStyle: subtitleTextStyle
                                                  .copyWith(fontSize: 10.sp)),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                            .toList()),
                    SizedBox(
                      height: 20.sp,
                    ),
                    Container(
                      alignment: Alignment.bottomRight,
                      child: TextButton(
                        // onPressed: () => terimaBarang(context),
                        onPressed: () {},
                        style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(9.sp),
                          ),
                          backgroundColor: primaryColor,
                        ),
                        child: Text(
                          'Kirim',
                          style: primaryTextStyle.copyWith(
                            fontSize: 11.sp,
                            fontWeight: semiBold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    }

    Widget footBeriUlasan() {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.all(
          19.sp,
        ),
        child: Row(
          children: [
            Expanded(
              child: Container(
                height: 11.5.w,
                child: TextButton(
                  // onPressed: () => terimaBarang(context),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => RatingProductPage(
                          widget.orders.idPenjualan.toString(),
                          widget.orders,
                        ),
                      ),
                    );
                  },
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(9.sp),
                    ),
                    backgroundColor: primaryColor,
                  ),
                  child: Text(
                    keranjangProvider
                                .where((x) => x.namaReseller!
                                    .toLowerCase()
                                    .contains(widget.orders.namaReseller
                                        .toString()
                                        .toLowerCase()))
                                .first
                                .rating ==
                            ''
                        ? 'Beri Ulasan'
                        : 'Lihat Ulasan',
                    style: primaryTextStyle.copyWith(
                      fontSize: 11.sp,
                      fontWeight: semiBold,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget foot() {
      if (widget.kondisi == '3') {
        return footDikirim();
      } else if (widget.kondisi == '4') {
        return footBeriUlasan();
      } else {
        return const SizedBox();
      }
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
        backgroundColor: backgroundColor1,
        appBar: header(),
        body: content(),
        bottomNavigationBar: foot(),
      );
    });
  }
}
