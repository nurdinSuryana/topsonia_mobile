import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/provider/wishlist_provider.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:topsonia_mobile/widgets/wishlist_card.dart';

class WishlistPage extends StatefulWidget {
  List wishlistProduk;
  WishlistPage(this.wishlistProduk, {Key? key}) : super(key: key);

  @override
  State<WishlistPage> createState() => _WishlistPageState();
}

class _WishlistPageState extends State<WishlistPage> {
  @override
  Widget build(BuildContext context) {
    WishlistProvider wishlist = Provider.of<WishlistProvider>(context);

    Widget header() {
      return AppBar(
        backgroundColor: backgroundColor1,
        centerTitle: true,
        title: Text(
          "Favorit",
          style: primaryTextStyle.copyWith(fontSize: 14.sp, fontWeight: medium),
        ),
        elevation: 0,
        automaticallyImplyLeading: false,
      );
    }

    Widget emptyWishlist() {
      return Expanded(
        child: Container(
          color: backgroundColor2,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/image_wishlist.png',
                  width: 17.w,
                ),
                SizedBox(
                  height: 12.sp,
                ),
                Text(
                  'Opss ! Wishlist Kosong',
                  style: primaryTextStyle.copyWith(
                    fontSize: 12.sp,
                    fontWeight: medium,
                  ),
                ),
                SizedBox(
                  height: 6.sp,
                ),
                Text(
                  'Cari Produk Favorit Anda',
                  style: secondaryTextStyle.copyWith(
                    fontSize: 10.sp,
                  ),
                ),
                Container(
                  width: 40.w,
                  height: 26.sp,
                  margin: EdgeInsets.only(
                    top: 13.sp,
                  ),
                  child: TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/home');
                    },
                    style: TextButton.styleFrom(
                      backgroundColor: primaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.sp),
                      ),
                    ),
                    child: Text(
                      'Cari Produk',
                      style: primaryTextStyle.copyWith(
                        fontSize: 10.sp,
                        fontWeight: medium,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }

    Widget content() {
      return Expanded(
        child: Container(
          color: backgroundColor2,
          child: ListView(
            padding: EdgeInsets.symmetric(
              horizontal: 19.sp,
            ),
            children:
                widget.wishlistProduk.map((e) => WishLisctCard(e)).toList(),
          ),
        ),
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Column(
        children: [
          header(),
          wishlist.wishlist.isEmpty ? emptyWishlist() : content(),
        ],
      );
    });
  }
}
