import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:topsonia_mobile/models/product_model.dart';
import 'package:topsonia_mobile/provider/category_product_provider.dart';
import 'package:topsonia_mobile/provider/product_provider.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:topsonia_mobile/widgets/product_tile.dart';
import 'package:http/http.dart' as http;
import 'package:sizer/sizer.dart';
import 'package:package_info_plus/package_info_plus.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final ScrollController _scrollController = ScrollController();
  List produk = [];
  int catIndex = 0;
  String cari = '';
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;
  String? versi;
  int str = 1;
  int lim = 10;
  bool isLoading = false;

  @override
  void initState() {
    validate().whenComplete(() => null);
    version().whenComplete(() => null);
    super.initState();
    // getproduct();

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        getproduct();
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Future version() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      versi = packageInfo.version;
    });
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
  }

  @override
  Widget build(BuildContext context) {
    ProductProvider productProvider = Provider.of<ProductProvider>(context);
    CategoryProductProvider categoryProvider =
        Provider.of<CategoryProductProvider>(context);
    produk = productProvider.products;

    TextEditingController searchController = TextEditingController(text: cari);

    handleSearch() async {
      setState(() {
        isLoading = true;
      });

      await Provider.of<ProductProvider>(context, listen: false).getProducts(
        searchProd: searchController.text,
        apikey: keyApi,
        categoryproduct: catIndex.toString(),
        start: str.toString(),
        limit: lim.toString(),
      );

      setState(() {
        str = 0;
        lim = 10;
        cari = searchController.text;
        isLoading = false;
      });
    }

    Widget header() {
      return Container(
        margin: EdgeInsets.only(
          top: 19.sp,
          left: 19.sp,
          right: 19.sp,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    namaKonsumen.toString(),
                    style: primaryTextStyle.copyWith(
                        fontSize: 15.sp, fontWeight: semiBold),
                  ),
                  Text(
                    username.toString(),
                    style: subtitleTextStyle.copyWith(fontSize: 13.sp),
                  )
                ],
              ),
            ),
            Container(
              width: 11.5.w,
              height: 8.h,
              decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: AssetImage('assets/image_profile.png'))),
            )
          ],
        ),
      );
    }

    Widget categories() {
      return Container(
        margin: EdgeInsets.only(top: 4.h, left: 20.sp),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: categoryProvider.categoryProducts
                .map(
                  (category) => GestureDetector(
                    onTap: () async {
                      setState(() {
                        isLoading = true;
                      });
                      await Provider.of<ProductProvider>(context, listen: false)
                          .getProducts(
                        searchProd: searchController.text,
                        apikey: keyApi,
                        categoryproduct: category.id.toString(),
                        start: str.toString(),
                        limit: lim.toString(),
                      );
                      setState(() {
                        str = 0;
                        lim = 10;
                        catIndex = int.parse(category.id.toString());
                        isLoading = false;
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: 10.sp, vertical: 6.sp),
                      margin: EdgeInsets.only(right: 10.sp),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.sp),
                          border: Border.all(color: subtitleColor),
                          color: catIndex == int.parse(category.id.toString())
                              ? primaryColor
                              : transparentColor),
                      child: Text(
                        category.namaCategory.toString(),
                        style: primaryTextStyle.copyWith(
                            fontSize: 10.sp, fontWeight: medium),
                      ),
                    ),
                  ),
                )
                .toList(),
          ),
        ),
      );
    }

    Widget searchProduct() {
      return Container(
        margin: EdgeInsets.only(
          top: 19.sp,
          left: 19.sp,
          right: 19.sp,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 6.5.h,
              padding: EdgeInsets.symmetric(horizontal: 2.h),
              decoration: BoxDecoration(
                color: backgroundColor2,
                borderRadius: BorderRadius.circular(9.sp),
              ),
              child: Center(
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        handleSearch();
                      },
                      child: Icon(
                        Icons.search,
                        color: primaryColor,
                        size: 17.sp,
                      ),
                    ),
                    SizedBox(
                      width: 2.w,
                    ),
                    Expanded(
                      child: TextFormField(
                        textInputAction: TextInputAction.search,
                        onEditingComplete: () {
                          handleSearch();
                          FocusScope.of(context).requestFocus(FocusNode());
                        },
                        controller: searchController,
                        style: primaryTextStyle.copyWith(fontSize: 12.sp),
                        decoration: InputDecoration.collapsed(
                          hintText: 'Cari Produk',
                          hintStyle:
                              subtitleTextStyle.copyWith(fontSize: 12.sp),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget productTitle() {
      return Container(
        margin: EdgeInsets.only(top: 19.sp, left: 20.sp, right: 19.sp),
        child: Text(
          "Produk",
          style:
              primaryTextStyle.copyWith(fontSize: 15.sp, fontWeight: semiBold),
        ),
      );
    }

    Widget productNotfound() {
      return Container(
        margin: EdgeInsets.only(left: 19.sp, right: 19.sp),
        child: Text(
          "Produk Tidak Di Temukan",
          style: priceTextStyle.copyWith(fontSize: 15.sp, fontWeight: semiBold),
        ),
      );
    }

    Widget loading() {
      return Center(
        child: Text(
          "Loading",
          style: primaryTextStyle.copyWith(
            fontSize: 14.sp,
          ),
        ),
      );
    }

    Widget product() {
      return GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 1.67.w / 1.26.h,
            mainAxisSpacing: 3,
          ),
          itemCount: produk.length,
          physics: const ScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) {
            return ProducTile(produk[index]);
          });
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return ListView(
        controller: _scrollController,
        children: [
          idKonsumen != null ? header() : const SizedBox(),
          searchProduct(),
          categories(),
          productTitle(),
          SizedBox(height: 2.h),
          isLoading == true
              ? loading()
              : productProvider.products.isEmpty
                  ? productNotfound()
                  : product(),
          // product(),
        ],
      );
    });
  }

  getproduct() async {
    String star = (str + 10).toString();
    String end = (lim + 10).toString();

    var url =
        '$urlApi/product/data?cari=$cari&kategori=$catIndex.toString()&start=$star&limit=$end';
    var headers = {
      'Content-Type': 'application/json',
      'Topsonia-X-Key': keyApi
    };

    var response = await http.get(
      Uri.parse(url),
      headers: headers,
    );
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['product'];
      // ignore: avoid_print
      print(jsonDecode(response.body));

      setState(() {
        str = str + 10;
        lim = lim + 10;
        for (var item in data) {
          if (produk
              .where((x) => x.id.toString() == item['id_produk'])
              .isEmpty) {
            produk.add(ProductModel.fromJson(item));
          }
        }
      });
    } else {
      throw Exception('Gagal');
    }
  }
}
