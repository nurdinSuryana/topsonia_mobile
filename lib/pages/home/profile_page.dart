import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/provider/provinsi_provider.dart';
import 'package:topsonia_mobile/theme.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;

  @override
  void initState() {
    validate().whenComplete(() => null);
    super.initState();
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
  }

  @override
  Widget build(BuildContext context) {
    ProvinsiProvider provinsi = Provider.of<ProvinsiProvider>(context);

    Widget header() {
      return AppBar(
        backgroundColor: backgroundColor1,
        centerTitle: true,
        elevation: 0,
        automaticallyImplyLeading: false,
        flexibleSpace: SafeArea(
          child: Container(
            padding: EdgeInsets.all(19.sp),
            child: Row(
              children: [
                ClipOval(
                  child: Image.asset(
                    'assets/image_profile.png',
                    width: 17.w,
                  ),
                ),
                SizedBox(
                  width: 5.w,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        namaKonsumen.toString(),
                        style: primaryTextStyle.copyWith(
                          fontSize: 13.sp,
                          fontWeight: semiBold,
                        ),
                      ),
                      Text(
                        '@${username.toString()}',
                        style: subtitleTextStyle.copyWith(
                          fontSize: 12.sp,
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    final SharedPreferences share =
                        await SharedPreferences.getInstance();
                    share.clear();
                    Navigator.pushNamedAndRemoveUntil(
                        context, '/sign-in', (route) => false);
                  },
                  child: Image.asset(
                    'assets/button_exit.png',
                    width: 7.w,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }

    Widget logIn() {
      return Container(
        height: 5.5.h,
        width: double.infinity,
        margin: EdgeInsets.only(top: 0.5.h),
        child: TextButton(
          onPressed: () {
            Navigator.pushNamedAndRemoveUntil(
                context, '/sign-in', (route) => false);
          },
          style: TextButton.styleFrom(
            backgroundColor: primaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.sp),
            ),
          ),
          child: Text(
            'Login',
            style: primaryTextStyle.copyWith(
              fontSize: 9.sp,
              fontWeight: medium,
            ),
          ),
        ),
      );
    }

    Widget signUp() {
      return Container(
        height: 5.5.h,
        width: double.infinity,
        margin: EdgeInsets.only(top: 0.5.h),
        child: TextButton(
          onPressed: () async {
            if (await provinsi.getProvinsi(
              apikey: keyApi,
              token: '',
            )) {
              Navigator.pushNamedAndRemoveUntil(
                  context, '/sign-up', (route) => false);
            } else {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: secondaryColor,
                  content: Text(
                    'Periksa Koneksi Internet Anda',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12.sp),
                  ),
                ),
              );
            }
          },
          style: TextButton.styleFrom(
            backgroundColor: primaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.sp),
            ),
          ),
          child: Text(
            'Sign Up',
            style: primaryTextStyle.copyWith(
              fontSize: 9.sp,
              fontWeight: medium,
            ),
          ),
        ),
      );
    }

    Widget header2() {
      return AppBar(
        backgroundColor: backgroundColor1,
        centerTitle: true,
        elevation: 0,
        automaticallyImplyLeading: false,
        flexibleSpace: SafeArea(
          child: Container(
            padding: EdgeInsets.all(19.sp),
            child: Column(
              children: [
                Row(
                  children: [
                    ClipOval(
                      child: Image.asset(
                        'assets/image_profile.png',
                        width: 17.w,
                      ),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Hallo, User',
                            style: primaryTextStyle.copyWith(
                              fontSize: 13.sp,
                              fontWeight: semiBold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 22.w,
                    ),
                    Expanded(child: logIn()),
                    SizedBox(
                      width: 2.w,
                    ),
                    Expanded(child: signUp()),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    }

    Widget menuItem(String text) {
      return Container(
        margin: EdgeInsets.only(top: 3.h),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              text,
              style: secondaryTextStyle.copyWith(
                fontSize: 12.sp,
              ),
            ),
            Icon(
              Icons.chevron_right,
              color: primaryTextColor,
            ),
          ],
        ),
      );
    }

    Widget content() {
      return Expanded(
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: defaultMargin,
          ),
          width: double.infinity,
          decoration: BoxDecoration(
            color: backgroundColor2,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 3.h,
              ),
              Text(
                'Account',
                style: primaryTextStyle.copyWith(
                  fontSize: 14.sp,
                  fontWeight: semiBold,
                ),
              ),
              // GestureDetector(
              //     child: menuItem('Edit Profile'),
              //     onTap: () {
              //       Navigator.pushNamed(context, '/edit-profile');
              //     }),
              GestureDetector(
                child: menuItem('Orders Report'),
                onTap: () async {
                  Navigator.pushNamed(context, '/orders-report');
                },
              ),
              // menuItem('Help'),
              // SizedBox(
              //   height: 5.h,
              // ),
              // Text(
              //   'General',
              //   style: primaryTextStyle.copyWith(
              //     fontSize: 14.sp,
              //     fontWeight: semiBold,
              //   ),
              // ),
              // menuItem('Privacy & Policy'),
              // menuItem('Term Of Service'),
              // menuItem('Rate App'),
            ],
          ),
        ),
      );
    }

    if (idKonsumen != null) {
      return Sizer(builder: (context, orientation, deviceType) {
        return Column(
          children: [
            header(),
            content(),
          ],
        );
      });
    } else {
      return Sizer(builder: (context, orientation, deviceType) {
        return Column(
          children: [
            header2(),
          ],
        );
      });
    }
  }
}
