import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/pages/home/chat_page.dart';
import 'package:topsonia_mobile/pages/home/home_page.dart';
import 'package:topsonia_mobile/pages/home/profile_page.dart';
import 'package:topsonia_mobile/pages/home/wishlist_page.dart';
import 'package:topsonia_mobile/provider/wishlist_provider.dart';
import 'package:topsonia_mobile/theme.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentIndex = 0;
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;

  List produkWishlist = [];

  @override
  void initState() {
    validate().whenComplete(() => null);
    super.initState();
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK;
      namaKonsumen = nama;
      username = usern;
      token = tkn;
    });
  }

  Future<bool> _outApp() async {
    return (await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text(
              'Keluar ?',
              style: TextStyle(fontSize: currentIndex == 0 ? 30.sp : 15.sp),
            ),
            content: Text(
              'Apakah Anda Yakin Ingin Keluar Dari Aplikasi Ini',
              style: TextStyle(fontSize: currentIndex == 0 ? 25.sp : 12.sp),
            ),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text(
                  'Tidak',
                  style: TextStyle(fontSize: currentIndex == 0 ? 20.sp : 10.sp),
                ),
              ),
              TextButton(
                onPressed: () => exit(0),
                child: Text(
                  'Ya',
                  style: TextStyle(fontSize: currentIndex == 0 ? 20.sp : 12.sp),
                ),
              ),
            ],
          ),
        )) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    WishlistProvider wishlist = Provider.of<WishlistProvider>(context);
    produkWishlist = wishlist.wishlist;

    Widget cartButton() {
      return SizedBox(
        height: 35.sp,
        width: 35.sp,
        child: FloatingActionButton(
          onPressed: () async {
            if (idKonsumen != null) {
              // if (await Provider.of<DetailKeranjangProvider>(context,
              //         listen: false)
              //     .getDetailKeranjang(
              //         token: token.toString(),
              //         apikey: keyApi,
              //         idKonsumen: idKonsumen.toString())) {
              Navigator.pushNamed(context, '/cart');
              // } else {
              //   ScaffoldMessenger.of(context).showSnackBar(
              //     SnackBar(
              //       backgroundColor: secondaryColor,
              //       content: Text(
              //         'Gagal Periksa Koneksi Internet Anda',
              //         textAlign: TextAlign.center,
              //         style: TextStyle(fontSize: 12.sp),
              //       ),
              //     ),
              //   );
              // }
            } else {
              Navigator.pushNamed(context, '/sign-in');
            }
          },
          child: Image.asset(
            'assets/icon_cart.png',
            width: 5.w,
          ),
          backgroundColor: secondaryColor,
        ),
      );
    }

    Widget customBottomNav() {
      return ClipRRect(
        borderRadius: BorderRadius.vertical(top: Radius.circular(20.sp)),
        child: BottomAppBar(
          shape: const CircularNotchedRectangle(),
          notchMargin: 11.sp,
          clipBehavior: Clip.antiAlias,
          child: BottomNavigationBar(
              backgroundColor: backgroundColor5,
              currentIndex: currentIndex,
              onTap: (value) async {
                setState(() {
                  currentIndex = value;
                });
                if (value == 1 || value == 2) {
                  if (idKonsumen == null) {
                    setState(() {
                      currentIndex = 0;
                    });
                    Navigator.pushNamed(context, '/sign-in');
                  } else {
                    if (await wishlist.getWishlist(
                      token: '',
                      apikey: keyApi,
                      idKonsumen: idKonsumen.toString(),
                    )) {
                      setState(() {
                        produkWishlist = wishlist.wishlist;
                      });
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          backgroundColor: secondaryColor,
                          content: Text(
                            'Gagal Periksa Koneksi Internet Anda',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 12.sp),
                          ),
                        ),
                      );
                      setState(() {
                        currentIndex = 0;
                      });
                    }
                  }
                }
              },
              type: BottomNavigationBarType.fixed,
              items: [
                BottomNavigationBarItem(
                    icon: Container(
                      margin: EdgeInsets.only(top: 8.sp, bottom: 8.sp),
                      child: Image.asset(
                        'assets/icon_home.png',
                        width: 5.w,
                        color: currentIndex == 0
                            ? primaryColor
                            : const Color(0xff808191),
                      ),
                    ),
                    label: ''),
                BottomNavigationBarItem(
                    icon: Container(
                      margin: EdgeInsets.only(top: 8.sp, bottom: 8.sp),
                      child: Image.asset(
                        'assets/icon_chat.png',
                        width: 5.w,
                        color: currentIndex == 1
                            ? primaryColor
                            : const Color(0xff808191),
                      ),
                    ),
                    label: ''),
                BottomNavigationBarItem(
                    icon: Container(
                      margin: EdgeInsets.only(top: 8.sp, bottom: 8.sp),
                      child: Image.asset(
                        'assets/icon_wishlist.png',
                        width: 5.w,
                        color: currentIndex == 2
                            ? primaryColor
                            : const Color(0xff808191),
                      ),
                    ),
                    label: ''),
                BottomNavigationBarItem(
                    icon: Container(
                      margin: EdgeInsets.only(top: 8.sp, bottom: 8.sp),
                      child: Image.asset(
                        'assets/icon_profile.png',
                        width: 5.w,
                        color: currentIndex == 3
                            ? primaryColor
                            : const Color(0xff808191),
                      ),
                    ),
                    label: ''),
              ]),
        ),
      );
    }

    Widget body() {
      switch (currentIndex) {
        case 0:
          return const HomePage();
        case 1:
          return const ChatPage();
        case 2:
          return WishlistPage(produkWishlist);
        case 3:
          return const ProfilePage();
        default:
          return const HomePage();
      }
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return WillPopScope(
        onWillPop: () {
          return _outApp();
        },
        child: Scaffold(
          backgroundColor: backgroundColor1,
          floatingActionButton: cartButton(),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          bottomNavigationBar: customBottomNav(),
          body: Container(
            child: body(),
          ),
        ),
      );
    });
  }
}
