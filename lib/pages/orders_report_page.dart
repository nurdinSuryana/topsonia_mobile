import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/orders_report_model.dart';
import 'package:topsonia_mobile/pages/detail_transaksi_page.dart';
import 'package:topsonia_mobile/pages/orders_report_detail.dart';
import 'package:topsonia_mobile/provider/alamat_penerima_provider.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/orders_report_provider.dart';
import 'package:topsonia_mobile/services/orders_report_service.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:topsonia_mobile/widgets/orders_reportcard.dart';
import 'package:http/http.dart' as http;

class OrdersReportPage extends StatefulWidget {
  const OrdersReportPage({Key? key}) : super(key: key);

  @override
  State<OrdersReportPage> createState() => _OrdersReportPageState();
}

class _OrdersReportPageState extends State<OrdersReportPage>
    with TickerProviderStateMixin {
  int catIndex = 8;
  final ScrollController _scrollController = ScrollController();
  List orders = [];
  String cari = '';
  int str = 0;
  int lim = 9;
  bool isloading = true;
  bool isLoadingDetail = false;

  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;
  late TabController _tabController;

  @override
  void initState() {
    validate().whenComplete(() => null);
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        getorder();
      }
    });
  }

  Future loadReport() async {
    try {
      List<OrdersReportModel> report = await OrdersReportService().getDetail(
        token: '',
        apikey: keyApi,
        idKonsumen: idKonsumen.toString(),
        kondisi: catIndex.toString(),
        start: '0',
        end: '9',
      );

      setState(() {
        orders = report;
        isloading = false;
      });
    } catch (e) {
      setState(() {
        isloading = false;
      });
      print(e);
    }
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
    if (catIndex == 8) {
      loadReport();
    }
  }

  @override
  Widget build(BuildContext context) {
    Future<String> checkStatusqris(String kodeTrx) async {
      var url = '$urlApi/pembayaran/checkStatusqris';
      var headers = {
        'Content-Type': 'application/json',
        'Topsonia-X-Key': keyApi
      };
      var body = jsonEncode({
        'token': token,
        'id_konsumen': idKonsumen,
        'kode_transaksi': kodeTrx,
      });

      var response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );

      if (response.statusCode == 200) {
        var respon = jsonDecode(response.body)['result']['responseStatus'];
        if (respon == 'Failed') {
          return 'failed';
        } else if (respon == 'Success') {
          return 'sukses';
        } else {
          return 'gagal';
        }
      } else {
        return 'gagal';
      }
    }

    OrdersReportProvider ordersReport =
        Provider.of<OrdersReportProvider>(context);
    AlamatPenerimaProvider detailAlamat =
        Provider.of<AlamatPenerimaProvider>(context);

    Widget filterReport() {
      return Container(
        color: backgroundColor2,
        margin: EdgeInsets.only(top: 1.h, left: 10.sp),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              GestureDetector(
                onTap: () async {
                  setState(() {
                    isloading = true;
                  });
                  if (await ordersReport.getOrdersReport(
                    token: '',
                    apikey: keyApi,
                    idKonsumen: idKonsumen.toString(),
                    kondisi: '6',
                    start: '0',
                    end: '9',
                  )) {
                    setState(() {
                      catIndex = 6;
                      str = 0;
                      lim = 9;
                      orders = ordersReport.ordersReport;
                    });
                  }
                  setState(() {
                    isloading = false;
                  });
                },
                child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.sp, vertical: 6.sp),
                  margin: EdgeInsets.only(right: 10.sp),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.sp),
                      border: Border.all(color: subtitleColor),
                      color: catIndex == 6 ? primaryColor : transparentColor),
                  child: Text(
                    'Menunggu Pembayaran',
                    style: primaryTextStyle.copyWith(
                        fontSize: 10.sp, fontWeight: medium),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () async {
                  setState(() {
                    isloading = true;
                  });
                  if (await ordersReport.getOrdersReport(
                    token: '',
                    apikey: keyApi,
                    idKonsumen: idKonsumen.toString(),
                    kondisi: '2',
                    start: '0',
                    end: '9',
                  )) {
                    setState(() {
                      catIndex = 2;
                      str = 0;
                      lim = 9;
                      orders = ordersReport.ordersReport;
                    });
                  }
                  setState(() {
                    isloading = false;
                  });
                },
                child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.sp, vertical: 6.sp),
                  margin: EdgeInsets.only(right: 10.sp),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.sp),
                      border: Border.all(color: subtitleColor),
                      color: catIndex == 2 ? primaryColor : transparentColor),
                  child: Text(
                    'Dikemas',
                    style: primaryTextStyle.copyWith(
                        fontSize: 10.sp, fontWeight: medium),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () async {
                  setState(() {
                    isloading = true;
                  });
                  if (await ordersReport.getOrdersReport(
                    token: '',
                    apikey: keyApi,
                    idKonsumen: idKonsumen.toString(),
                    kondisi: '3',
                    start: '0',
                    end: '9',
                  )) {
                    setState(() {
                      catIndex = 3;
                      str = 0;
                      lim = 9;
                      orders = ordersReport.ordersReport;
                    });
                  }
                  setState(() {
                    isloading = false;
                  });
                },
                child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.sp, vertical: 6.sp),
                  margin: EdgeInsets.only(right: 10.sp),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.sp),
                      border: Border.all(color: subtitleColor),
                      color: catIndex == 3 ? primaryColor : transparentColor),
                  child: Text(
                    'Dikirim',
                    style: primaryTextStyle.copyWith(
                        fontSize: 10.sp, fontWeight: medium),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () async {
                  setState(() {
                    isloading = true;
                  });
                  if (await ordersReport.getOrdersReport(
                    token: '',
                    apikey: keyApi,
                    idKonsumen: idKonsumen.toString(),
                    kondisi: '4',
                    start: '0',
                    end: '9',
                  )) {
                    setState(() {
                      catIndex = 4;
                      str = 0;
                      lim = 9;
                      orders = ordersReport.ordersReport;
                    });
                  }
                  setState(() {
                    isloading = false;
                  });
                },
                child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.sp, vertical: 6.sp),
                  margin: EdgeInsets.only(right: 10.sp),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.sp),
                      border: Border.all(color: subtitleColor),
                      color: catIndex == 4 ? primaryColor : transparentColor),
                  child: Text(
                    'Selesai',
                    style: primaryTextStyle.copyWith(
                        fontSize: 10.sp, fontWeight: medium),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () async {
                  setState(() {
                    isloading = true;
                  });
                  if (await ordersReport.getOrdersReport(
                    token: '',
                    apikey: keyApi,
                    idKonsumen: idKonsumen.toString(),
                    kondisi: '5',
                    start: '0',
                    end: '9',
                  )) {
                    setState(() {
                      catIndex = 5;
                      str = 0;
                      lim = 9;
                      orders = ordersReport.ordersReport;
                    });
                  }
                  setState(() {
                    isloading = false;
                  });
                },
                child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.sp, vertical: 6.sp),
                  margin: EdgeInsets.only(right: 10.sp),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.sp),
                      border: Border.all(color: subtitleColor),
                      color: catIndex == 5 ? primaryColor : transparentColor),
                  child: Text(
                    'Batal',
                    style: primaryTextStyle.copyWith(
                        fontSize: 10.sp, fontWeight: medium),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }

    Widget ordersShow() {
      return ListView(
          controller: _scrollController,
          padding: EdgeInsets.symmetric(
            horizontal: 19.sp,
          ),
          children: orders
              .map(
                (orders) => GestureDetector(
                  onTap: () async {
                    setState(() {
                      isLoadingDetail = true;
                    });
                    if (orders.pembayaran == 'qris' &&
                        orders.status.toString().replaceAll(' ', '') ==
                            'BelumBayar'.toString()) {
                      String statusQris = await checkStatusqris(
                          orders.kodeTransaksi.toString());
                      // print(statusQris);
                      if (statusQris == 'failed') {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => OrdersReportDetail(
                              orders,
                              catIndex.toString(),
                            ),
                          ),
                        );
                      } else if (statusQris == 'sukses') {
                        if (await ordersReport.getOrdersReport(
                          token: '',
                          apikey: keyApi,
                          idKonsumen: idKonsumen.toString(),
                          kondisi: '8',
                          start: '0',
                          end: '9',
                        )) {
                          Navigator.pushNamed(context, '/orders-report');
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              backgroundColor: secondaryColor,
                              content: Text(
                                'Gagal Periksa Koneksi Internet Anda',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 12.sp),
                              ),
                            ),
                          );
                        }
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            backgroundColor: secondaryColor,
                            content: Text(
                              'Periksa Koneksi Internet Anda',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 12.sp),
                            ),
                          ),
                        );
                      }
                    } else {
                      if ({'6', '8'}.contains(catIndex.toString())) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => OrdersReportDetail(
                              orders,
                              catIndex.toString(),
                            ),
                          ),
                        );
                      } else {
                        if (await detailAlamat.getalamat(
                          token: token.toString(),
                          apikey: keyApi,
                          idKonsumen: idKonsumen.toString(),
                          idAlamat: orders.idAlamat,
                        )) {
                          if (await ordersReport.getOrdersReport(
                            token: '',
                            apikey: keyApi,
                            idKonsumen: idKonsumen.toString(),
                            kondisi: '7',
                            kodeTrx: orders.kodeTransaksi.toString(),
                            start: '0',
                            end: '9',
                          )) {
                            if (await Provider.of<DetailKeranjangProvider>(
                                    context,
                                    listen: false)
                                .getDetailKeranjang(
                              token: token.toString(),
                              apikey: keyApi,
                              idKonsumen: idKonsumen.toString(),
                              kodeTrx: orders.kodeTransaksi,
                            )) {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => DetailTransaksiPage(
                                    orders,
                                    catIndex.toString(),
                                    orders.idPenjualan.toString(),
                                  ),
                                ),
                              );
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  backgroundColor: secondaryColor,
                                  content: Text(
                                    'Gagal Periksa Koneksi Internet Anda',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 12.sp),
                                  ),
                                ),
                              );
                            }
                          }
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              backgroundColor: secondaryColor,
                              content: Text(
                                'Data Tidak Di Temukan, Periksa Koneksi Internet Anda !',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 12.sp),
                              ),
                            ),
                          );
                        }
                      }
                    }
                    setState(() {
                      isLoadingDetail = false;
                    });
                  },
                  child: OrdersReportCard(orders, catIndex),
                ),
              )
              .toList());
    }

    Widget ordersNotfound() {
      return Container(
        margin: EdgeInsets.only(top: 20.sp, left: 19.sp, right: 19.sp),
        child: Text(
          "Transaksi Tidak Di Temukan",
          style: priceTextStyle.copyWith(fontSize: 13.sp, fontWeight: semiBold),
        ),
      );
    }

    Widget content() {
      return Container(
        color: backgroundColor2,
        child: orders.isEmpty ? ordersNotfound() : ordersShow(),
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return WillPopScope(
        onWillPop: () async {
          Navigator.of(context, rootNavigator: false).popAndPushNamed('/home');
          return true;
        },
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
                icon: const Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/home');
                }),
            backgroundColor: backgroundColor1,
            centerTitle: true,
            title: Text(
              'Orders Report ',
              style: primaryTextStyle.copyWith(fontSize: 14.sp),
            ),
            elevation: 0,
            bottom: TabBar(
              onTap: (index) async {
                setState(() {
                  isloading = true;
                });
                if (index == 0) {
                  if (await ordersReport.getOrdersReport(
                    token: '',
                    apikey: keyApi,
                    idKonsumen: idKonsumen.toString(),
                    kondisi: '8',
                    start: '0',
                    end: '9',
                  )) {
                    setState(() {
                      catIndex = 8;
                      str = 0;
                      lim = 9;
                      orders = ordersReport.ordersReport;
                    });
                  }
                } else {
                  if (await ordersReport.getOrdersReport(
                    token: '',
                    apikey: keyApi,
                    idKonsumen: idKonsumen.toString(),
                    kondisi: '6',
                    start: '0',
                    end: '9',
                  )) {
                    setState(() {
                      catIndex = 6;
                      str = 0;
                      lim = 9;
                      orders = ordersReport.ordersReport;
                    });
                  }
                }
                setState(() {
                  isloading = false;
                });
              },
              controller: _tabController,
              labelStyle: primaryTextStyle.copyWith(fontSize: 12.sp),
              labelColor: Colors.black,
              tabs: const [
                Tab(
                  child: Text('Per Transaksi'),
                  // icon: Icon(Icons.cloud_outlined),
                ),
                Tab(
                  child: Text('Per Merchant'),
                  // icon: Icon(Icons.beach_access_sharp),
                ),
              ],
            ),
          ),
          backgroundColor: backgroundColor2,
          body: TabBarView(
            physics: const NeverScrollableScrollPhysics(),
            controller: _tabController,
            children: [
              isloading == true || isLoadingDetail == true
                  ? Center(
                      child: Text(
                        'Loading',
                        style: primaryTextStyle.copyWith(
                          fontSize: 15.sp,
                        ),
                      ),
                    )
                  : content(),
              Column(
                children: [
                  SizedBox(
                    height: 10.sp,
                  ),
                  // header(),
                  filterReport(),
                  // emptyOrdersReport(),
                  isloading == true || isLoadingDetail == true
                      ? Expanded(
                          child: Center(
                            child: Text(
                              'Loading',
                              style: primaryTextStyle.copyWith(
                                fontSize: 15.sp,
                              ),
                            ),
                          ),
                        )
                      : Expanded(child: content()),
                ],
              ),
            ],
          ),
        ),
      );
    });
  }

  getorder() async {
    String star = (str + 10).toString();
    String end = (lim + 10).toString();
    var url = '$urlApi/checkout/orders_report';
    var headers = {
      'Content-Type': 'application/json',
      'Topsonia-X-Key': keyApi
    };
    var body = jsonEncode({
      'token': token,
      'id_konsumen': idKonsumen,
      'kondisi': catIndex,
      'start': star,
      'end': end,
    });
    var response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: body,
    );
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['order'];

      if (data.length > 0) {
        setState(() {
          str = str + 10;
          lim = lim + 10;
          for (var item in data) {
            orders.add(OrdersReportModel.fromJson(item));
          }
        });
      }
    } else {
      throw Exception('Gagal');
    }
  }
}
