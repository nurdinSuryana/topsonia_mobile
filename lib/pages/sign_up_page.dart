import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/provider/provinsi_provider.dart';
import 'package:topsonia_mobile/provider/register_provider.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:collection/collection.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  var groupKota;
  String provinsi = "";
  String kota = "";
  String username = "";
  String password = "";
  String name = "";
  String email = "";
  String kecamatan = "";
  String kodePos = "";
  String alamatLengkap = "";
  String noHp = "";

  @override
  Widget build(BuildContext context) {
    TextEditingController usernameController =
        TextEditingController(text: username);
    TextEditingController passwordController =
        TextEditingController(text: password);
    TextEditingController nameController = TextEditingController(text: name);
    TextEditingController emailController = TextEditingController(text: email);
    TextEditingController kecamatanController =
        TextEditingController(text: kecamatan);
    TextEditingController kodePosController =
        TextEditingController(text: kodePos);
    TextEditingController alamatLengkapController =
        TextEditingController(text: alamatLengkap);
    TextEditingController noHpController = TextEditingController(text: noHp);

    ProvinsiProvider provinsiProvider = Provider.of<ProvinsiProvider>(context);

    RegisterProvider registerProvider = Provider.of<RegisterProvider>(context);

    List groupProvinsi = [];
    for (var i = 0; i < provinsiProvider.provinsi.length; i++) {
      bool repeated = false;
      for (var j = 0; j < groupProvinsi.length; j++) {
        if (provinsiProvider.provinsi[i].idProvinsi ==
            groupProvinsi[j].idProvinsi) {
          repeated = true;
        }
      }
      if (!repeated) {
        groupProvinsi.add(provinsiProvider.provinsi[i]);
      }
    }

    if (provinsi == '') {
      provinsi = groupProvinsi.first.idProvinsi.toString();
    }

    groupKota = provinsiProvider.provinsi
        .where((x) => x.idProvinsi!.toLowerCase().contains(provinsi));

    if (kota == '') {
      kota = groupKota.first.idKota.toString();
    } else if (provinsiProvider.provinsi
        .where((x) =>
            x.idProvinsi!.toLowerCase().contains(provinsi) &&
            x.idKota!.toLowerCase().contains(kota))
        .isEmpty) {
      kota = groupKota.first.idKota.toString();
    }

    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 18.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Sign Up',
              style: primaryTextStyle.copyWith(
                fontSize: 16.sp,
                fontWeight: semiBold,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Text(
              'Register and Happy Shopping',
              style: subtitleTextStyle.copyWith(
                fontSize: 10.sp,
              ),
            )
          ],
        ),
      );
    }

    Widget nameInput() {
      return Container(
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Full Name",
              style: primaryTextStyle.copyWith(
                fontSize: 10.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 6.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    // Image.asset('assets/icon_name.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                        child: TextFormField(
                      textInputAction: TextInputAction.next,
                      onChanged: (text) {
                        name = text;
                      },
                      controller: nameController,
                      style: primaryTextStyle.copyWith(fontSize: 10.sp),
                      decoration: InputDecoration.collapsed(
                          hintText: 'Your Full Name',
                          hintStyle:
                              subtitleTextStyle.copyWith(fontSize: 10.sp)),
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget usernameInput() {
      return Container(
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Username",
              style: primaryTextStyle.copyWith(
                  fontSize: 10.sp, fontWeight: medium),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 6.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    // Image.asset('assets/icon_username.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                        child: TextFormField(
                      textInputAction: TextInputAction.next,
                      onChanged: (text) {
                        username = text;
                      },
                      controller: usernameController,
                      style: primaryTextStyle.copyWith(fontSize: 10.sp),
                      decoration: InputDecoration.collapsed(
                          hintText: 'Username',
                          hintStyle:
                              subtitleTextStyle.copyWith(fontSize: 10.sp)),
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget emailInput() {
      return Container(
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Email Address",
              style: primaryTextStyle.copyWith(
                fontSize: 10.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 6.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    // Image.asset('assets/icon_email.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                        child: TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.next,
                      onChanged: (text) {
                        email = text;
                      },
                      controller: emailController,
                      style: primaryTextStyle.copyWith(fontSize: 10.sp),
                      decoration: InputDecoration.collapsed(
                        hintText: 'Your Email Addres',
                        hintStyle: subtitleTextStyle.copyWith(fontSize: 10.sp),
                      ),
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget passwordInput() {
      return Container(
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Password",
              style: primaryTextStyle.copyWith(
                fontSize: 10.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 6.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    // Image.asset('assets/icon_password.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                        child: TextFormField(
                      textInputAction: TextInputAction.next,
                      onChanged: (text) {
                        password = text;
                      },
                      controller: passwordController,
                      style: primaryTextStyle.copyWith(fontSize: 10.sp),
                      obscureText: true,
                      decoration: InputDecoration.collapsed(
                        hintText: 'Your password',
                        hintStyle: subtitleTextStyle.copyWith(fontSize: 10.sp),
                      ),
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget provinsiInput() {
      return Container(
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              provinsiProvider.provinsi.first.namaKota.toString(),
              style: primaryTextStyle.copyWith(
                fontSize: 10.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 6.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    // Image.asset('assets/icon_name.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                      child: DropdownButton<String>(
                        value: provinsi,
                        // icon: const Icon(Icons.arrow_downward),
                        elevation: 16,
                        style: priceTextStyle,
                        underline: Container(
                          height: 0,
                          color: Colors.deepPurpleAccent,
                        ),
                        onChanged: (String? newValue) {
                          setState(() {
                            provinsi = newValue!;
                          });
                        },
                        items: groupProvinsi
                            // items : groupBy(provinsiProvider.)
                            .map<DropdownMenuItem<String>>((value) {
                          return DropdownMenuItem<String>(
                            value: value.idProvinsi.toString(),
                            child: Text(value.namaProvinsi.toString()),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget kotaInput() {
      return Container(
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Kota',
              style: primaryTextStyle.copyWith(
                fontSize: 10.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 6.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    // Image.asset('assets/icon_name.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                      child: DropdownButton<String>(
                        value: kota,
                        // icon: const Icon(Icons.arrow_downward),
                        elevation: 16,
                        style: priceTextStyle,
                        underline: Container(
                          height: 0,
                          color: Colors.deepPurpleAccent,
                        ),
                        onChanged: (String? newValue) {
                          setState(() {
                            kota = newValue!;
                          });
                        },
                        items: groupKota.map<DropdownMenuItem<String>>((value) {
                          return DropdownMenuItem<String>(
                            value: value.idKota.toString(),
                            child: Text(value.namaKota.toString()),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget kecamatanInput() {
      return Container(
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Kecamatan",
              style: primaryTextStyle.copyWith(
                fontSize: 10.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 6.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    //Image.asset('assets/icon_name.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                        child: TextFormField(
                      textInputAction: TextInputAction.next,
                      onChanged: (text) {
                        kecamatan = text;
                      },
                      controller: kecamatanController,
                      style: primaryTextStyle.copyWith(fontSize: 10.sp),
                      decoration: InputDecoration.collapsed(
                          hintText: 'Kecamatan',
                          hintStyle:
                              subtitleTextStyle.copyWith(fontSize: 10.sp)),
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget kodePosInput() {
      return Container(
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Kode Pos",
              style: primaryTextStyle.copyWith(
                fontSize: 10.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 6.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    // Image.asset('assets/icon_name.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                      child: TextFormField(
                        textInputAction: TextInputAction.done,
                        onChanged: (text) {
                          kodePos = text;
                        },
                        controller: kodePosController,
                        style: primaryTextStyle.copyWith(fontSize: 10.sp),
                        decoration: InputDecoration.collapsed(
                          hintText: 'Kode Pos',
                          hintStyle:
                              subtitleTextStyle.copyWith(fontSize: 10.sp),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget alamatLengkapInput() {
      return Container(
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Alamat Lengkap",
              style: primaryTextStyle.copyWith(
                fontSize: 10.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 15.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    // Image.asset('assets/icon_name.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                      child: TextFormField(
                        onChanged: (text) {
                          alamatLengkap = text;
                        },
                        controller: alamatLengkapController,
                        minLines: 20,
                        maxLines: null,
                        style: primaryTextStyle.copyWith(fontSize: 10.sp),
                        decoration: InputDecoration.collapsed(
                            hintText: '',
                            hintStyle:
                                subtitleTextStyle.copyWith(fontSize: 10.sp)),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget noHpInput() {
      return Container(
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "No HP",
              style: primaryTextStyle.copyWith(
                fontSize: 10.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 6.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    // Image.asset('assets/icon_name.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                        child: TextFormField(
                      textInputAction: TextInputAction.next,
                      onChanged: (text) {
                        noHp = text;
                      },
                      controller: noHpController,
                      style: primaryTextStyle.copyWith(fontSize: 10.sp),
                      decoration: InputDecoration.collapsed(
                          hintText: 'No Hp',
                          hintStyle:
                              subtitleTextStyle.copyWith(fontSize: 10.sp)),
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget signUpButton() {
      return Container(
        height: 5.h,
        width: double.infinity,
        margin: EdgeInsets.only(top: 30.sp),
        child: TextButton(
          onPressed: () async {
            if (usernameController.text == '') {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: secondaryColor,
                  content: Text(
                    'Username Belum Di Isi',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12.sp),
                  ),
                ),
              );
            } else if (passwordController.text == '') {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: secondaryColor,
                  content: Text(
                    'Password Belum Di Isi',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12.sp),
                  ),
                ),
              );
            } else if (nameController.text == '') {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: secondaryColor,
                  content: Text(
                    'Nama Belum Di Isi',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12.sp),
                  ),
                ),
              );
            } else if (emailController.text == '') {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: secondaryColor,
                  content: Text(
                    'Email Belum Di Isi',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12.sp),
                  ),
                ),
              );
            } else if (kecamatanController.text == '') {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: secondaryColor,
                  content: Text(
                    'Kecamatan Belum Di Isi',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12.sp),
                  ),
                ),
              );
            } else if (kodePosController.text == '') {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: secondaryColor,
                  content: Text(
                    'Kode Pos Belum Di Isi',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12.sp),
                  ),
                ),
              );
            } else if (alamatLengkapController.text == '') {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: secondaryColor,
                  content: Text(
                    'Alamat Belum Di Isi',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12.sp),
                  ),
                ),
              );
            } else if (noHpController.text == '') {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: secondaryColor,
                  content: Text(
                    'No Hp Belum Di Isi',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12.sp),
                  ),
                ),
              );
            } else {
              if (await registerProvider.register(
                keyapi: keyApi,
                username: username,
                password: password,
                namaLengkap: name,
                email: email,
                provinsi: provinsi,
                kota: kota,
                kecamatan: kecamatan,
                kodePos: kodePos,
                alamatLengkap: alamatLengkap,
                noHp: noHp,
              )) {
                Navigator.pushNamed(context, '/home');
              } else {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    backgroundColor: secondaryColor,
                    content: Text(
                      'Periksa Koneksi Internet Anda',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 12.sp),
                    ),
                  ),
                );
              }
            }
          },
          style: TextButton.styleFrom(
              backgroundColor: primaryColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.sp))),
          child: Text('Sign UP',
              style:
                  primaryTextStyle.copyWith(fontSize: 16, fontWeight: medium)),
        ),
      );
    }

    Widget footer() {
      return Container(
        margin: EdgeInsets.only(bottom: 2.h),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'You have an account? ',
              style: subtitleTextStyle.copyWith(fontSize: 10.sp),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, '/sign-in');
              },
              child: Text(
                'Sign IN',
                style: purpleTextStyle.copyWith(
                    fontSize: 10.sp, fontWeight: medium),
              ),
            ),
          ],
        ),
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: backgroundColor1,
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 19.sp),
          child: ListView(
            children: [
              header(),
              usernameInput(),
              passwordInput(),
              nameInput(),
              emailInput(),
              provinsiInput(),
              kotaInput(),
              kecamatanInput(),
              kodePosInput(),
              alamatLengkapInput(),
              noHpInput(),
              signUpButton(),
              // const Spacer(),
              footer()
            ],
          ),
        ),
      );
    });
  }
}
