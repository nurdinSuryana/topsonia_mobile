import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/orders_report_model.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:http/http.dart' as http;

class RatingProductPage extends StatefulWidget {
  final OrdersReportModel orders;
  final String idp;
  RatingProductPage(this.idp, this.orders, {Key? key}) : super(key: key);

  @override
  State<RatingProductPage> createState() => _RatingProductPageState();
}

class _RatingProductPageState extends State<RatingProductPage> {
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;

  @override
  void initState() {
    validate().whenComplete(() => null);
    super.initState();
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
  }

  @override
  Widget build(BuildContext context) {
    final keranjangProvider =
        Provider.of<DetailKeranjangProvider>(context).detailKeranjang;

    void saveulasan() async {
      List kirim = keranjangProvider;
      var url = '$urlApi/product/saveUlasanProduk';
      var headers = {
        'Content-Type': 'application/json',
        'Topsonia-X-Key': keyApi
      };
      var body = jsonEncode({
        'token': token,
        'id_konsumen': idKonsumen,
        'produk': json.encode(kirim),
        'id_penjualan': widget.idp,
      });
      var response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['produk'];
        print(data);
      } else {
        throw Exception('Gagal');
      }
    }

    PreferredSizeWidget header() {
      return AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.pop(context),
        ),
        backgroundColor: backgroundColor1,
        elevation: 0,
        centerTitle: true,
        title: Text(
          keranjangProvider
                      .where((x) => x.namaReseller!.toLowerCase().contains(
                          widget.orders.namaReseller.toString().toLowerCase()))
                      .first
                      .rating ==
                  ''
              ? 'Beri Ulasan'
              : 'Lihat Ulasan',
          style: primaryTextStyle.copyWith(
            fontSize: 12.sp,
          ),
        ),
      );
    }

    Widget content() {
      return Container(
        margin: EdgeInsets.only(
          top: 19.sp,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 14.sp,
          vertical: 9.sp,
        ),
        decoration: BoxDecoration(
          color: backgroundColor1,
          borderRadius: BorderRadius.circular(12.sp),
        ),
        child: ListView(
          children: [
            // Container(
            //   child: Center(
            //     child: Text(
            //       'Rating',
            //       style: primaryTextStyle.copyWith(
            //         fontSize: 14.sp,
            //       ),
            //     ),
            //   ),
            // ),
            Column(
                children: keranjangProvider
                    .where((x) => x.namaReseller!.toLowerCase().contains(
                        widget.orders.namaReseller.toString().toLowerCase()))
                    .map(
                      (e) => Card(
                        elevation: 1,
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                  width: 15.w,
                                  height: 45.sp,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.sp),
                                    image: DecorationImage(
                                      image: NetworkImage(
                                        '${urlFile}foto_produk/${e.gambar.toString().split(';').first}',
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    children: [
                                      Center(
                                        child: Text(
                                          e.namaProduk.toString(),
                                          style: primaryTextStyle.copyWith(
                                            fontSize: 10.sp,
                                          ),
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                      // Center(
                                      //   child: Text(
                                      //     e.rating.toString(),
                                      //     style: primaryTextStyle.copyWith(
                                      //       fontSize: 10.sp,
                                      //     ),
                                      //     maxLines: 1,
                                      //     overflow: TextOverflow.ellipsis,
                                      //   ),
                                      // ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            RatingBar.builder(
                              initialRating: e.rating.toString() == ''
                                  ? 0
                                  : double.parse(e.rating.toString()),
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 4.sp),
                              // ignore: prefer_const_constructors
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (rating) {
                                final ganti = keranjangProvider.firstWhere(
                                    (se) =>
                                        se.idPenjualanDetail ==
                                        e.idPenjualanDetail);
                                setState(() {
                                  ganti.rating = rating.toString();
                                });
                                // print(rating);
                              },
                            ),
                            Container(
                              padding: EdgeInsets.all(10.sp),
                              child: Card(
                                color: backgroundColor1,
                                elevation: 1,
                                child: TextFormField(
                                  textInputAction: TextInputAction.done,

                                  initialValue: e.ulasan.toString(),
                                  onChanged: (text) {
                                    final ganti = keranjangProvider.firstWhere(
                                        (se) =>
                                            se.idPenjualanDetail ==
                                            e.idPenjualanDetail);
                                    setState(() {
                                      ganti.ulasan = text.toString();
                                    });
                                  },
                                  // controller: alamatLengkapController,
                                  minLines: 4,
                                  maxLines: null,
                                  style: primaryTextStyle.copyWith(
                                      fontSize: 10.sp),
                                  decoration: InputDecoration.collapsed(
                                      hintText: '',
                                      hintStyle: subtitleTextStyle.copyWith(
                                          fontSize: 10.sp)),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                    .toList()),
            SizedBox(
              height: 20.sp,
            ),
            Container(
              alignment: Alignment.bottomRight,
              child: TextButton(
                // onPressed: () => terimaBarang(context),
                onPressed: () {
                  if (keranjangProvider
                      .where((x) =>
                          x.namaReseller!.toLowerCase().contains(widget
                              .orders.namaReseller
                              .toString()
                              .toLowerCase()) &&
                          x.rating == '')
                      .isNotEmpty) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        backgroundColor: secondaryColor,
                        content: Text(
                          'Harap Isi Semua Ulasan',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                      ),
                    );
                  } else {
                    saveulasan();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        backgroundColor: secondaryColor,
                        content: Text(
                          'Ulasan Berhasil D Kirim',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                      ),
                    );
                  }
                },
                style: TextButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(9.sp),
                  ),
                  backgroundColor: primaryColor,
                ),
                child: Text(
                  'Kirim',
                  style: primaryTextStyle.copyWith(
                    fontSize: 11.sp,
                    fontWeight: semiBold,
                  ),
                ),
              ),
            )
          ],
        ),
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
        backgroundColor: backgroundColor1,
        appBar: header(),
        body: content(),
      );
    });
  }
}
