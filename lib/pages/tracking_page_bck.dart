import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/theme.dart';

class TrackingPage extends StatefulWidget {
  final String awb;
  const TrackingPage(this.awb, {Key? key}) : super(key: key);

  @override
  State<TrackingPage> createState() => _TrackingPageState();
}

class _TrackingPageState extends State<TrackingPage> {
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;

  void initState() {
    validate().whenComplete(() => null);
    super.initState();
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
  }

  PreferredSizeWidget header() {
    return AppBar(
      leading: IconButton(
        icon: const Icon(Icons.arrow_back, color: Colors.black),
        onPressed: () => Navigator.pop(context),
      ),
      backgroundColor: backgroundColor1,
      elevation: 0,
      centerTitle: true,
      title: Text(
        'Tracking',
        style: primaryTextStyle.copyWith(
          fontSize: 12.sp,
        ),
      ),
    );
  }

  // Widget content() {
  //   return ListView(
  //     padding: EdgeInsets.symmetric(
  //       horizontal: 20.sp,
  //     ),
  //     children: [
  //       //Note : Addres Detail
  //       Container(
  //         margin: EdgeInsets.only(
  //           top: 20.sp,
  //         ),
  //         padding: EdgeInsets.all(18.sp),
  //         decoration: BoxDecoration(
  //           color: backgroundColor2,
  //           borderRadius: BorderRadius.circular(10.sp),
  //         ),
  //         child: Column(
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: [
  //             Text(
  //               'Tracking',
  //               style: primaryTextStyle.copyWith(
  //                 fontSize: 16,
  //                 fontWeight: medium,
  //               ),
  //             ),
  //             SizedBox(height: 12),
  //             Row(
  //               children: [
  //                 Column(
  //                   children: [
  //                     Image.asset(
  //                       'assets/icon_store_location.png',
  //                       width: 40,
  //                     ),
  //                     Image.asset(
  //                       'assets/icon_line.png',
  //                       height: 30,
  //                     ),
  //                     Image.asset(
  //                       'assets/icon_your_address.png',
  //                       width: 40,
  //                     ),
  //                   ],
  //                 ),
  //                 SizedBox(
  //                   width: 12,
  //                 ),
  //                 Column(
  //                   crossAxisAlignment: CrossAxisAlignment.start,
  //                   children: [
  //                     Text(
  //                       'Store Location',
  //                       style: secondaryTextStyle.copyWith(
  //                         fontSize: 12,
  //                         fontWeight: light,
  //                       ),
  //                     ),
  //                     Text(
  //                       'Adidas Core',
  //                       style: primaryTextStyle.copyWith(
  //                         fontWeight: medium,
  //                       ),
  //                     ),
  //                     SizedBox(
  //                       height: 20.sp,
  //                     ),
  //                     Text(
  //                       'Your Address',
  //                       style: secondaryTextStyle.copyWith(
  //                         fontSize: 12,
  //                         fontWeight: light,
  //                       ),
  //                     ),
  //                     Text(
  //                       'Marsemoon',
  //                       style: primaryTextStyle.copyWith(
  //                         fontWeight: medium,
  //                       ),
  //                     ),
  //                   ],
  //                 )
  //               ],
  //             )
  //           ],
  //         ),
  //       ),
  //     ],
  //   );
  // }

  // Widget content() {
  //   return Container(
  //     child: Timeline.tileBuilder(
  //       builder: TimelineTileBuilder.fromStyle(
  //         contentsAlign: ContentsAlign.alternating,
  //         contentsBuilder: (context, index) => Padding(
  //           padding: const EdgeInsets.all(24.0),
  //           child: Text('Timeline Event $index'),
  //         ),
  //         itemCount: 10,
  //       ),
  //     ),
  //   );
  // }

  Widget content() {
    return Text('sss');
  }

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
        backgroundColor: backgroundColor1,
        appBar: header(),
        body: content(),
      );
    });
  }
}
