import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/error_model.dart';
import 'package:topsonia_mobile/models/global_model.dart';
import 'package:topsonia_mobile/models/product_model.dart';
import 'package:topsonia_mobile/provider/add_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/wishlist_provider.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:flutter_html/flutter_html.dart';

class ProductPage extends StatefulWidget {
  final ProductModel product;

  const ProductPage(this.product, {Key? key}) : super(key: key);

  @override
  State<ProductPage> createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  int currentIndex = 0;
  String? wis;
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;

  @override
  void initState() {
    validate().whenComplete(() => null);
    super.initState();
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
  }

  @override
  Widget build(BuildContext context) {
    WishlistProvider wishlist = Provider.of<WishlistProvider>(context);

    if (wishlist.wishlist
        .where((e) => e.idProduk.toString() == widget.product.id.toString())
        .isNotEmpty) {
      wis = '1';
    } else {
      wis = '2';
    }

    Future<void> changeWishlist() async {
      if (idKonsumen == null) {
        Navigator.pushNamed(context, '/sign-in');
      } else {
        if (await wishlist.getWishlist(
          token: '',
          apikey: keyApi,
          idKonsumen: idKonsumen.toString(),
          kondisi: wis,
          idProduk: widget.product.id.toString(),
        )) {
          setState(() {
            if (wis == '1') {
              wis = '2';
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: secondaryColor,
                  content: Text(
                    'Berhasil Menghapus Dari Daftar Wishlist',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12.sp),
                  ),
                ),
              );
            } else {
              wis = '1';
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: secondaryColor,
                  content: Text(
                    'Berhasil Menambahkan Ke Daftar Wishlist',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12.sp),
                  ),
                ),
              );
            }
          });
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              backgroundColor: secondaryColor,
              content: Text(
                'Periksa Koneksi Internet Anda',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 12.sp),
              ),
            ),
          );
        }
      }
    }

    Future<void> addKeranjang() async {
      if (idKonsumen == null) {
        Navigator.pushNamed(context, '/sign-in');
      } else {
        ErrorModel eror = await Provider.of<AddKeranjangProvider>(
          context,
          listen: false,
        ).addKeranjang(
          token: token.toString(),
          idProduk: widget.product.id.toString(),
          qty: '1',
          apikey: keyApi,
          idKonsumen: idKonsumen.toString(),
        );

        if (eror.statusCode.toString() == '200') {
          return showDialog(
            context: context,
            builder: (BuildContext context) => Container(
              width: 20.w,
              child: AlertDialog(
                backgroundColor: backgroundColor2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.sp),
                ),
                content: SingleChildScrollView(
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.close,
                            color: primaryTextColor,
                          ),
                        ),
                      ),
                      Image.asset(
                        'assets/icon_success.png',
                        width: 20.w,
                      ),
                      SizedBox(
                        height: 10.sp,
                      ),
                      Text(
                        'Success',
                        style: primaryTextStyle.copyWith(
                          fontSize: 14.sp,
                          fontWeight: semiBold,
                        ),
                      ),
                      SizedBox(
                        height: 10.sp,
                      ),
                      Center(
                        child: Text(
                          'Berhasil Menambahkan Produk',
                          style: secondaryTextStyle.copyWith(
                            fontSize: 12.sp,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.sp,
                      ),
                      SizedBox(
                        width: 40.w,
                        height: 30.sp,
                        child: TextButton(
                          onPressed: () async {
                            await Provider.of<DetailKeranjangProvider>(context,
                                    listen: false)
                                .getDetailKeranjang(
                                    token: token.toString(),
                                    apikey: keyApi,
                                    idKonsumen: idKonsumen.toString());
                            Navigator.pushNamed(context, '/cart');
                          },
                          style: TextButton.styleFrom(
                            backgroundColor: primaryColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12),
                            ),
                          ),
                          child: Text(
                            'Lihat Keranjang',
                            style: primaryTextStyle.copyWith(
                              fontSize: 12.sp,
                              fontWeight: medium,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              backgroundColor: secondaryColor,
              content: Text(
                eror.message.toString(),
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 12.sp),
              ),
            ),
          );
        }
      }
    }

    Widget indicator(int index) {
      return Container(
        width: currentIndex == index ? 4.w : 1.w,
        height: 0.5.h,
        margin: EdgeInsets.symmetric(horizontal: 1.sp),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.sp),
          color: currentIndex == index ? primaryColor : const Color(0xffC4C4C4),
        ),
      );
    }

    Widget header() {
      int index = -1;
      return Column(
        children: [
          Container(
            margin: EdgeInsets.only(
              top: 12.5.sp,
              left: 19.sp,
              right: 19.sp,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: const Icon(
                    Icons.chevron_left,
                  ),
                ),
                Icon(
                  Icons.shopping_bag,
                  color: primaryColor,
                ),
              ],
            ),
          ),
          CarouselSlider(
            items: widget.product.gambar
                .toString()
                .split(';')
                .map(
                  (String img) => Image.network(
                    '${urlFile}foto_produk/$img',
                    width: MediaQuery.of(context).size.width,
                    height: 310,
                    fit: BoxFit.cover,
                  ),
                )
                .toList(),
            options: CarouselOptions(
                initialPage: 0,
                onPageChanged: (index, reason) {
                  setState(() {
                    currentIndex = index;
                  });
                }),
          ),
          SizedBox(
            height: 10.sp,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: widget.product.gambar.toString().split(';').map((e) {
              index++;
              return indicator(index);
            }).toList(),
          )
        ],
      );
    }

    Widget content() {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 10.sp),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20.sp),
          ),
          color: backgroundColor1,
        ),
        child: Column(
          children: [
            //Note: header
            Container(
              margin: EdgeInsets.only(
                top: 19.sp,
                left: 19.sp,
                right: 19.sp,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.product.namaProduk.toString(),
                          style: primaryTextStyle.copyWith(
                            fontSize: 11.sp,
                            fontWeight: semiBold,
                          ),
                        ),
                      ],
                    ),
                  ),
                  wis == '1'
                      ? GestureDetector(
                          onTap: changeWishlist,
                          child: Image.asset(
                            'assets/button_wishlist_blue.png',
                            width: 9.w,
                          ),
                        )
                      : GestureDetector(
                          onTap: changeWishlist,
                          child: Image.asset(
                            'assets/button_wishlist.png',
                            width: 9.w,
                          ),
                        ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                left: 19.sp,
                right: 19.sp,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Rp.${rupiah(widget.product.hargaProduk.toString())}',
                          style: priceTextStyle.copyWith(
                            fontSize: 10.sp,
                            fontWeight: semiBold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            widget.product.rating.toString() == ''
                ? SizedBox()
                : Container(
                    margin: EdgeInsets.only(
                      left: 19.sp,
                      right: 19.sp,
                    ),
                    child: Row(
                      children: [
                        Icon(
                          Icons.star,
                          size: 12.sp,
                          color: primaryColor,
                        ),
                        Text(
                          '${widget.product.rating}',
                          style: primaryTextStyle.copyWith(
                            fontSize: 11.sp,
                          ),
                        ),
                      ],
                    ),
                  ),
            Container(
              padding: EdgeInsets.all(10.sp),
              width: double.infinity,
              margin: EdgeInsets.only(
                top: 12.sp,
                left: 19.sp,
                right: 19.sp,
              ),
              decoration: BoxDecoration(
                color: backgroundColor2,
                borderRadius: BorderRadius.circular(4.sp),
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Berat',
                        style: primaryTextStyle.copyWith(
                          fontSize: 9.5.sp,
                        ),
                      ),
                      Text(
                        '${widget.product.berat} Gram',
                        style: primaryTextStyle.copyWith(
                          fontSize: 9.5.sp,
                          fontWeight: semiBold,
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 4.sp,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Kategori',
                        style: primaryTextStyle.copyWith(
                          fontSize: 9.5.sp,
                        ),
                      ),
                      Text(
                        widget.product.kategori.toString(),
                        style: primaryTextStyle.copyWith(
                          fontSize: 9.5.sp,
                          fontWeight: semiBold,
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Stok',
                        style: primaryTextStyle.copyWith(
                          fontSize: 9.5.sp,
                        ),
                      ),
                      Text(
                        '${widget.product.stock} ${widget.product.satuan}',
                        style: primaryTextStyle.copyWith(
                          fontSize: 9.5.sp,
                          fontWeight: semiBold,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            //Note: Description
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(
                top: 10.sp,
                left: 19.sp,
                right: 19.sp,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Description',
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 10.sp,
                    ),
                  ),
                  SizedBox(
                    height: 1.sp,
                  ),
                  Html(
                    data: '<div>${widget.product.keterangan.toString()}</div>',
                    style: {
                      'div': Style(
                        color: const Color(0xff504F5E),
                      )
                    },
                  ),
                ],
              ),
            ),

            Container(
              width: double.infinity,
              margin: EdgeInsets.all(
                19.sp,
              ),
            ),
          ],
        ),
      );
    }

    Widget bottom() {
      return Container(
        color: backgroundColor1,
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.all(
            19.sp,
          ),
          child: Row(
            children: [
              // Container(
              //   width: 11.5.w,
              //   height: 6.2.h,
              //   decoration: const BoxDecoration(
              //     image: DecorationImage(
              //       image: AssetImage(
              //         'assets/button_chat.png',
              //       ),
              //     ),
              //   ),
              // ),
              // SizedBox(
              //   width: 8.sp,
              // ),
              Expanded(
                child: Container(
                  height: 11.5.w,
                  child: TextButton(
                    onPressed: addKeranjang,
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(9.sp),
                      ),
                      backgroundColor: primaryColor,
                    ),
                    child: Text(
                      'Add To Cart',
                      style: primaryTextStyle.copyWith(
                        fontSize: 11.sp,
                        fontWeight: semiBold,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
        backgroundColor: backgroundColor2,
        body: ListView(
          children: [
            header(),
            content(),
          ],
        ),
        bottomNavigationBar: bottom(),
      );
    });
  }
}
