import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/detail_reseller_model.dart';
import 'package:topsonia_mobile/models/global_model.dart';
import 'package:topsonia_mobile/pages/orders_report_detail.dart';
import 'package:topsonia_mobile/provider/alamat_penerima_provider.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/detail_reseller_provider.dart';
import 'package:topsonia_mobile/provider/orders_report_provider.dart';
import 'package:topsonia_mobile/provider/payment_provider.dart';
import 'package:topsonia_mobile/provider/proses_checkout_provider.dart';
import 'package:topsonia_mobile/services/detail_reseller_service.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:topsonia_mobile/widgets/loading_button.dart';
import 'package:topsonia_mobile/widgets/reseller_list.dart';
import 'package:http/http.dart' as http;

class CheckoutPage extends StatefulWidget {
  const CheckoutPage({Key? key}) : super(key: key);

  @override
  State<CheckoutPage> createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  bool isLoading2 = false;
  bool isLoadingb = false;
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;
  List reseller = [];

  @override
  void initState() {
    validate().whenComplete(() => null);
    super.initState();
  }

  Future<bool> buildqris(String kodeTrx) async {
    var url = '$urlApi/pembayaran/genarateQrcode';
    var headers = {
      'Content-Type': 'application/json',
      'Topsonia-X-Key': keyApi
    };
    var body = jsonEncode({
      'token': token,
      'id_konsumen': idKonsumen,
      'kode_transaksi': kodeTrx,
    });

    var response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: body,
    );

    //var data = jsonDecode(response.body);

    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future getReseller() async {
    try {
      List<DetailResellerModel> detailReseller =
          await DetailResellerService().getDetail(
        token: '',
        apikey: keyApi,
        idKonsumen: idKonsumen!,
      );
      setState(() {
        reseller = detailReseller;
      });
      return true;
    } catch (e) {
      return false;
    }
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
  }

  @override
  Widget build(BuildContext context) {
    OrdersReportProvider ordersReport =
        Provider.of<OrdersReportProvider>(context);
    AlamatPenerimaProvider detailAlamat =
        Provider.of<AlamatPenerimaProvider>(context);
    ProsesCheckoutProvider prosesCheckout =
        Provider.of<ProsesCheckoutProvider>(context);
    final keranjangProvider =
        Provider.of<DetailKeranjangProvider>(context).detailKeranjang;
    DetailResellerProvider detailReseller =
        Provider.of<DetailResellerProvider>(context);
    PaymentProvider mdrPayment = Provider.of<PaymentProvider>(context);

    List<String> va = ['bca', 'bni', 'bri', 'mandiri', 'permata', 'other_bank'];

    int totalBelanja = keranjangProvider.isEmpty
        ? 0
        : keranjangProvider
            .map((m) =>
                int.parse(m.hargaJual.toString()) *
                int.parse(m.jumlah.toString()))
            .reduce((a, b) => a + b);

    int totalBarang = keranjangProvider.isEmpty
        ? 0
        : keranjangProvider
            .map((m) => int.parse(m.jumlah.toString()))
            .reduce((a, b) => a + b);

    int cashback = detailReseller.detailReseller.isEmpty
        ? 0
        : detailReseller.detailReseller
            .map((m) => int.parse(m.cashback.toString()))
            .reduce((a, b) => a + b);
    int diskon = detailReseller.detailReseller.isEmpty
        ? 0
        : detailReseller.detailReseller
            .map((m) => int.parse(m.diskon.toString()))
            .reduce((a, b) => a + b);

    int totalPengiriman = detailReseller.detailReseller.isEmpty
        ? 0
        : detailReseller.detailReseller
            .map((m) => m.tarif == '' ? 0 : int.parse(m.tarif.toString()))
            .reduce((a, b) => a + b);

    int diskonPengiriman = detailReseller.detailReseller.isEmpty
        ? 0
        : detailReseller.detailReseller
            .map((m) =>
                m.diskonTarif == '' ? 0 : int.parse(m.diskonTarif.toString()))
            .reduce((a, b) => a + b);

    Widget showPembayaran() {
      return Card(
        elevation: 1,
        child: Container(
          padding: EdgeInsets.only(bottom: 12.sp),
          child: Container(
            padding: EdgeInsets.all(15.sp),
            child: Column(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    mdrPayment.payment
                            .where((x) => x.jenisPayment!
                                .toLowerCase()
                                .contains('Virtual Account'.toLowerCase()))
                            .isNotEmpty
                        ? Text(
                            'Virtual Account',
                            style: primaryTextStyle.copyWith(
                              fontSize: 11.sp,
                            ),
                          )
                        : const SizedBox(),
                    Column(
                      children: mdrPayment.payment
                          .where((x) => x.jenisPayment!
                              .toLowerCase()
                              .contains('Virtual Account'.toLowerCase()))
                          .map(
                            (e) => e.status == '1'
                                ? GestureDetector(
                                    onTap: () {
                                      final ganti =
                                          detailReseller.detailReseller;
                                      int jmlS = ganti.toList().length;
                                      setState(() {
                                        for (int i = 0; i < jmlS; i++) {
                                          ganti[i].biayaAdmin = e.mdr;
                                          ganti[i].pembayaran = e.namaPayment;
                                        }
                                      });
                                      Navigator.pop(context);
                                      Navigator.pushNamed(context, '/checkout');
                                    },
                                    child: Card(
                                      margin: EdgeInsets.only(top: 10.sp),
                                      color: backgroundColor2,
                                      elevation: 1,
                                      child: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                              left: 4.sp,
                                              top: 4.sp,
                                              bottom: 4.sp,
                                            ),
                                            width: 16.w,
                                            height: 34.sp,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10.sp),
                                              image: DecorationImage(
                                                image: NetworkImage(
                                                  '${urlFile}ket_pembayaran/${e.logo}',
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 9.sp,
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  e.valuePayment.toString(),
                                                  style:
                                                      primaryTextStyle.copyWith(
                                                    fontWeight: medium,
                                                    fontSize: 10.sp,
                                                  ),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                                SizedBox(
                                                  height: 1.sp,
                                                ),
                                                Text(
                                                  'Biaya Admin Rp. ${rupiah(e.mdr)}',
                                                  style:
                                                      priceTextStyle.copyWith(
                                                    fontSize: 9.sp,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                : const SizedBox(),
                          )
                          .toList(),
                    ),
                    SizedBox(
                      height: 8.sp,
                    ),
                    mdrPayment.payment
                            .where((x) => x.jenisPayment!
                                .toLowerCase()
                                .contains('E-Wallet'.toLowerCase()))
                            .isNotEmpty
                        ? Text(
                            'E-Wallet',
                            style: primaryTextStyle.copyWith(
                              fontSize: 11.sp,
                            ),
                          )
                        : const SizedBox(),
                    Column(
                      children: mdrPayment.payment
                          .where((x) => x.jenisPayment!
                              .toLowerCase()
                              .contains('E-Wallet'.toLowerCase()))
                          .map(
                            (e) => e.status == '1'
                                ? GestureDetector(
                                    onTap: () {
                                      final ganti =
                                          detailReseller.detailReseller;
                                      int jmlS = ganti.toList().length;
                                      setState(() {
                                        for (int i = 0; i < jmlS; i++) {
                                          ganti[i].biayaAdmin = e.mdr;
                                          ganti[i].pembayaran = e.namaPayment;
                                        }
                                      });
                                      Navigator.pop(context);
                                      Navigator.pushNamed(context, '/checkout');
                                    },
                                    child: Card(
                                      margin: EdgeInsets.only(top: 10.sp),
                                      color: backgroundColor2,
                                      elevation: 1,
                                      child: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                              left: 4.sp,
                                              top: 4.sp,
                                              bottom: 4.sp,
                                            ),
                                            width: 16.w,
                                            height: 34.sp,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10.sp),
                                              image: DecorationImage(
                                                image: NetworkImage(
                                                  '${urlFile}ket_pembayaran/${e.logo}',
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 9.sp,
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  e.valuePayment.toString(),
                                                  style:
                                                      primaryTextStyle.copyWith(
                                                    fontWeight: medium,
                                                    fontSize: 10.sp,
                                                  ),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                                SizedBox(
                                                  height: 1.sp,
                                                ),
                                                Text(
                                                  'Biaya Admin Rp. ${rupiah(e.mdr)}',
                                                  style:
                                                      priceTextStyle.copyWith(
                                                    fontSize: 9.sp,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 9.sp,
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                : const SizedBox(),
                          )
                          .toList(),
                    ),
                    SizedBox(
                      height: 8.sp,
                    ),
                    mdrPayment.payment
                            .where((x) => x.jenisPayment!
                                .toLowerCase()
                                .contains('manual-transfer'.toLowerCase()))
                            .isNotEmpty
                        ? Text(
                            'Manual-Transfer',
                            style: primaryTextStyle.copyWith(
                              fontSize: 11.sp,
                            ),
                          )
                        : const SizedBox(),
                    Column(
                      children: mdrPayment.payment
                          .where((x) => x.jenisPayment!
                              .toLowerCase()
                              .contains('manual-transfer'.toLowerCase()))
                          .map(
                            (e) => e.status == '1'
                                ? GestureDetector(
                                    onTap: () {
                                      final ganti =
                                          detailReseller.detailReseller;
                                      int jmlS = ganti.toList().length;
                                      setState(() {
                                        for (int i = 0; i < jmlS; i++) {
                                          ganti[i].biayaAdmin = e.mdr;
                                          ganti[i].pembayaran = e.namaPayment;
                                        }
                                      });
                                      Navigator.pop(context);
                                      Navigator.pushNamed(context, '/checkout');
                                    },
                                    child: Card(
                                      margin: EdgeInsets.only(top: 10.sp),
                                      color: backgroundColor2,
                                      elevation: 1,
                                      child: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                              left: 4.sp,
                                              top: 4.sp,
                                              bottom: 4.sp,
                                            ),
                                            width: 16.w,
                                            height: 34.sp,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10.sp),
                                              image: DecorationImage(
                                                image: NetworkImage(
                                                  '${urlFile}ket_pembayaran/${e.logo}',
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 9.sp,
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  e.valuePayment.toString(),
                                                  style:
                                                      primaryTextStyle.copyWith(
                                                    fontWeight: medium,
                                                    fontSize: 10.sp,
                                                  ),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                                SizedBox(
                                                  height: 1.sp,
                                                ),
                                                Text(
                                                  'Biaya Admin Rp. ${rupiah(e.mdr)}',
                                                  style:
                                                      priceTextStyle.copyWith(
                                                    fontSize: 9.sp,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            width: 7.sp,
                                          ),
                                          SizedBox(
                                            height: 9.sp,
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                : const SizedBox(),
                          )
                          .toList(),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    }

    Future<void> pilihPembayaran(total) async {
      setState(() {
        isLoading2 = true;
      });
      if (await mdrPayment.getPayment(
        apikey: keyApi,
        token: '',
        total: total,
      )) {
        setState(() {
          isLoading2 = false;
        });
        return showDialog(
          context: context,
          builder: (BuildContext context) => SizedBox(
            width: 30.w,
            child: AlertDialog(
              backgroundColor: backgroundColor2,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.sp),
              ),
              content: SingleChildScrollView(
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.close,
                          size: 10.sp,
                          color: primaryTextColor,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10.sp),
                      padding: EdgeInsets.symmetric(
                        vertical: 3.sp,
                        horizontal: 12.sp,
                      ),
                      child: Column(children: [
                        showPembayaran(),
                      ]),
                    ),
                    SizedBox(
                      height: 10.sp,
                    ),
                    SizedBox(
                      width: 300.sp,
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      } else {}
    }

    PreferredSizeWidget header() {
      return AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.pushNamed(context, '/cart'),
        ),
        backgroundColor: backgroundColor1,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Checkout Details',
          style: primaryTextStyle.copyWith(
            fontSize: 12.sp,
          ),
        ),
      );
    }

    Widget pic() {
      return Card(
        margin: EdgeInsets.only(
          top: 8.sp,
        ),
        elevation: 2,
        color: backgroundColor1,
        child: Container(
          padding: EdgeInsets.all(15.sp),
          decoration: BoxDecoration(
            color: backgroundColor1,
            borderRadius: BorderRadius.circular(10.sp),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Alamat Pengiriman',
                style: primaryTextStyle.copyWith(
                  fontSize: 10.sp,
                  fontWeight: semiBold,
                ),
              ),
              const Divider(
                thickness: 1,
                color: Colors.black,
              ),
              SizedBox(height: 4.sp),
              Row(
                children: [
                  SizedBox(
                    width: 9.sp,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Penerima     :  (${detailAlamat.alamatPenerima.namaAlamat}) ${detailAlamat.alamatPenerima.pic}',
                          style: primaryTextStyle.copyWith(
                            fontWeight: medium,
                            fontSize: 9.sp,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(
                          height: 2.sp,
                        ),
                        Text(
                          'Alamat         :  ${detailAlamat.alamatPenerima.alamatLengkap}',
                          style: primaryTextStyle.copyWith(
                            fontWeight: medium,
                            fontSize: 9.sp,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        SizedBox(
                          height: 2.sp,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }

    Widget payment() {
      return Card(
        margin: EdgeInsets.only(
          top: 19.sp,
        ),
        color: backgroundColor1,
        elevation: 2,
        child: Container(
          padding: EdgeInsets.all(20.sp),
          decoration: BoxDecoration(
            color: backgroundColor1,
            borderRadius: BorderRadius.circular(12),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Total',
                style: primaryTextStyle.copyWith(
                  fontSize: 11.sp,
                  fontWeight: medium,
                ),
              ),
              SizedBox(
                height: 8.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Jumlah Barang',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 8.sp,
                    ),
                  ),
                  Text(
                    totalBarang.toString(),
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 9.sp,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Total Belanja',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 8.sp,
                    ),
                  ),
                  Text(
                    'Rp. ${rupiah(totalBelanja)} ',
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 9.sp,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Diskon',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 8.sp,
                    ),
                  ),
                  Text(
                    'Rp. ${rupiah(diskon)}',
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 9.sp,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'CashBack',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 8.sp,
                    ),
                  ),
                  Text(
                    '(Rp. ${rupiah(cashback)} )',
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 9.sp,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Pengiriman',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 8.sp,
                    ),
                  ),
                  Text(
                    'Rp. ${rupiah(totalPengiriman)}',
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 9.sp,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Potongan Pengiriman',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 8.sp,
                    ),
                  ),
                  Text(
                    'Rp. ${rupiah(diskonPengiriman)}',
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 9.sp,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Biaya Administrator',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 8.sp,
                    ),
                  ),
                  Text(
                    detailReseller.detailReseller.first.biayaAdmin.toString() ==
                            ''
                        ? 'Rp. 0'
                        : "Rp. ${rupiah(detailReseller.detailReseller.first.biayaAdmin)}"
                            .toString(),
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 9.sp,
                    ),
                  ),
                ],
              ),
              const Divider(
                thickness: 1,
                color: Colors.white10,
              ),
              SizedBox(
                height: 7.sp,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Total',
                    style: priceTextStyle.copyWith(
                      fontSize: 9.sp,
                      fontWeight: semiBold,
                    ),
                  ),
                  Text(
                    'Rp. ${rupiah(totalBelanja + totalPengiriman + (detailReseller.detailReseller.first.biayaAdmin.toString() == '' ? 0 : int.parse(detailReseller.detailReseller.first.biayaAdmin.toString())) - diskon - diskonPengiriman)}',
                    style: priceTextStyle.copyWith(
                      fontSize: 9.sp,
                      fontWeight: semiBold,
                    ),
                  ),
                ],
              ),
              detailReseller.detailReseller.first.biayaAdmin.toString() == ''
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          height: 25.sp,
                          width: 33.w,
                          margin: EdgeInsets.symmetric(
                            vertical: 8.sp,
                          ),
                          child: isLoading2
                              ? const Text('Loading')
                              : TextButton(
                                  onPressed: () {
                                    pilihPembayaran(totalBelanja +
                                        totalPengiriman -
                                        diskon);
                                  },
                                  style: TextButton.styleFrom(
                                    backgroundColor: primaryColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.sp),
                                    ),
                                  ),
                                  child: Text(
                                    'Metode Pembayaran',
                                    style: primaryTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      fontSize: 8.sp,
                                    ),
                                  ),
                                ),
                        ),
                      ],
                    )
                  : GestureDetector(
                      onTap: () {
                        pilihPembayaran(
                            totalBelanja + totalPengiriman - diskon);
                      },
                      child: Container(
                        margin: EdgeInsets.symmetric(
                          vertical: 8.sp,
                        ),
                        padding: EdgeInsets.only(bottom: 12.sp),
                        child: Container(
                          padding: EdgeInsets.all(9.sp),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.sp),
                            color: backgroundColor2,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              va.contains(detailReseller
                                      .detailReseller.first.pembayaran
                                      .toString())
                                  ? Text(
                                      'Pembayaran : Virtual Account ${detailReseller.detailReseller.first.pembayaran}',
                                      style: primaryTextStyle.copyWith(
                                          fontSize: 9.sp),
                                    )
                                  : Text(
                                      'Pembayaran : ${detailReseller.detailReseller.first.pembayaran}',
                                      style: primaryTextStyle.copyWith(
                                          fontSize: 9.sp),
                                    )
                            ],
                          ),
                        ),
                      ),
                    ),
            ],
          ),
        ),
      );
    }

    Widget btnCheckout() {
      return Container(
        height: 50,
        width: double.infinity,
        margin: EdgeInsets.symmetric(
          vertical: defaultMargin,
        ),
        child: TextButton(
          onPressed: () async {
            setState(() {
              isLoadingb = true;
            });
            int kurir = detailReseller.detailReseller
                .where((y) => y.service!.toString() == '')
                .toList()
                .length;
            if (kurir > 0) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: secondaryColor,
                  content: Text(
                    'Metode Pengiriman Belum Di Pilih',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12.sp),
                  ),
                ),
              );
            } else {
              int biayaAdmin = detailReseller.detailReseller
                  .where((y) => y.biayaAdmin!.toString() == '')
                  .toList()
                  .length;
              if (biayaAdmin > 0) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    backgroundColor: secondaryColor,
                    content: Text(
                      'Metode Pembayaran Belum Di Pilih',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 12.sp),
                    ),
                  ),
                );
              } else {
                if (await prosesCheckout.getProsesCheckouts(
                  token: '',
                  apikey: keyApi,
                  idKonsumen: idKonsumen.toString(),
                  reseller: detailReseller.detailReseller,
                  produk: keranjangProvider,
                  totalBayar: totalBelanja +
                      totalPengiriman +
                      (detailReseller.detailReseller.first.biayaAdmin
                                  .toString() ==
                              ''
                          ? 0
                          : int.parse(detailReseller
                              .detailReseller.first.biayaAdmin
                              .toString())) -
                      diskon -
                      diskonPengiriman,
                )) {
                  if (await ordersReport.getOrdersReport(
                    token: '',
                    apikey: keyApi,
                    idKonsumen: idKonsumen.toString(),
                    kondisi: '8',
                    start: '0',
                    end: '9',
                  )) {
                    if (ordersReport.ordersReport.first.pembayaran == 'qris' &&
                        ordersReport.ordersReport.first.status
                                .toString()
                                .replaceAll(' ', '') ==
                            'BelumBayar'.toString()) {
                      if (await buildqris(ordersReport
                          .ordersReport.first.kodeTransaksi
                          .toString())) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => OrdersReportDetail(
                                ordersReport.ordersReport.first, '8'),
                          ),
                        );
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            backgroundColor: secondaryColor,
                            content: Text(
                              'Periksa Koneksi Internet Anda',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 12.sp),
                            ),
                          ),
                        );
                      }
                    } else {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => OrdersReportDetail(
                              ordersReport.ordersReport.first, '8'),
                        ),
                      );
                    }
                  }
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      backgroundColor: secondaryColor,
                      content: Text(
                        'Periksa Koneksi Internet Anda',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 12.sp),
                      ),
                    ),
                  );
                }
              }
            }
            setState(() {
              isLoadingb = false;
            });
          },
          style: TextButton.styleFrom(
            backgroundColor: primaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
          ),
          child: Text(
            'Checkout Now',
            style: primaryTextStyle.copyWith(
              fontWeight: semiBold,
              fontSize: 16,
            ),
          ),
        ),
      );
    }

    Widget content() {
      return ListView(
        padding: EdgeInsets.symmetric(
          horizontal: 19.sp,
        ),
        children: [
          pic(),
          const ResellerList(),
          payment(),
          isLoadingb ? const LoadingButton() : btnCheckout(),
        ],
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return WillPopScope(
        onWillPop: () async {
          Navigator.of(context, rootNavigator: false).popAndPushNamed('/cart');
          return true;
        },
        child: Scaffold(
          backgroundColor: backgroundColor2,
          appBar: header(),
          body: content(),
        ),
      );
    });
  }
}
