// ignore_for_file: prefer_const_constructors

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:topsonia_mobile/provider/category_product_provider.dart';
import 'package:topsonia_mobile/provider/product_provider.dart';
import 'package:http/http.dart' as http;

import '../theme.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  String appV = '';
  String? versi;

  @override
  void initState() {
    version().whenComplete(() => null);

    //Timer(Duration(seconds: 3), () => Navigator.pushNamed(context, '/home'));
    super.initState();
  }

  getproduk() async {
    await Provider.of<ProductProvider>(context, listen: false).getProducts(
      searchProd: '',
      apikey: keyApi,
      start: '0',
      limit: '10',
    );
    await Provider.of<CategoryProductProvider>(context, listen: false)
        .getcategoryProducts(apikey: keyApi);
    Navigator.pushNamed(context, '/home');
  }

  updateApp() async {
    Navigator.pushNamed(context, '/update-Page');
  }

  Future version() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    var url = '$urlApi/auth/cek_version';
    var headers = {
      'Content-Type': 'application/json',
      'Topsonia-X-Key': keyApi
    };
    var response = await http.get(
      Uri.parse(url),
      headers: headers,
    );
    if (response.statusCode == 200) {
      var link = jsonDecode(response.body)['link'];
      versi = jsonDecode(response.body)['version'];
      appV = packageInfo.version.toString();
      if (appV == versi) {
        getproduk();
      } else {
        final SharedPreferences share = await SharedPreferences.getInstance();
        share.setString("linkApp", link.toString());
        share.setString("versiApp", versi.toString());
        updateApp();
      }
    } else {
      throw Exception('Gagal');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFFAB00),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/splash.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        // child: Column(
        //   mainAxisAlignment: MainAxisAlignment.center,
        //   crossAxisAlignment: CrossAxisAlignment.stretch,
        //   // ignore: prefer_const_literals_to_create_immutables
        //   children: [
        //     SizedBox(
        //       height: 5,
        //     ),
        //     Image.asset(
        //       'assets/logo_topsonia.png',
        //       width: 100,
        //       height: 100,
        //     ),
        //     SizedBox(
        //       height: 15,
        //     ),
        //     Image.asset(
        //       'assets/shop2.png',
        //       width: 100,
        //       height: 100,
        //     ),
        //     // SizedBox(
        //     //   height: 100,
        //     //   width: 100,
        //     //   child: CircularProgressIndicator(
        //     //     valueColor: AlwaysStoppedAnimation(Colors.orange.shade50),
        //     //   ),
        //     // ),
        //   ],
        // ),
      ),
    );
  }
}
