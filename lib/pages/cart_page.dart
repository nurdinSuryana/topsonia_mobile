import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/detail_keranjang_model.dart';
import 'package:topsonia_mobile/provider/alamat_penerima_provider.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/detail_reseller_provider.dart';
import 'package:topsonia_mobile/services/detail_keranjang_service.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:topsonia_mobile/widgets/cart_card.dart';

class CartPage extends StatefulWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  int? sum;
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;
  bool isLoading = false;
  bool isLoadC = false;
  List keranjang = [];

  @override
  void initState() {
    validate().whenComplete(() => null);
    super.initState();
  }

  Future loadKeranjang() async {
    try {
      List<DetailKeranjangModel> detailKeranjang =
          await DetailKeranjangService().getDetail(
        token: '',
        apikey: keyApi,
        idKonsumen: idKonsumen!,
      );
      setState(() {
        keranjang = detailKeranjang;
        isLoading = false;
      });
      // return true;
    } catch (e) {
      setState(() {
        isLoading = false;
      });

      // return false;
    }
  }

  Future validate() async {
    setState(() {
      isLoading = true;
    });
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
    loadKeranjang();
  }

  Widget loading() {
    return Center(
      child: Text(
        "Loading",
        style: primaryTextStyle.copyWith(
          fontSize: 14.sp,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    DetailResellerProvider detailReseller =
        Provider.of<DetailResellerProvider>(context);
    AlamatPenerimaProvider detailAlamat =
        Provider.of<AlamatPenerimaProvider>(context);
    final rupiah = NumberFormat('#,##0', 'ID');
    DetailKeranjangProvider keranjangProvider =
        Provider.of<DetailKeranjangProvider>(context);
    // keranjang = keranjangProvider.detailKeranjang;
    sum = keranjang.isEmpty
        ? 0
        : keranjang
            .map((m) =>
                int.parse(m.hargaJual.toString()) *
                int.parse(m.jumlah.toString()))
            .reduce((a, b) => a + b);

    PreferredSizeWidget header() {
      return AppBar(
        leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/home');
            }),
        backgroundColor: backgroundColor1,
        centerTitle: true,
        title: Text(
          'Keranjang',
          style: primaryTextStyle.copyWith(
            fontSize: 14.sp,
          ),
        ),
        elevation: 0,
      );
    }

    Widget emptyCart() {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/icon_empty_cart.png',
              width: 17.w,
            ),
            SizedBox(
              height: 12.sp,
            ),
            Text(
              'Opss ! Keranjang Kosong',
              style: primaryTextStyle.copyWith(
                fontSize: 12.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 6.sp,
            ),
            Text(
              'Cari Produk Favorit Anda',
              style: secondaryTextStyle.copyWith(
                fontSize: 10.sp,
              ),
            ),
            Container(
              width: 40.w,
              height: 26.sp,
              margin: EdgeInsets.only(
                top: 13.sp,
              ),
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/home');
                },
                style: TextButton.styleFrom(
                  backgroundColor: primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.sp),
                  ),
                ),
                child: Text(
                  'Cari Produk',
                  style: primaryTextStyle.copyWith(
                    fontSize: 10.sp,
                    fontWeight: medium,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget content() {
      return ListView(
          padding: EdgeInsets.symmetric(
            horizontal: 19.sp,
          ),
          children: keranjang
              .map(
                (x) => CartCard(x),
              )
              .toList());
    }

    Widget customBottomNav() {
      return SizedBox(
        height: 19.h,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: 19.sp,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Subtotal',
                    style: primaryTextStyle.copyWith(fontSize: 12.sp),
                  ),
                  Text(
                    'Rp.${rupiah.format(int.parse(sum.toString()))}',
                    style: priceTextStyle.copyWith(
                      fontSize: 12.sp,
                      fontWeight: semiBold,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 1.h,
            ),
            Divider(
              thickness: 0.3,
              color: subtitleColor,
            ),
            SizedBox(
              height: 3.5.h,
            ),
            Container(
              height: 6.h,
              margin: EdgeInsets.symmetric(
                horizontal: defaultMargin,
              ),
              child: isLoadC == true
                  ? Text(
                      "Loading",
                      style: primaryTextStyle.copyWith(
                        fontSize: 14.sp,
                      ),
                    )
                  : TextButton(
                      onPressed: () async {
                        setState(() {
                          isLoadC = true;
                        });
                        if (await detailAlamat.getalamat(
                          token: token.toString(),
                          apikey: keyApi,
                          idKonsumen: idKonsumen.toString(),
                        )) {
                          await detailReseller.getDetailReseller(
                            token: token.toString(),
                            apikey: keyApi,
                            idKonsumen: idKonsumen.toString(),
                          );
                          await keranjangProvider.getDetailKeranjang(
                            token: token.toString(),
                            apikey: keyApi,
                            idKonsumen: idKonsumen.toString(),
                          );
                          Navigator.pushNamed(context, '/checkout');
                        }
                        setState(() {
                          isLoadC = false;
                        });
                      },
                      style: TextButton.styleFrom(
                        backgroundColor: primaryColor,
                        padding: EdgeInsets.symmetric(
                          horizontal: 16.sp,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.sp),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Continue to Checkout',
                            style: primaryTextStyle.copyWith(
                              fontSize: 12.sp,
                              fontWeight: semiBold,
                            ),
                          ),
                          Icon(
                            Icons.arrow_forward,
                            color: primaryTextColor,
                          ),
                        ],
                      ),
                    ),
            ),
          ],
        ),
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return WillPopScope(
        onWillPop: () async {
          Navigator.of(context, rootNavigator: false).popAndPushNamed('/home');
          return true;
        },
        child: Scaffold(
          backgroundColor: backgroundColor2,
          appBar: header(),
          body: isLoading == true
              ? loading()
              : keranjang.isEmpty
                  ? emptyCart()
                  : content(),
          bottomNavigationBar: isLoading == true
              ? loading()
              : keranjang.isEmpty
                  ? const SizedBox()
                  : customBottomNav(),
        ),
      );
    });
  }
}
