import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:timeline_tile/timeline_tile.dart';
import 'package:topsonia_mobile/models/tracking_model.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:http/http.dart' as http;

class TrackingPage extends StatefulWidget {
  final String awb;
  const TrackingPage(this.awb, {Key? key}) : super(key: key);

  @override
  State<TrackingPage> createState() => _TrackingPageState();
}

class _TrackingPageState extends State<TrackingPage> {
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;
  bool isLoading = true;

  List<TrackingModel> tracking = [];

  void initState() {
    validate().whenComplete(() => null);
    super.initState();
  }

  Future getTracking() async {
    var url = '$urlApi/tracking/index';
    var headers = {
      'Content-Type': 'application/json',
      'Topsonia-X-Key': keyApi
    };
    var body = jsonEncode({
      'token': token,
      'awb': widget.awb,
    });
    var response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: body,
    );
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['tracking'];
      List<TrackingModel> lacak = [];
      // ignore: avoid_print
      // print(jsonDecode(data));
      for (var item in data) {
        lacak.add(TrackingModel.fromJson(item));
      }
      setState(() {
        tracking = lacak;
        isLoading = false;
      });

      // print(lacak);
    } else {
      throw Exception('Gagal');
    }

    setState(() {
      isLoading = false;
    });
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
    getTracking();
  }

  PreferredSizeWidget header() {
    return AppBar(
      leading: IconButton(
        icon: const Icon(Icons.arrow_back, color: Colors.black),
        onPressed: () => Navigator.pop(context),
      ),
      backgroundColor: backgroundColor1,
      elevation: 0,
      centerTitle: true,
      title: Text(
        'Tracking',
        style: primaryTextStyle.copyWith(
          fontSize: 12.sp,
        ),
      ),
    );
  }

  Widget order() {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.sp),
      ),
      elevation: 2,
      margin: EdgeInsets.only(
        top: 8.sp,
      ),
      child: Container(
        padding: EdgeInsets.all(15.sp),
        decoration: BoxDecoration(
          color: backgroundColor1,
          borderRadius: BorderRadius.circular(10.sp),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Ncs Tracking',
              style: primaryTextStyle.copyWith(
                fontSize: 10.sp,
                fontWeight: semiBold,
              ),
            ),
            SizedBox(height: 4.sp),
            Row(
              children: [
                SizedBox(
                  width: 9.sp,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SelectableText(
                        'Awb           : ${widget.awb} ',
                        style: primaryTextStyle.copyWith(
                          fontWeight: medium,
                          fontSize: 10.sp,
                        ),
                      ),
                      SizedBox(
                        height: 2.sp,
                      ),
                      Text(
                        'Status       :  ${tracking.where((e) => e.lastStatus == '1').first.comment}',
                        style: primaryTextStyle.copyWith(
                          fontWeight: medium,
                          fontSize: 10.sp,
                        ),
                      ),
                      SizedBox(
                        height: 2.sp,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget lacak() {
    return Column(
      children: tracking
          .map(
            (e) => Container(
              margin: EdgeInsets.only(
                left: 30.sp,
                right: 30.sp,
              ),
              width: 220.sp,
              height: 65.sp,
              child: TimelineTile(
                endChild: Container(
                  color: backgroundColor1,
                  margin: EdgeInsets.only(
                    left: 3.sp,
                  ),
                  child: Card(
                    margin: EdgeInsets.only(bottom: 10.sp),
                    elevation: 1,
                    child: Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                              left: 10.sp,
                            ),
                            child: Text(
                              e.comment.toString(),
                              style: primaryTextStyle.copyWith(
                                fontSize: 10.sp,
                              ),
                            ),
                          ),
                          e.status == 'OK'
                              ? const SizedBox()
                              : Container(
                                  margin: EdgeInsets.only(
                                    left: 9.sp,
                                  ),
                                  child: Text(
                                    e.station.toString(),
                                    style: primaryTextStyle.copyWith(
                                      fontSize: 10.sp,
                                    ),
                                  ),
                                ),
                          e.status == 'OK'
                              ? Container(
                                  margin: EdgeInsets.only(
                                    left: 10.sp,
                                  ),
                                  child: Text(
                                    "Penerima: ${e.recipient}(${e.relationName})",
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: primaryTextStyle.copyWith(
                                      fontSize: 9.sp,
                                    ),
                                  ),
                                )
                              : const SizedBox(),
                          Container(
                            margin: EdgeInsets.only(
                              left: 10.sp,
                            ),
                            child: Text(
                              e.timeStamp.toString(),
                              style: primaryTextStyle.copyWith(
                                fontSize: 8.sp,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                isLast: e.lastStatus.toString() == '1' ? true : false,
                isFirst: e.nomor.toString() == '1' ? true : false,
              ),
            ),
          )
          .toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
        backgroundColor: backgroundColor1,
        appBar: header(),
        body: isLoading == true
            ? Center(
                child: Text(
                  'Loading',
                  style: primaryTextStyle.copyWith(
                    fontSize: 15.sp,
                  ),
                ),
              )
            : tracking.isEmpty
                ? Center(
                    child: Text(
                    'Data Tidak Ditemukan',
                    style: primaryTextStyle.copyWith(
                      fontSize: 15.sp,
                    ),
                  ))
                : ListView(
                    shrinkWrap: true,
                    children: [
                      order(),
                      SizedBox(
                        height: 20.sp,
                      ),
                      lacak(),
                    ],
                  ),
      );
    });
  }
}
