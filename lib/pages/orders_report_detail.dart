import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/error_model.dart';
import 'package:topsonia_mobile/models/global_model.dart';
import 'package:topsonia_mobile/models/orders_report_model.dart';
import 'package:topsonia_mobile/pages/detail_transaksi_page.dart';
import 'package:topsonia_mobile/provider/alamat_penerima_provider.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/intruksi_payment_provider.dart';
import 'package:topsonia_mobile/provider/orders_report_provider.dart';
import 'package:topsonia_mobile/provider/pmb_manual_transfer_provider.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:http/http.dart' as http;

class OrdersReportDetail extends StatefulWidget {
  final OrdersReportModel orders;
  final String kondisi;

  const OrdersReportDetail(this.orders, this.kondisi, {Key? key})
      : super(key: key);

  @override
  State<OrdersReportDetail> createState() => _OrdersReportDetailState();
}

class _OrdersReportDetailState extends State<OrdersReportDetail> {
  int currentIndex = 0;
  final format = DateFormat("yyyy-MM-dd");
  String stringImage = '';
  XFile? imageFile;
  String rekPengirim = '';
  String nPengirim = '';
  String tglTransfer = '';
  DateTime? currentTime;
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;
  bool timePayment = true;

  @override
  void initState() {
    if (widget.orders.pembayaran.toString() == 'qris') {
      setState(() {
        currentTime = DateFormat("yyyy-MM-dd hh:mm:ss")
            .parse(widget.orders.waktuOrder.toString())
            .add(const Duration(minutes: 6));
      });
    } else {
      setState(() {
        currentTime = DateFormat("yyyy-MM-dd hh:mm:ss")
            .parse(widget.orders.waktuOrder.toString())
            .add(const Duration(hours: 48));
      });
    }
    validate().whenComplete(() => null);
    super.initState();
  }

  Future paymentTime() async {
    setState(() {
      timePayment = false;
    });
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
  }

  Future<void> _showChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Choose option",
              style: TextStyle(color: Colors.blue, fontSize: 10.sp),
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Divider(
                    height: 1.sp,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openGallery(context);
                    },
                    title: Text(
                      "Gallery",
                      style: TextStyle(color: Colors.blue, fontSize: 10.sp),
                    ),
                    leading: const Icon(
                      Icons.account_box,
                      color: Colors.blue,
                    ),
                  ),
                  Divider(
                    height: 1.sp,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openCamera(context);
                    },
                    title: Text(
                      "Camera",
                      style: TextStyle(color: Colors.blue, fontSize: 10.sp),
                    ),
                    leading: const Icon(
                      Icons.camera,
                      color: Colors.blue,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController rekeningPengirim =
        TextEditingController(text: rekPengirim);
    TextEditingController namaPengirim = TextEditingController(text: nPengirim);
    TextEditingController tanggalTransfer =
        TextEditingController(text: tglTransfer);
    OrdersReportProvider ordersReport =
        Provider.of<OrdersReportProvider>(context);
    AlamatPenerimaProvider detailAlamat =
        Provider.of<AlamatPenerimaProvider>(context);

    int endTime = currentTime!.millisecondsSinceEpoch + 1000 * 30;

    IntruksiPaymentProvider iPayment =
        Provider.of<IntruksiPaymentProvider>(context);

    Future<Object> uploadPembayaran() async {
      if (rekeningPengirim.text == '') {
        return ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: secondaryColor,
            content: Text(
              'No Rekening Pengirim Belum Di Isi',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 12.sp),
            ),
          ),
        );
      } else if (namaPengirim.text == '') {
        return ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: secondaryColor,
            content: Text(
              'Nama Pengirim Belum Di Isi',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 12.sp),
            ),
          ),
        );
      } else if (tanggalTransfer.text == '') {
        return ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: secondaryColor,
            content: Text(
              'Tanggal Transfer Belum Di isi',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 12.sp),
            ),
          ),
        );
      } else if (imageFile == null) {
        return ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: secondaryColor,
            content: Text(
              'Bukti Transfer Belum DI Upload',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 12.sp),
            ),
          ),
        );
      } else {
        ErrorModel eror = await Provider.of<PmbManualTransferProvider>(
          context,
          listen: false,
        ).upload(
          token: token.toString(),
          apikey: keyApi,
          idKonsumen: idKonsumen.toString(),
          buktiTransfer: stringImage,
          kodeTransaksi: widget.orders.kodeTransaksi.toString(),
          namaPengirim: namaPengirim.text,
          noRekeningPenerima: widget.orders.nomor.toString(),
          noRekeningPengirim: rekeningPengirim.text,
          nominal: widget.orders.totalBelanja.toString(),
          tanggalTransfer: tanggalTransfer.text,
        );

        if (eror.statusCode.toString() == '200') {
          return showDialog(
            context: context,
            builder: (BuildContext context) => Container(
              width: 20.w,
              child: AlertDialog(
                backgroundColor: backgroundColor3,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.sp),
                ),
                content: SingleChildScrollView(
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: GestureDetector(
                          onTap: () async {
                            if (await ordersReport.getOrdersReport(
                              token: '',
                              apikey: keyApi,
                              idKonsumen: idKonsumen.toString(),
                              kondisi: '6',
                            )) {
                              Navigator.pushNamed(context, '/orders-report');
                            } else {
                              // Navigator.pushNamed(context, '/orders-report');
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  backgroundColor: secondaryColor,
                                  content: Text(
                                    'Periksa Koneksi Internet Anda',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 12.sp),
                                  ),
                                ),
                              );
                            }
                          },
                          child: Icon(
                            Icons.close,
                            color: primaryTextColor,
                          ),
                        ),
                      ),
                      Image.asset(
                        'assets/icon_success.png',
                        width: 20.w,
                      ),
                      SizedBox(
                        height: 10.sp,
                      ),
                      Text(
                        'Success',
                        style: primaryTextStyle.copyWith(
                          fontSize: 14.sp,
                          fontWeight: semiBold,
                        ),
                      ),
                      SizedBox(
                        height: 10.sp,
                      ),
                      Text(
                        'Upload Bukti Transfer Berhasil',
                        style: secondaryTextStyle.copyWith(
                          fontSize: 12.sp,
                        ),
                      ),
                      SizedBox(
                        height: 10.sp,
                      ),
                      SizedBox(
                        width: 20.w,
                        height: 30.sp,
                        child: TextButton(
                          onPressed: () async {
                            if (await ordersReport.getOrdersReport(
                              token: '',
                              apikey: keyApi,
                              idKonsumen: idKonsumen.toString(),
                              kondisi: '6',
                            )) {
                              Navigator.pushNamed(context, '/orders-report');
                            } else {
                              // Navigator.pushNamed(context, '/orders-report');
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  backgroundColor: secondaryColor,
                                  content: Text(
                                    'Periksa Koneksi Internet Anda',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 12.sp),
                                  ),
                                ),
                              );
                            }
                          },
                          style: TextButton.styleFrom(
                            backgroundColor: primaryColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12.sp),
                            ),
                          ),
                          child: Text(
                            'OK',
                            style: primaryTextStyle.copyWith(
                              fontSize: 12.sp,
                              fontWeight: medium,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        } else {
          return ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              backgroundColor: secondaryColor,
              content: Text(
                eror.message.toString(),
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 12.sp),
              ),
            ),
          );
        }
      }
    }

    Future<bool> checkStatusqris(String kodeTrx) async {
      var url = '$urlApi/pembayaran/checkStatusqris';
      var headers = {
        'Content-Type': 'application/json',
        'Topsonia-X-Key': keyApi
      };
      var body = jsonEncode({
        'token': token,
        'id_konsumen': idKonsumen,
        'kode_transaksi': kodeTrx,
      });

      var response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );

      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    }

    Future intruksi() async {
      if (await iPayment.get(
        token: '',
        apikey: keyApi,
        payment: widget.orders.pembayaran.toString(),
      )) {
        return showDialog(
          context: context,
          builder: (BuildContext context) => Container(
            width: 30.w,
            child: AlertDialog(
              backgroundColor: backgroundColor2,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.sp),
              ),
              content: SingleChildScrollView(
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.close,
                          size: 10.sp,
                          color: primaryTextColor,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10.sp),
                      padding: EdgeInsets.symmetric(
                        vertical: 3.sp,
                        horizontal: 12.sp,
                      ),
                      child: Column(
                        children: iPayment.intruksiPayment
                            .map(
                              (e) => ExpansionTile(
                                title: Text(
                                  e.via.toString(),
                                  style: primaryTextStyle,
                                ),
                                children: <Widget>[
                                  Html(
                                    data: '<div>${e.intruksi.toString()}</div>',
                                    style: {
                                      'div': Style(
                                        color: const Color(0xff504F5E),
                                      )
                                    },
                                  ),
                                ],
                              ),
                            )
                            .toList(),
                      ),
                    ),
                    SizedBox(
                      height: 10.sp,
                    ),
                    SizedBox(
                      width: 300.sp,
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: secondaryColor,
            content: Text(
              'Periksa Koneksi Internet Anda',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 12.sp),
            ),
          ),
        );
      }
    }

    Widget header() {
      return Container(
        color: backgroundColor1,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(
                top: 12.5.sp,
                left: 19.sp,
                right: 19.sp,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/orders-report');
                    },
                    child: const Icon(
                      Icons.chevron_left,
                    ),
                  ),
                  Text(
                    'Detail Pesanan',
                    style: TextStyle(
                      fontSize: 12.sp,
                    ),
                  ),
                  Icon(
                    Icons.shopping_bag,
                    color: backgroundColor1,
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget contentManualTransfer() {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 10.sp),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20.sp),
          ),
          color: backgroundColor1,
        ),
        child: Column(
          children: [
            //Note: header
            Container(
              margin: EdgeInsets.only(
                top: 19.sp,
                left: 19.sp,
                right: 19.sp,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SelectableText(
                          ' Kode Transaksi : ${widget.orders.kodeTransaksi.toString()}',
                          style: primaryTextStyle.copyWith(
                            fontSize: 11.sp,
                            fontWeight: semiBold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            {'6', '8'}.contains(widget.kondisi)
                ? const SizedBox()
                : Container(
                    margin: EdgeInsets.only(
                      top: 2.sp,
                      left: 19.sp,
                      right: 19.sp,
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SelectableText(
                                ' ${widget.orders.namaReseller.toString()}',
                                style: primaryTextStyle.copyWith(
                                  fontSize: 11.sp,
                                  fontWeight: semiBold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
            Card(
              elevation: 2,
              margin: EdgeInsets.only(
                top: 12.sp,
                left: 19.sp,
                right: 19.sp,
              ),
              child: Container(
                padding: EdgeInsets.all(10.sp),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(4.sp),
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Waktu Order',
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                          ),
                        ),
                        Text(
                          widget.orders.waktuOrder.toString(),
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                            fontWeight: semiBold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4.sp,
                    ),
                    // widget.orders.status.toString() == 'Belum Bayar'.toString()
                    widget.orders.status.toString().replaceAll(' ', '') ==
                            'BelumBayar'.toString()
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Waktu Pembayaran',
                                style: primaryTextStyle.copyWith(
                                  fontSize: 9.5.sp,
                                ),
                              ),
                              CountdownTimer(
                                endTime: endTime,
                                widgetBuilder: (context, time) {
                                  if (time == null) {
                                    return Text(
                                      'Waktu Pembayaran Habis',
                                      style: primaryTextStyle.copyWith(
                                        fontSize: 9.5.sp,
                                      ),
                                    );
                                  } else {
                                    return Text(
                                      ' ${time.days ?? 0} Hari, ${time.hours ?? 0}:${time.min ?? 0}:${time.sec}',
                                      style: primaryTextStyle.copyWith(
                                        fontSize: 9.5.sp,
                                      ),
                                    );
                                  }
                                },
                                onEnd: () {
                                  paymentTime();
                                },
                              ),
                            ],
                          )
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Status Pembayaran',
                                style: primaryTextStyle.copyWith(
                                  fontSize: 9.5.sp,
                                ),
                              ),
                              Text(
                                widget.orders.status.toString(),
                                style: primaryTextStyle.copyWith(
                                  fontSize: 9.5.sp,
                                ),
                              ),
                            ],
                          ),
                    SizedBox(
                      height: 4.sp,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Total Pembayaran',
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                          ),
                        ),
                        Text(
                          'Rp. ${rupiah(widget.orders.totalBayar.toString())}',
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                            fontWeight: semiBold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4.sp,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Metode Pembayaran',
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                          ),
                        ),
                        Text(
                          widget.orders.pembayaran.toString(),
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                            fontWeight: semiBold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4.sp,
                    ),
                    widget.orders.nomor == null || timePayment == false
                        ? const SizedBox()
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Nomor Rekening',
                                style: primaryTextStyle.copyWith(
                                  fontSize: 9.5.sp,
                                ),
                              ),
                              Text(
                                widget.orders.nomor.toString(),
                                style: primaryTextStyle.copyWith(
                                  fontSize: 9.5.sp,
                                  fontWeight: semiBold,
                                ),
                              )
                            ],
                          ),
                  ],
                ),
              ),
            ),
            widget.orders.status.toString().replaceAll(' ', '') ==
                        'BelumBayar'.toString() &&
                    timePayment == true
                ? Container(
                    padding: EdgeInsets.all(10.sp),
                    width: double.infinity,
                    margin: EdgeInsets.only(
                      top: 12.sp,
                      left: 19.sp,
                      right: 19.sp,
                    ),
                    decoration: BoxDecoration(
                      color: backgroundColor2,
                      borderRadius: BorderRadius.circular(4.sp),
                    ),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Rekening Pengirim',
                              style: primaryTextStyle.copyWith(
                                fontSize: 9.5.sp,
                              ),
                            ),
                            SizedBox(
                              width: 20.sp,
                            ),
                            Expanded(
                              child: TextFormField(
                                onChanged: (text) {
                                  rekPengirim = text;
                                },
                                controller: rekeningPengirim,
                                style:
                                    primaryTextStyle.copyWith(fontSize: 12.sp),
                                decoration: InputDecoration.collapsed(
                                  hintText: 'No Rekening Pengirim',
                                  hintStyle: subtitleTextStyle.copyWith(
                                      fontSize: 9.sp),
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 4.sp,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Nama Pengirim ',
                              style: primaryTextStyle.copyWith(
                                fontSize: 9.5.sp,
                              ),
                            ),
                            SizedBox(
                              width: 31.sp,
                            ),
                            Expanded(
                              child: TextFormField(
                                onChanged: (text) {
                                  nPengirim = text;
                                },
                                controller: namaPengirim,
                                style:
                                    primaryTextStyle.copyWith(fontSize: 12.sp),
                                decoration: InputDecoration.collapsed(
                                  hintText: 'Nama Pengirim',
                                  hintStyle: subtitleTextStyle.copyWith(
                                      fontSize: 9.sp),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 0.sp,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Tanggal Transfer',
                              style: primaryTextStyle.copyWith(
                                fontSize: 9.5.sp,
                              ),
                            ),
                            SizedBox(
                              width: 25.sp,
                            ),
                            Expanded(
                              child: DateTimeField(
                                format: format,
                                onShowPicker: (context, currentValue) {
                                  return showDatePicker(
                                      context: context,
                                      firstDate: DateTime(1900),
                                      initialDate:
                                          currentValue ?? DateTime.now(),
                                      lastDate: DateTime(2100));
                                },
                                onChanged: (text) {
                                  setState(() {
                                    tglTransfer = DateFormat("yyyy-MM-dd")
                                        .format(text!)
                                        .toString();
                                  });
                                },
                                style:
                                    primaryTextStyle.copyWith(fontSize: 10.sp),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 4.sp,
                        ),
                      ],
                    ),
                  )
                : const SizedBox(),
            widget.orders.status.toString().replaceAll(' ', '') ==
                        'BelumBayar'.toString() &&
                    timePayment == true
                ? Center(
                    child: Container(
                      margin: EdgeInsets.only(
                        left: 15.sp,
                        right: 15.sp,
                        top: 6.sp,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Card(
                            child: (imageFile == null)
                                ? SizedBox()
                                : Image.file(File(imageFile!.path)),
                          ),
                          MaterialButton(
                            textColor: Colors.white,
                            color: Colors.pink,
                            onPressed: () {
                              _showChoiceDialog(context);
                            },
                            child: Text(
                              "Bukti Transfer",
                              style: TextStyle(
                                fontSize: 10.sp,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                : const SizedBox(),
            widget.orders.status.toString().replaceAll(' ', '') ==
                        'BelumBayar'.toString() &&
                    timePayment == true
                ? Container(
                    margin: EdgeInsets.only(
                      top: 8.sp,
                      left: 19.sp,
                      right: 19.sp,
                    ),
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () {
                              intruksi();
                            },
                            child: Text(
                              'Lihat Cara Pembayaran',
                              style: primaryTextStyle.copyWith(
                                fontSize: 9.sp,
                                fontWeight: semiBold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : const SizedBox(),
          ],
        ),
      );
    }

    Widget contentQris() {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 10.sp),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20.sp),
          ),
          color: backgroundColor1,
        ),
        child: Column(
          children: [
            //Note: header
            Container(
              margin: EdgeInsets.only(
                top: 19.sp,
                left: 19.sp,
                right: 19.sp,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SelectableText(
                          ' Kode Transaksi : ${widget.orders.kodeTransaksi.toString()}',
                          style: primaryTextStyle.copyWith(
                            fontSize: 11.sp,
                            fontWeight: semiBold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            {'6', '8'}.contains(widget.kondisi)
                ? const SizedBox()
                : Container(
                    margin: EdgeInsets.only(
                      top: 2.sp,
                      left: 19.sp,
                      right: 19.sp,
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SelectableText(
                                ' ${widget.orders.namaReseller.toString()}',
                                style: primaryTextStyle.copyWith(
                                  fontSize: 11.sp,
                                  fontWeight: semiBold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
            Card(
              elevation: 2,
              margin: EdgeInsets.only(
                top: 12.sp,
                left: 19.sp,
                right: 19.sp,
              ),
              child: Container(
                padding: EdgeInsets.all(10.sp),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(4.sp),
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Waktu Order',
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                          ),
                        ),
                        Text(
                          widget.orders.waktuOrder.toString(),
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                            fontWeight: semiBold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4.sp,
                    ),
                    widget.orders.status.toString().replaceAll(' ', '') ==
                            'BelumBayar'.toString()
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Waktu Pembayaran',
                                style: primaryTextStyle.copyWith(
                                  fontSize: 9.5.sp,
                                ),
                              ),
                              CountdownTimer(
                                endTime: endTime,
                                widgetBuilder: (context, time) {
                                  if (time == null) {
                                    return Text(
                                      'Waktu Pembayaran Habis',
                                      style: primaryTextStyle.copyWith(
                                        fontSize: 9.5.sp,
                                      ),
                                    );
                                  } else {
                                    return Text(
                                      ' ${time.days ?? 0} Hari, ${time.hours ?? 0}:${time.min ?? 0}:${time.sec}',
                                      style: primaryTextStyle.copyWith(
                                        fontSize: 9.5.sp,
                                      ),
                                    );
                                  }
                                },
                                onEnd: () {
                                  paymentTime();
                                },
                              ),
                            ],
                          )
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Status Pembayaran',
                                style: primaryTextStyle.copyWith(
                                  fontSize: 9.5.sp,
                                ),
                              ),
                              Text(
                                widget.orders.status.toString(),
                                style: primaryTextStyle.copyWith(
                                  fontSize: 9.5.sp,
                                ),
                              ),
                            ],
                          ),
                    SizedBox(
                      height: 4.sp,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Total Pembayaran',
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                          ),
                        ),
                        Text(
                          'Rp. ${rupiah(widget.orders.totalBayar.toString())}',
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                            fontWeight: semiBold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4.sp,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Metode Pembayaran',
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                          ),
                        ),
                        Text(
                          widget.orders.pembayaran.toString(),
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                            fontWeight: semiBold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4.sp,
                    ),
                  ],
                ),
              ),
            ),
            widget.orders.status.toString().replaceAll(' ', '') ==
                        'BelumBayar'.toString() &&
                    timePayment == true
                ? Container(
                    margin: EdgeInsets.only(
                      top: 8.sp,
                      left: 19.sp,
                      right: 19.sp,
                    ),
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () {
                              intruksi();
                            },
                            child: Text(
                              'Lihat Cara Pembayaran',
                              style: primaryTextStyle.copyWith(
                                fontSize: 9.sp,
                                fontWeight: semiBold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : const SizedBox(),
            widget.orders.status.toString().replaceAll(' ', '') ==
                        'BelumBayar'.toString() &&
                    timePayment == true
                ? Container(
                    margin: EdgeInsets.only(
                      top: 8.sp,
                      left: 19.sp,
                      right: 19.sp,
                    ),
                    child: Center(
                      child: Card(
                        child: Image.network(
                          '${urlFile}qrcode/${widget.orders.kodeTransaksi}.png',
                          width: 200.sp,
                        ),
                      ),
                    ),
                  )
                : const SizedBox(),
          ],
        ),
      );
    }

    Widget contentVirtualAcounnt() {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 10.sp),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20.sp),
          ),
          color: backgroundColor1,
        ),
        child: Column(
          children: [
            //Note: header
            Container(
              margin: EdgeInsets.only(
                top: 19.sp,
                left: 19.sp,
                right: 19.sp,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SelectableText(
                          ' Kode Transaksi : ${widget.orders.kodeTransaksi.toString()}',
                          style: primaryTextStyle.copyWith(
                            fontSize: 11.sp,
                            fontWeight: semiBold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            {'6', '8'}.contains(widget.kondisi)
                ? const SizedBox()
                : Container(
                    margin: EdgeInsets.only(
                      top: 2.sp,
                      left: 19.sp,
                      right: 19.sp,
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SelectableText(
                                ' ${widget.orders.namaReseller.toString()}',
                                style: primaryTextStyle.copyWith(
                                  fontSize: 11.sp,
                                  fontWeight: semiBold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
            Card(
              elevation: 2,
              margin: EdgeInsets.only(
                top: 12.sp,
                left: 19.sp,
                right: 19.sp,
              ),
              child: Container(
                padding: EdgeInsets.all(10.sp),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(4.sp),
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Waktu Order',
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                          ),
                        ),
                        Text(
                          widget.orders.waktuOrder.toString(),
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                            fontWeight: semiBold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4.sp,
                    ),
                    widget.orders.status.toString().replaceAll(' ', '') ==
                            'BelumBayar'.toString()
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Waktu Pembayaran',
                                style: primaryTextStyle.copyWith(
                                  fontSize: 9.5.sp,
                                ),
                              ),
                              CountdownTimer(
                                endTime: endTime,
                                widgetBuilder: (context, time) {
                                  if (time == null) {
                                    return Text(
                                      'Waktu Pembayaran Habis',
                                      style: primaryTextStyle.copyWith(
                                        fontSize: 9.5.sp,
                                      ),
                                    );
                                  } else {
                                    return Text(
                                      ' ${time.days ?? 0} Hari, ${time.hours ?? 0}:${time.min ?? 0}:${time.sec}',
                                      style: primaryTextStyle.copyWith(
                                        fontSize: 9.5.sp,
                                      ),
                                    );
                                  }
                                },
                                onEnd: () {
                                  paymentTime();
                                },
                              ),
                            ],
                          )
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Status Pembayaran',
                                style: primaryTextStyle.copyWith(
                                  fontSize: 9.5.sp,
                                ),
                              ),
                              Text(
                                widget.orders.status.toString(),
                                style: primaryTextStyle.copyWith(
                                  fontSize: 9.5.sp,
                                ),
                              ),
                            ],
                          ),
                    SizedBox(
                      height: 4.sp,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Total Pembayaran',
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                          ),
                        ),
                        Text(
                          'Rp. ${rupiah(widget.orders.totalBayar.toString())}',
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                            fontWeight: semiBold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4.sp,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Metode Pembayaran',
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                          ),
                        ),
                        Text(
                          widget.orders.pembayaran.toString(),
                          style: primaryTextStyle.copyWith(
                            fontSize: 9.5.sp,
                            fontWeight: semiBold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4.sp,
                    ),
                    widget.orders.status.toString().replaceAll(' ', '') ==
                                'BelumBayar'.toString() &&
                            timePayment == true
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'No Virtual Acount',
                                style: primaryTextStyle.copyWith(
                                  fontSize: 9.5.sp,
                                ),
                              ),
                              SelectableText(
                                widget.orders.nomor.toString(),
                                style: primaryTextStyle.copyWith(
                                  fontSize: 9.5.sp,
                                  fontWeight: semiBold,
                                ),
                              )
                            ],
                          )
                        : const SizedBox(),
                  ],
                ),
              ),
            ),
            widget.orders.status.toString().replaceAll(' ', '') ==
                        'BelumBayar'.toString() &&
                    timePayment == true
                ? Container(
                    margin: EdgeInsets.only(
                      top: 8.sp,
                      left: 19.sp,
                      right: 19.sp,
                    ),
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () {
                              intruksi();
                            },
                            child: Text(
                              'Lihat Cara Pembayaran',
                              style: primaryTextStyle.copyWith(
                                fontSize: 9.sp,
                                fontWeight: semiBold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : const SizedBox(),
          ],
        ),
      );
    }

    Widget content() {
      if (widget.orders.pembayaran == 'manual-transfer') {
        return contentManualTransfer();
      } else if (widget.orders.pembayaran == 'qris') {
        return contentQris();
      } else {
        return contentVirtualAcounnt();
      }
    }

    Widget footManualTransfer() {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.all(
          19.sp,
        ),
        child: Row(
          children: [
            widget.orders.status.toString().replaceAll(' ', '') ==
                        'BelumBayar'.toString() &&
                    timePayment == true
                ? Expanded(
                    child: Container(
                      height: 11.5.w,
                      child: TextButton(
                        onPressed: () {
                          uploadPembayaran();
                        },
                        style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(9.sp),
                          ),
                          backgroundColor: primaryColor,
                        ),
                        child: Text(
                          'Upload',
                          style: primaryTextStyle.copyWith(
                            fontSize: 11.sp,
                            fontWeight: semiBold,
                          ),
                        ),
                      ),
                    ),
                  )
                : const SizedBox(),
            SizedBox(
              width: 8.sp,
            ),
            Expanded(
              child: Container(
                height: 11.5.w,
                child: TextButton(
                  onPressed: () async {
                    if (await detailAlamat.getalamat(
                      token: token.toString(),
                      apikey: keyApi,
                      idKonsumen: idKonsumen.toString(),
                      idAlamat: widget.orders.idAlamat,
                    )) {
                      if (await ordersReport.getOrdersReport(
                        token: '',
                        apikey: keyApi,
                        idKonsumen: idKonsumen.toString(),
                        kondisi: '7',
                        kodeTrx: widget.orders.kodeTransaksi.toString(),
                        start: '0',
                        end: '9',
                      )) {
                        if (await Provider.of<DetailKeranjangProvider>(context,
                                listen: false)
                            .getDetailKeranjang(
                          token: token.toString(),
                          apikey: keyApi,
                          idKonsumen: idKonsumen.toString(),
                          kodeTrx: widget.orders.kodeTransaksi,
                        )) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DetailTransaksiPage(
                                widget.orders,
                                widget.kondisi,
                                widget.orders.idPenjualan.toString(),
                              ),
                            ),
                          );
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              backgroundColor: secondaryColor,
                              content: Text(
                                'Gagal Periksa Koneksi Internet Anda',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 12.sp),
                              ),
                            ),
                          );
                        }
                      }
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          backgroundColor: secondaryColor,
                          content: Text(
                            'Data Tidak Di Temukan, Periksa Koneksi Internet Anda !',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 12.sp),
                          ),
                        ),
                      );
                    }
                  },
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(9.sp),
                    ),
                    backgroundColor: primaryColor,
                  ),
                  child: Text(
                    'Detail Transaksi',
                    style: primaryTextStyle.copyWith(
                      fontSize: 11.sp,
                      fontWeight: semiBold,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget footVirtualAcounnt() {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.all(
          19.sp,
        ),
        child: Row(
          children: [
            widget.orders.status.toString().replaceAll(' ', '') ==
                    'BelumBayar'.toString()
                ? Expanded(
                    child: Container(
                      height: 11.5.w,
                      child: TextButton(
                        onPressed: () async {
                          if (await ordersReport.getOrdersReport(
                            token: '',
                            apikey: keyApi,
                            idKonsumen: idKonsumen.toString(),
                            kondisi: '8',
                            start: '0',
                            end: '9',
                          )) {
                            Navigator.pushNamed(context, '/orders-report');
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                backgroundColor: secondaryColor,
                                content: Text(
                                  'Gagal Periksa Koneksi Internet Anda',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 12.sp),
                                ),
                              ),
                            );
                          }
                        },
                        style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(9.sp),
                          ),
                          backgroundColor: primaryColor,
                        ),
                        child: Text(
                          'Cek Status',
                          style: primaryTextStyle.copyWith(
                            fontSize: 11.sp,
                            fontWeight: semiBold,
                          ),
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
            SizedBox(
              width: 8.sp,
            ),
            Expanded(
              child: Container(
                height: 11.5.w,
                child: TextButton(
                  onPressed: () async {
                    if (await detailAlamat.getalamat(
                      token: token.toString(),
                      apikey: keyApi,
                      idKonsumen: idKonsumen.toString(),
                      idAlamat: widget.orders.idAlamat,
                    )) {
                      if (await ordersReport.getOrdersReport(
                        token: '',
                        apikey: keyApi,
                        idKonsumen: idKonsumen.toString(),
                        kondisi: '7',
                        kodeTrx: widget.orders.kodeTransaksi.toString(),
                        start: '0',
                        end: '9',
                      )) {
                        if (await Provider.of<DetailKeranjangProvider>(context,
                                listen: false)
                            .getDetailKeranjang(
                          token: token.toString(),
                          apikey: keyApi,
                          idKonsumen: idKonsumen.toString(),
                          kodeTrx: widget.orders.kodeTransaksi,
                        )) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DetailTransaksiPage(
                                widget.orders,
                                widget.kondisi,
                                widget.orders.idPenjualan.toString(),
                              ),
                            ),
                          );
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              backgroundColor: secondaryColor,
                              content: Text(
                                'Gagal Periksa Koneksi Internet Anda',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 12.sp),
                              ),
                            ),
                          );
                        }
                      }
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          backgroundColor: secondaryColor,
                          content: Text(
                            'Data Tidak Di Temukan, Periksa Koneksi Internet Anda !',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 12.sp),
                          ),
                        ),
                      );
                    }
                  },
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(9.sp),
                    ),
                    backgroundColor: primaryColor,
                  ),
                  child: Text(
                    'Detail Transaksi',
                    style: primaryTextStyle.copyWith(
                      fontSize: 11.sp,
                      fontWeight: semiBold,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget footQris() {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.all(
          19.sp,
        ),
        child: Row(
          children: [
            widget.orders.status.toString().replaceAll(' ', '') ==
                    'BelumBayar'.toString()
                ? Expanded(
                    child: Container(
                      height: 11.5.w,
                      child: TextButton(
                        onPressed: () async {
                          if (await checkStatusqris(
                              widget.orders.kodeTransaksi.toString())) {
                            if (await ordersReport.getOrdersReport(
                              token: '',
                              apikey: keyApi,
                              idKonsumen: idKonsumen.toString(),
                              kondisi: '8',
                              start: '0',
                              end: '9',
                            )) {
                              Navigator.pushNamed(context, '/orders-report');
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  backgroundColor: secondaryColor,
                                  content: Text(
                                    'Gagal Periksa Koneksi Internet Anda',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 12.sp),
                                  ),
                                ),
                              );
                            }
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                backgroundColor: secondaryColor,
                                content: Text(
                                  'Gagal Periksa Koneksi Internet Anda',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 12.sp),
                                ),
                              ),
                            );
                          }
                        },
                        style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(9.sp),
                          ),
                          backgroundColor: primaryColor,
                        ),
                        child: Text(
                          'Cek Status',
                          style: primaryTextStyle.copyWith(
                            fontSize: 11.sp,
                            fontWeight: semiBold,
                          ),
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
            SizedBox(
              width: 8.sp,
            ),
            Expanded(
              child: Container(
                height: 11.5.w,
                child: TextButton(
                  onPressed: () async {
                    if (await detailAlamat.getalamat(
                      token: token.toString(),
                      apikey: keyApi,
                      idKonsumen: idKonsumen.toString(),
                      idAlamat: widget.orders.idAlamat,
                    )) {
                      if (await ordersReport.getOrdersReport(
                        token: '',
                        apikey: keyApi,
                        idKonsumen: idKonsumen.toString(),
                        kondisi: '7',
                        kodeTrx: widget.orders.kodeTransaksi.toString(),
                        start: '0',
                        end: '9',
                      )) {
                        if (await Provider.of<DetailKeranjangProvider>(context,
                                listen: false)
                            .getDetailKeranjang(
                          token: token.toString(),
                          apikey: keyApi,
                          idKonsumen: idKonsumen.toString(),
                          kodeTrx: widget.orders.kodeTransaksi,
                        )) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DetailTransaksiPage(
                                widget.orders,
                                widget.kondisi,
                                widget.orders.idPenjualan.toString(),
                              ),
                            ),
                          );
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              backgroundColor: secondaryColor,
                              content: Text(
                                'Gagal Periksa Koneksi Internet Anda',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 12.sp),
                              ),
                            ),
                          );
                        }
                      }
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          backgroundColor: secondaryColor,
                          content: Text(
                            'Data Tidak Di Temukan, Periksa Koneksi Internet Anda !',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 12.sp),
                          ),
                        ),
                      );
                    }
                  },
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(9.sp),
                    ),
                    backgroundColor: primaryColor,
                  ),
                  child: Text(
                    'Detail Transaksi',
                    style: primaryTextStyle.copyWith(
                      fontSize: 11.sp,
                      fontWeight: semiBold,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget foot() {
      if (widget.orders.pembayaran == 'manual-transfer') {
        return footManualTransfer();
      } else if (widget.orders.pembayaran == 'qris') {
        return footQris();
      } else {
        return footVirtualAcounnt();
      }
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
        backgroundColor: backgroundColor2,
        body: ListView(
          children: [
            header(),
            content(),
          ],
        ),
        bottomNavigationBar: foot(),
      );
    });
  }

  void _openGallery(BuildContext context) async {
    final pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
    );
    setState(() {
      imageFile = pickedFile!;
      final imageBytes = File(pickedFile.path.toString()).readAsBytesSync();
      stringImage = base64.encode(imageBytes);
    });

    Navigator.pop(context);
  }

  void _openCamera(BuildContext context) async {
    final pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
    );
    setState(() {
      imageFile = pickedFile!;
      final imageBytes = File(pickedFile.path.toString()).readAsBytesSync();
      stringImage = base64.encode(imageBytes);
    });
    Navigator.pop(context);
  }
}
