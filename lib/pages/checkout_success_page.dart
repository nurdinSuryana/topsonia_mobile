import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/provider/orders_report_provider.dart';
import 'package:topsonia_mobile/theme.dart';

class CheckoutSuccesPage extends StatefulWidget {
  const CheckoutSuccesPage({Key? key}) : super(key: key);

  @override
  State<CheckoutSuccesPage> createState() => _CheckoutSuccesPageState();
}

class _CheckoutSuccesPageState extends State<CheckoutSuccesPage> {
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;

  @override
  void initState() {
    validate().whenComplete(() => null);
    super.initState();
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
  }

  @override
  Widget build(BuildContext context) {
    OrdersReportProvider ordersReport =
        Provider.of<OrdersReportProvider>(context);

    PreferredSizeWidget header() {
      return AppBar(
        backgroundColor: backgroundColor1,
        centerTitle: true,
        title: const Text('Checkout Success'),
        elevation: 0,
      );
    }

    Widget content() {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/icon_empty_cart.png',
              width: 80,
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              'Pesanan Berhasil Di Checkout',
              style: primaryTextStyle.copyWith(
                fontSize: 16,
                fontWeight: medium,
              ),
            ),
            // const SizedBox(
            //   height: 12,
            // ),
            // Text(
            //   'Pesanan Berhasil Di Checkout',
            //   style: secondaryTextStyle,
            //   textAlign: TextAlign.center,
            // ),
            Container(
              width: 196,
              height: 44,
              margin: EdgeInsets.only(top: defaultMargin),
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(
                    context,
                    '/home',
                    (route) => false,
                  );
                },
                style: TextButton.styleFrom(
                  backgroundColor: primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                ),
                child: Text(
                  'Beli Produk Lain',
                  style: primaryTextStyle.copyWith(
                    fontSize: 16,
                    fontWeight: medium,
                  ),
                ),
              ),
            ),
            Container(
              width: 196,
              height: 44,
              margin: EdgeInsets.only(top: 10.sp),
              child: TextButton(
                onPressed: () async {
                  if (await ordersReport.getOrdersReport(
                    token: '',
                    apikey: keyApi,
                    idKonsumen: idKonsumen.toString(),
                    kondisi: '8',
                    start: '0',
                    end: '9',
                  )) {
                    Navigator.pushNamed(context, '/orders-report');
                  } else {
                    // Navigator.pushNamed(context, '/orders-report');
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        backgroundColor: secondaryColor,
                        content: Text(
                          'Periksa Koneksi Internet Anda',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                      ),
                    );
                  }
                },
                style: TextButton.styleFrom(
                  backgroundColor: const Color(0xff39374B),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.sp),
                  ),
                ),
                child: Text(
                  'Daftar Pesanan',
                  style: primaryTextStyle.copyWith(
                    fontSize: 16,
                    fontWeight: medium,
                    color: const Color(0xffB7B6Bf),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
        backgroundColor: backgroundColor3,
        appBar: header(),
        body: content(),
      );
    });
  }
}
