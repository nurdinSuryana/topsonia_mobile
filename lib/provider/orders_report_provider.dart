import 'package:flutter/widgets.dart';
import 'package:topsonia_mobile/models/orders_report_model.dart';
import 'package:topsonia_mobile/services/orders_report_service.dart';

class OrdersReportProvider with ChangeNotifier {
  List<OrdersReportModel> _ordersReport = [];
  List<OrdersReportModel> get ordersReport => _ordersReport;

  set ordersReport(List<OrdersReportModel> ordersReport) {
    _ordersReport = ordersReport;
    notifyListeners();
  }

  Future<bool> getOrdersReport({
    required String token,
    required String apikey,
    required String idKonsumen,
    required String kondisi,
    String? kodeTrx,
    String? start,
    String? end,
  }) async {
    try {
      List<OrdersReportModel> report = await OrdersReportService().getDetail(
        token: token,
        apikey: apikey,
        idKonsumen: idKonsumen,
        kodeTrx: kodeTrx,
        kondisi: kondisi,
        start: start,
        end: end,
      );
      _ordersReport = report;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  // Future<bool> setOrdersReport({
  //   required List report,
  // }) async {
  //   _ordersReport = report;
  //   return true;
  // }
}
