import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:topsonia_mobile/models/wishlist_model.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:http/http.dart' as http;

class WishlistProvider with ChangeNotifier {
  List<WishlistModel> _wishlist = [];
  List<WishlistModel> get wishlist => _wishlist;

  set wishlist(List<WishlistModel> wishlist) {
    _wishlist = wishlist;
    notifyListeners();
  }

  Future<bool> getWishlist({
    required String token,
    required String apikey,
    required String idKonsumen,
    String? kondisi,
    String? idProduk,
  }) async {
    try {
      var url = '${urlApi}product/wishlistData';
      var headers = {
        'Content-Type': 'application/json',
        'Topsonia-X-Key': apikey
      };
      var body = jsonEncode({
        'token': token,
        'id_konsumen': idKonsumen,
        'kondisi': kondisi,
        'id_produk': idProduk,
      });
      var response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );
      print(response.body);
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['wishlist'];
        List<WishlistModel> wishlist = [];
        print(jsonDecode(response.body));
        if (data.length > 0) {
          for (var item in data) {
            wishlist.add(WishlistModel.fromJson(item));
          }
        }
        _wishlist = wishlist;
        // print(Wishlist);
        return true;
      } else {
        throw Exception('Gagal');
      }
    } catch (e) {
      print(e);
      return false;
    }
  }
}
