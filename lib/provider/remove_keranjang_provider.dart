import 'package:flutter/widgets.dart';
import 'package:topsonia_mobile/models/error_model.dart';
import 'package:topsonia_mobile/services/remove_keranjang_service.dart';

class RemoveKeranjangProvider with ChangeNotifier {
  Future<ErrorModel> remove({
    required String token,
    required String idProduk,
    required String apikey,
    required String idKonsumen,
  }) async {
    try {
      ErrorModel errorr = await RemoveKeranjangService().remove(
        token: token,
        idProduk: idProduk,
        apikey: apikey,
        idKonsumen: idKonsumen,
      );
      // ErrorModel errorr = ErrorModel.error();
      return errorr;
    } catch (e) {
      ErrorModel errorr = ErrorModel.error();
      return errorr;
    }
  }
}
