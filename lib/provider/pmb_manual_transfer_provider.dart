import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:topsonia_mobile/models/error_model.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:http/http.dart' as http;

class PmbManualTransferProvider with ChangeNotifier {
  Future<ErrorModel> upload({
    required String token,
    required String apikey,
    required String idKonsumen,
    required String buktiTransfer,
    required String kodeTransaksi,
    required String noRekeningPenerima,
    required String nominal,
    required String noRekeningPengirim,
    required String namaPengirim,
    required String tanggalTransfer,
  }) async {
    try {
      var url = '$urlApi/pembayaran/manual_transfer';
      var headers = {
        'Content-Type': 'application/json',
        'Topsonia-X-Key': apikey
      };
      var body = jsonEncode({
        'token': token,
        'id_konsumen': idKonsumen,
        'kode_transaksi': kodeTransaksi,
        'bukti_transfer': buktiTransfer,
        'no_rekening_pen': noRekeningPenerima,
        'nominal': nominal,
        'rekening_pengirim': noRekeningPengirim,
        'nama_pengirim': namaPengirim,
        'tanggal_transfer': tanggalTransfer,
      });
      // print(noRekeningPenerima);
      var response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );

      var data = jsonDecode(response.body);
      if (response.statusCode == 200) {
        ErrorModel error = ErrorModel.fromJson(data);
        return error;
      } else {
        print('bisa 1');
        ErrorModel error = ErrorModel.error();
        return error;
      }
    } catch (e) {
      print('bisa 2');
      ErrorModel errorr = ErrorModel.error();
      return errorr;
    }
  }
}
