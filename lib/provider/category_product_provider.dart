import 'package:flutter/widgets.dart';
import 'package:topsonia_mobile/models/category_product_model.dart';
import 'package:topsonia_mobile/services/product_category_service.dart';

class CategoryProductProvider with ChangeNotifier {
  List<CategoryProductModel> _categoryProducts = [];
  List<CategoryProductModel> get categoryProducts => _categoryProducts;

  set categoryProducts(List<CategoryProductModel> categoryProducts) {
    _categoryProducts = categoryProducts;
    notifyListeners();
  }

  Future<bool> getcategoryProducts({
    required String apikey,
  }) async {
    try {
      List<CategoryProductModel> categoryProducts =
          await CategoryProductService().getCategory(apikey: apikey);
      _categoryProducts = categoryProducts;
      return true;
    } catch (e) {
      // ignore: avoid_print
      print(e);
      return false;
    }
  }
}
