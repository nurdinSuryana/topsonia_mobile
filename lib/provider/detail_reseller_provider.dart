import 'package:flutter/widgets.dart';
import 'package:topsonia_mobile/models/detail_reseller_model.dart';
import 'package:topsonia_mobile/services/detail_reseller_service.dart';

class DetailResellerProvider with ChangeNotifier {
  List<DetailResellerModel> _detailReseller = [];
  List<DetailResellerModel> get detailReseller => _detailReseller;

  set detailReseller(List<DetailResellerModel> detailReseller) {
    _detailReseller = detailReseller;
    notifyListeners();
  }

  Future<bool> getDetailReseller(
      {required String token,
      required String apikey,
      required String idKonsumen}) async {
    try {
      List<DetailResellerModel> detailReseller =
          await DetailResellerService().getDetail(
        token: token,
        apikey: apikey,
        idKonsumen: idKonsumen,
      );
      _detailReseller = detailReseller;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
