import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:topsonia_mobile/models/kurir_data_model.dart';
import 'package:topsonia_mobile/theme.dart';

class KurirDataProvider with ChangeNotifier {
  List<KurirDataModel> _kurirData = [];
  List<KurirDataModel> get kurirData => _kurirData;

  set kurirData(List<KurirDataModel> kurirData) {
    _kurirData = kurirData;
    notifyListeners();
  }

  Future<bool> kurir({
    required String apikey,
    required String kotaPengirim,
    required String kotaPenerima,
    required String berat,
    required String idKonsumen,
    required String totalBelanja,
  }) async {
    try {
      var url = '$urlApi/checkout/kurirdata';
      var headers = {
        'Content-Type': 'application/json',
        'Topsonia-X-Key': apikey
      };
      var body = jsonEncode({
        'token': '',
        'id_konsumen': idKonsumen,
        'penerima': kotaPenerima,
        'pengirim': kotaPengirim,
        'total_belanja': totalBelanja,
        'berat': berat,
      });
      var response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );
      if (response.statusCode == 200) {
        if (jsonDecode(response.body)['status_code'] == 200) {
          var data = jsonDecode(response.body)['data'];
          List<KurirDataModel> kurirdata = [];
          // ignore: avoid_print
          for (var item in data) {
            kurirdata.add(KurirDataModel.fromJson(item));
          }
          _kurirData = kurirdata;
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (e) {
      // ignore: avoid_print
      print(e);
      return false;
    }
  }
}
