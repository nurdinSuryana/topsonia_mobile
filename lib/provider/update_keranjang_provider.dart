import 'package:flutter/widgets.dart';
import 'package:topsonia_mobile/models/error_model.dart';
import 'package:topsonia_mobile/services/update_keranjang_service.dart';

class UpdateKeranjangProvider with ChangeNotifier {
  Future<ErrorModel> update({
    required String token,
    required String idProduk,
    required String apikey,
    required String idKonsumen,
    required String qty,
  }) async {
    try {
      ErrorModel errorr = await UpdateKeranjangService().update(
        token: token,
        idProduk: idProduk,
        apikey: apikey,
        idKonsumen: idKonsumen,
        qty: qty,
      );
      return errorr;
    } catch (e) {
      ErrorModel errorr = ErrorModel.error();
      return errorr;
    }
  }
}
