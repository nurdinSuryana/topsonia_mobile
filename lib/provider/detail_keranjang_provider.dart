import 'package:flutter/widgets.dart';
import 'package:topsonia_mobile/models/detail_keranjang_model.dart';
import 'package:topsonia_mobile/services/detail_keranjang_service.dart';

class DetailKeranjangProvider with ChangeNotifier {
  List<DetailKeranjangModel> _detailKeranjang = [];
  List<DetailKeranjangModel> get detailKeranjang => _detailKeranjang;

  set detailKeranjang(List<DetailKeranjangModel> detailKeranjang) {
    _detailKeranjang = detailKeranjang;
    notifyListeners();
  }

  Future<bool> getDetailKeranjang({
    required String token,
    required String apikey,
    required String idKonsumen,
    String? kodeTrx,
    String? idp,
  }) async {
    try {
      List<DetailKeranjangModel> detailKeranjang =
          await DetailKeranjangService().getDetail(
        token: token,
        apikey: apikey,
        idKonsumen: idKonsumen,
        kodeTrx: kodeTrx,
        idp: idp,
      );
      _detailKeranjang = detailKeranjang;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
