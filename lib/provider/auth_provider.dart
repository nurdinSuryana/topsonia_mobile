import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:flutter_session_manager/flutter_session_manager.dart';
import 'package:topsonia_mobile/models/user_model.dart';
import 'package:topsonia_mobile/services/auth_service.dart';

class AuthProvider with ChangeNotifier {
  UserModel _user = UserModel.kosong();
  UserModel get user => _user;

  set user(UserModel user) {
    _user = user;
    notifyListeners();
  }
  // late UserModel _user;

  // UserModel get user => _user;

  // set user(UserModel user) {
  //   _user = user;
  //   notifyListeners();
  // }

  Future<bool> login({
    required String username,
    required String password,
    required String keyapi,
  }) async {
    try {
      UserModel user = await AuthService().login(
        keyapi: keyapi,
        username: username,
        password: password,
      );
      _user = user;
      final SharedPreferences share = await SharedPreferences.getInstance();
      // share.setString('idKonsumen', 'bisa');
      share.setString("idKonsumen", user.idKonsumen.toString());
      share.setString("namaKonsumen", user.nama.toString());
      share.setString("username", user.username.toString());
      share.setString("token", user.token.toString());
      return true;
    } catch (e) {
      return false;
    }
  }
}
