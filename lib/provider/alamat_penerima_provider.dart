import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:topsonia_mobile/models/alamat_penerima_model.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:http/http.dart' as http;

class AlamatPenerimaProvider with ChangeNotifier {
  AlamatPenerimaModel _alamatPenerima = AlamatPenerimaModel.kosong();
  AlamatPenerimaModel get alamatPenerima => _alamatPenerima;

  set alamatPenerima(AlamatPenerimaModel alamatPenerima) {
    _alamatPenerima = alamatPenerima;
    notifyListeners();
  }

  Future<bool> getalamat({
    required String apikey,
    required String idKonsumen,
    required String token,
    String? idAlamat,
  }) async {
    try {
      var url = '$urlApi/auth/alamat_konsumen';
      var headers = {
        'Content-Type': 'application/json',
        'Topsonia-X-Key': apikey
      };
      var body = jsonEncode({
        'id_konsumen': idKonsumen,
        'id_alamat': idAlamat,
      });
      var response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['alamat'];
        AlamatPenerimaModel alamat = AlamatPenerimaModel.fromJson(data);
        _alamatPenerima = alamat;
        return true;
      } else {
        throw Exception('Gagal');
      }
    } catch (e) {
      print(e);
      return false;
    }
  }
}
