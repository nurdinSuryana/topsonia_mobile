import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:topsonia_mobile/theme.dart';

class ProsesCheckoutProvider with ChangeNotifier {
  Future<bool> getProsesCheckouts({
    required String token,
    required String apikey,
    required String idKonsumen,
    required List reseller,
    required List produk,
    required int totalBayar,
  }) async {
    try {
      var url = '$urlApi/checkout/proses';
      var headers = {
        'Content-Type': 'application/json',
        'Topsonia-X-Key': apikey
      };
      var body = jsonEncode({
        'token': token,
        'id_konsumen': idKonsumen,
        'reseller': json.encode(reseller),
        'produk': json.encode(produk),
        'totalBayar': totalBayar,
      });
      var response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );
      //print(response.body);

      if (response.statusCode == 200) {
        if (jsonDecode(response.body)['status_code'] == 200) {
          var data = jsonDecode(response.body)['payment'];
          print(jsonDecode(response.body));
          return true;
        } else {
          return false;
        }
      } else {
        throw Exception(response.statusCode);
      }
    } catch (e) {
      // ignore: avoid_print
      print(e);
      return false;
    }
  }
}
