import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:topsonia_mobile/models/payment_model.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:http/http.dart' as http;

class PaymentProvider with ChangeNotifier {
  List<PaymentModel> _payment = [];
  List<PaymentModel> get payment => _payment;

  set payment(List<PaymentModel> payment) {
    _payment = payment;
    notifyListeners();
  }

  Future<bool> getPayment({
    required String token,
    required String apikey,
    required int total,
  }) async {
    try {
      var url = '$urlApi/pembayaran/mdr';
      var headers = {
        'Content-Type': 'application/json',
        'Topsonia-X-Key': apikey
      };
      var body = jsonEncode({
        'token': token,
        'total': total,
      });
      var response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );
      //print(response.body);

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['payment'];
        List<PaymentModel> payment = [];
        print(jsonDecode(response.body));
        for (var item in data) {
          payment.add(PaymentModel.fromJson(item));
        }
        _payment = payment;
        // print(payment);
        return true;
      } else {
        throw Exception('Gagal');
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<String> getmdr({
    required String token,
    required String apikey,
    required String pembayaran,
    required int total,
  }) async {
    try {
      var url = '$urlApi/pembayaran/getmdr';
      var headers = {
        'Content-Type': 'application/json',
        'Topsonia-X-Key': apikey
      };
      var body = jsonEncode({
        'token': token,
        'total': total,
        'pembayaran': pembayaran,
      });
      var response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );
      //print(response.body);

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['payment'];
        // print(data);
        // _payment = payment;
        return 'bisa';
      } else {
        throw Exception('Gagal');
      }
    } catch (e) {
      print(e);
      return '0';
    }
  }
}
