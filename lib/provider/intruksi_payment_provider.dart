import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:topsonia_mobile/models/intruksi_payment_model.dart';
import 'package:topsonia_mobile/theme.dart';

class IntruksiPaymentProvider with ChangeNotifier {
  List<IntruksiPaymentModel> _intruksiPayment = [];
  List<IntruksiPaymentModel> get intruksiPayment => _intruksiPayment;

  set intruksiPayment(List<IntruksiPaymentModel> intruksiPayment) {
    _intruksiPayment = intruksiPayment;
    notifyListeners();
  }

  Future<bool> get({
    required String apikey,
    required String token,
    required String payment,
  }) async {
    try {
      var url = '$urlApi/pembayaran/intruksi';
      var headers = {
        'Content-Type': 'application/json',
        'Topsonia-X-Key': apikey
      };
      var body = jsonEncode({
        'token': '',
        'payment': payment,
      });
      var response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );
      if (response.statusCode == 200) {
        if (jsonDecode(response.body)['status_code'] == 200) {
          var data = jsonDecode(response.body)['intruksi'];
          List<IntruksiPaymentModel> intruksiPayment = [];
          for (var item in data) {
            intruksiPayment.add(IntruksiPaymentModel.fromJson(item));
          }
          _intruksiPayment = intruksiPayment;
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (e) {
      // ignore: avoid_print
      print(e);
      return false;
    }
  }
}
