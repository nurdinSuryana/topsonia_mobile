import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:topsonia_mobile/models/user_model.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:http/http.dart' as http;

class RegisterProvider with ChangeNotifier {
  UserModel _user = UserModel.kosong();
  UserModel get user => _user;

  set user(UserModel user) {
    _user = user;
    notifyListeners();
  }

  Future<bool> register({
    required String keyapi,
    required String username,
    required String password,
    required String namaLengkap,
    required String email,
    required String provinsi,
    required String kota,
    required String kecamatan,
    required String kodePos,
    required String alamatLengkap,
    required String noHp,
  }) async {
    try {
      var url = '$urlApi/auth/register';
      var headers = {
        'Content-Type': 'application/json',
        'Topsonia-X-Key': keyapi
      };
      var body = jsonEncode({
        'username': username,
        'password': password,
        'nama_lengkap': namaLengkap,
        'email': email,
        'alamat_lengkap': alamatLengkap,
        'provinsi_id': provinsi,
        'kota_id': kota,
        'kecamatan': kecamatan,
        'kode_pos': kodePos,
        'no_hp': noHp,
      });

      var response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );

      var data = jsonDecode(response.body);
      final SharedPreferences share = await SharedPreferences.getInstance();
      share.setString("idKonsumen", data['user']['id_konsumen'].toString());
      share.setString("namaKonsumen", data['user']['nama_lengkap'].toString());
      share.setString("username", data['user']['username'].toString());
      share.setString("token", data['token'].toString());
      _user = UserModel.fromJson(data);
      return true;
    } catch (e) {
      return false;
    }
  }
}
