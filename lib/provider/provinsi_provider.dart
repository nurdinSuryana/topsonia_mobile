import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:topsonia_mobile/models/provinsi_model.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:http/http.dart' as http;

class ProvinsiProvider with ChangeNotifier {
  List<ProvinsiModel> _provinsi = [];
  List<ProvinsiModel> get provinsi => _provinsi;

  set provinsi(List<ProvinsiModel> provinsi) {
    _provinsi = provinsi;
    notifyListeners();
  }

  Future<bool> getProvinsi({
    required String token,
    required String apikey,
  }) async {
    try {
      var url = '$urlApi/auth/provinsi';
      var headers = {
        'Content-Type': 'application/json',
        'Topsonia-X-Key': apikey
      };
      var body = jsonEncode({
        'token': token,
      });
      var response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );
      //print(response.body);
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['provinsi'];
        List<ProvinsiModel> provinsi = [];
        print(jsonDecode(response.body));
        for (var item in data) {
          provinsi.add(ProvinsiModel.fromJson(item));
        }
        _provinsi = provinsi;
        // print(Provinsi);
        return true;
      } else {
        throw Exception('Gagal');
      }
    } catch (e) {
      print(e);
      return false;
    }
  }
}
