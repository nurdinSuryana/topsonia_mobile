import 'package:flutter/widgets.dart';
import 'package:topsonia_mobile/models/product_model.dart';
import 'package:topsonia_mobile/services/product_service.dart';

class ProductProvider with ChangeNotifier {
  List<ProductModel> _products = [];
  List<ProductModel> get products => _products;

  set products(List<ProductModel> products) {
    _products = products;
    notifyListeners();
  }

  Future<bool> getProducts({
    required String searchProd,
    required String apikey,
    categoryproduct,
    required String start,
    required String limit,
  }) async {
    try {
      List<ProductModel> products = await ProductService().getProducts(
        searchProd: searchProd,
        apikey: apikey,
        categoryproduct: categoryproduct,
        start: start,
        limit: limit,
      );

      _products = products;
      return true;
    } catch (e) {
      // ignore: avoid_print
      print(e);
      return false;
    }
  }
}
