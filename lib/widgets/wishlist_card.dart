import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/global_model.dart';
import 'package:topsonia_mobile/models/product_model.dart';
import 'package:topsonia_mobile/models/wishlist_model.dart';
import 'package:topsonia_mobile/pages/product_page.dart';
import 'package:topsonia_mobile/provider/product_provider.dart';
import 'package:topsonia_mobile/provider/wishlist_provider.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:http/http.dart' as http;

class WishLisctCard extends StatefulWidget {
  final WishlistModel wishlist;
  const WishLisctCard(this.wishlist, {Key? key}) : super(key: key);

  @override
  State<WishLisctCard> createState() => _WishLisctCardState();
}

class _WishLisctCardState extends State<WishLisctCard> {
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;
  List produk = [];

  @override
  void initState() {
    validate().whenComplete(() => null);
    super.initState();
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
  }

  @override
  Widget build(BuildContext context) {
    ProductProvider productProvider = Provider.of<ProductProvider>(context);
    produk = productProvider.products;
    WishlistProvider wish = Provider.of<WishlistProvider>(context);

    return Sizer(builder: (context, orientation, deviceType) {
      return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.sp),
        ),
        color: backgroundColor1,
        elevation: 2,
        child: Container(
          margin: EdgeInsets.only(top: 18.sp),
          padding: EdgeInsets.only(
            left: 11.sp,
            bottom: 13.sp,
            right: 18.sp,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12.sp),
            color: backgroundColor1,
          ),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(12.sp),
                child: Image.network(
                  '${urlFile}foto_produk/${widget.wishlist.gambar.toString().split(';').first}',
                  width: 19.w,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                width: 4.w,
              ),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    if (produk
                        .where((x) =>
                            x.id.toString() ==
                            widget.wishlist.idProduk.toString())
                        .isNotEmpty) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ProductPage(produk
                              .where((x) =>
                                  x.id.toString() ==
                                  widget.wishlist.idProduk.toString())
                              .first),
                        ),
                      );
                    } else {
                      if (getproduct(widget.wishlist.idProduk)) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ProductPage(produk
                                .where((x) =>
                                    x.id.toString() ==
                                    widget.wishlist.idProduk.toString())
                                .first),
                          ),
                        );
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            backgroundColor: secondaryColor,
                            content: Text(
                              'Periksa Koneksi Internet Anda',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 12.sp),
                            ),
                          ),
                        );
                      }
                    }
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.wishlist.namaProduk.toString(),
                        style: primaryTextStyle.copyWith(
                          fontWeight: semiBold,
                          fontSize: 14.sp,
                        ),
                      ),
                      Text(
                        'Rp. ${rupiah(widget.wishlist.harga.toString())}',
                        style: priceTextStyle.copyWith(
                          fontSize: 12.sp,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () async {
                  if (await wish.getWishlist(
                    token: '',
                    apikey: keyApi,
                    idKonsumen: idKonsumen.toString(),
                    kondisi: '1',
                    idProduk: widget.wishlist.idProduk,
                  )) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        backgroundColor: secondaryColor,
                        content: Text(
                          'Berhasil Menghapus Dari Daftar Wishlist',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                      ),
                    );
                    Navigator.pushNamed(context, '/home');
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        backgroundColor: secondaryColor,
                        content: Text(
                          'Periksa Koneksi Internet Anda',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                      ),
                    );
                  }
                },
                child: Image.asset(
                  'assets/button_wishlist_blue.png',
                  width: 9.5.w,
                ),
              )
            ],
          ),
        ),
      );
    });
  }

  getproduct(idProduk) async {
    var url = '$urlApi/product/data?id_produk=$idProduk';
    var headers = {
      'Content-Type': 'application/json',
      'Topsonia-X-Key': keyApi
    };

    var response = await http.get(
      Uri.parse(url),
      headers: headers,
    );
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['product'];
      // ignore: avoid_print
      print(jsonDecode(response.body));

      setState(() {
        for (var item in data) {
          produk.add(ProductModel.fromJson(item));
        }
      });
    } else {
      throw Exception('Gagal');
    }
  }
}
