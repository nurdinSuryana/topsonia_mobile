import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/global_model.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/orders_report_provider.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:topsonia_mobile/widgets/checkout_card.dart';

class DetailTransaksiProduk extends StatefulWidget {
  final String kondisi;
  final String idp;
  const DetailTransaksiProduk(this.kondisi, this.idp, {Key? key})
      : super(key: key);

  @override
  State<DetailTransaksiProduk> createState() => _DetailTransaksiProdukState();
}

class _DetailTransaksiProdukState extends State<DetailTransaksiProduk> {
  @override
  Widget build(BuildContext context) {
    final keranjangProvider =
        Provider.of<DetailKeranjangProvider>(context).detailKeranjang;
    OrdersReportProvider ordersdetail =
        Provider.of<OrdersReportProvider>(context);

    Widget itemAll() {
      return Container(
        margin: EdgeInsets.only(top: 11.sp),
        width: 100.sp,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: ordersdetail.ordersReport
              .map(
                (reseller) => Card(
                  color: backgroundColor1,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.sp),
                  ),
                  elevation: 2,
                  margin: EdgeInsets.only(top: 20.sp),
                  child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: backgroundColor1,
                          borderRadius: BorderRadius.circular(10.sp),
                        ),
                        padding: EdgeInsets.only(top: 20.sp),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 12.sp),
                              child: Text(
                                reseller.namaReseller.toString(),
                                textAlign: TextAlign.start,
                                style: primaryTextStyle.copyWith(
                                  fontSize: 11.sp,
                                  fontWeight: medium,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 12.sp),
                              child: Text(
                                reseller.kota.toString(),
                                textAlign: TextAlign.left,
                                style: primaryTextStyle.copyWith(
                                  fontSize: 8.sp,
                                  fontWeight: medium,
                                ),
                              ),
                            ),
                            Column(
                                children: keranjangProvider
                                    .where((x) => x.namaReseller!
                                        .toLowerCase()
                                        .contains(reseller.namaReseller
                                            .toString()
                                            .toLowerCase()))
                                    .map(
                                      (e) => CheckoutCard(e),
                                    )
                                    .toList()),
                            Container(
                              padding: EdgeInsets.only(left: 16.sp),
                              child: Text(
                                'Total Belanja : Rp. ${rupiah(int.parse(reseller.totalBelanja.toString()))}',
                                textAlign: TextAlign.start,
                                style: priceTextStyle.copyWith(
                                  fontSize: 9.sp,
                                  fontWeight: medium,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 16.sp),
                              child: Text(
                                'Ongkir : Rp. ${rupiah(int.parse(reseller.ongkir == '' ? '0' : reseller.ongkir.toString()) + int.parse(reseller.potonganOngkir == '' ? '0' : reseller.potonganOngkir.toString()))}',
                                textAlign: TextAlign.start,
                                style: priceTextStyle.copyWith(
                                  fontSize: 9.sp,
                                  fontWeight: medium,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 16.sp),
                              child: Text(
                                'Subtotal : Rp. ${rupiah(int.parse(reseller.totalBelanja.toString()) + int.parse(reseller.ongkir == '' ? '0' : reseller.ongkir.toString()) + int.parse(reseller.potonganOngkir == '' ? '0' : reseller.potonganOngkir.toString()))}',
                                textAlign: TextAlign.start,
                                style: priceTextStyle.copyWith(
                                  fontSize: 10.sp,
                                  fontWeight: medium,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20.sp,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
              .toList(),
        ),
      );
    }

    Widget itemPerSeller() {
      return Container(
        margin: EdgeInsets.only(top: 11.sp),
        width: 100.sp,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: ordersdetail.ordersReport
              .where((w) => w.idPenjualan == widget.idp)
              .map(
                (reseller) => Card(
                  color: backgroundColor1,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.sp),
                  ),
                  elevation: 2,
                  margin: EdgeInsets.only(top: 20.sp),
                  child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: backgroundColor1,
                          borderRadius: BorderRadius.circular(10.sp),
                        ),
                        padding: EdgeInsets.only(top: 20.sp),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 12.sp),
                              child: Text(
                                reseller.namaReseller.toString(),
                                textAlign: TextAlign.start,
                                style: primaryTextStyle.copyWith(
                                  fontSize: 11.sp,
                                  fontWeight: medium,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 12.sp),
                              child: Text(
                                reseller.kota.toString(),
                                textAlign: TextAlign.left,
                                style: primaryTextStyle.copyWith(
                                  fontSize: 8.sp,
                                  fontWeight: medium,
                                ),
                              ),
                            ),
                            Column(
                                children: keranjangProvider
                                    .where((x) => x.namaReseller!
                                        .toLowerCase()
                                        .contains(reseller.namaReseller
                                            .toString()
                                            .toLowerCase()))
                                    .map(
                                      (e) => CheckoutCard(e),
                                    )
                                    .toList()),
                            Container(
                              padding: EdgeInsets.only(left: 16.sp),
                              child: Text(
                                'Total Belanja : Rp. ${rupiah(int.parse(reseller.totalBelanja.toString()))}',
                                textAlign: TextAlign.start,
                                style: priceTextStyle.copyWith(
                                  fontSize: 9.sp,
                                  fontWeight: medium,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 16.sp),
                              child: Text(
                                'Ongkir : Rp. ${rupiah(int.parse(reseller.ongkir == '' ? '0' : reseller.ongkir.toString()) + int.parse(reseller.potonganOngkir == '' ? '0' : reseller.potonganOngkir.toString()))}',
                                textAlign: TextAlign.start,
                                style: priceTextStyle.copyWith(
                                  fontSize: 9.sp,
                                  fontWeight: medium,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 16.sp),
                              child: Text(
                                'Subtotal : Rp. ${rupiah(int.parse(reseller.totalBelanja.toString()) + int.parse(reseller.ongkir == '' ? '0' : reseller.ongkir.toString()) + int.parse(reseller.potonganOngkir == '' ? '0' : reseller.potonganOngkir.toString()))}',
                                textAlign: TextAlign.start,
                                style: priceTextStyle.copyWith(
                                  fontSize: 10.sp,
                                  fontWeight: medium,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20.sp,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
              .toList(),
        ),
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Container(
        child:
            {'6', '8'}.contains(widget.kondisi) ? itemAll() : itemPerSeller(),
      );
    });
  }
}
