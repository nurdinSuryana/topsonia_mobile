import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/detail_keranjang_model.dart';
import 'package:topsonia_mobile/models/error_model.dart';
import 'package:topsonia_mobile/models/global_model.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/remove_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/update_keranjang_provider.dart';
import 'package:topsonia_mobile/theme.dart';

class CartCard extends StatefulWidget {
  final DetailKeranjangModel keranjang;
  const CartCard(this.keranjang, {Key? key}) : super(key: key);

  @override
  State<CartCard> createState() => _CartCardState();
}

class _CartCardState extends State<CartCard> {
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;

  @override
  void initState() {
    validate().whenComplete(() => null);
    super.initState();
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
  }

  @override
  Widget build(BuildContext context) {
    final qtyController = TextEditingController();
    qtyController.text = widget.keranjang.jumlah.toString();

    Widget cekStok() {
      if (int.parse(widget.keranjang.stok.toString()) < 1) {
        return Container(
          padding: EdgeInsets.only(top: 10.sp),
          child: Row(
            children: [
              Text(
                'Stok Habis',
                style: alertTextStyle.copyWith(
                  fontSize: 9.sp,
                  fontWeight: light,
                ),
              ),
            ],
          ),
        );
      } else if (int.parse(widget.keranjang.jumlah.toString()) >
          int.parse(widget.keranjang.stok.toString())) {
        return Container(
          padding: EdgeInsets.only(top: 10.sp),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'jumlah Qty Melebihi Stok Barang',
                style: alertTextStyle.copyWith(
                  fontSize: 9.sp,
                  fontWeight: light,
                ),
              ),
              // SizedBox(
              //   width: 21.w,
              // ),
              Text(
                'Sisa Stock : ${widget.keranjang.stok}',
                style: alertTextStyle.copyWith(
                  fontSize: 9.sp,
                  fontWeight: light,
                  color: Colors.green,
                ),
              )
            ],
          ),
        );
      } else {
        return const SizedBox();
      }
    }

    Future<void> handleRemove() async {
      ErrorModel eror = await Provider.of<RemoveKeranjangProvider>(
        context,
        listen: false,
      ).remove(
        token: token.toString(),
        idProduk: widget.keranjang.id.toString(),
        apikey: keyApi,
        idKonsumen: idKonsumen.toString(),
      );

      if (eror.statusCode.toString() == '200') {
        await Provider.of<DetailKeranjangProvider>(context, listen: false)
            .getDetailKeranjang(
          token: token.toString(),
          apikey: keyApi,
          idKonsumen: idKonsumen.toString(),
        );
        Navigator.pushNamed(context, '/cart');

        return showDialog(
          context: context,
          builder: (BuildContext context) => Container(
            width: 20.w,
            child: AlertDialog(
              backgroundColor: backgroundColor3,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.sp),
              ),
              content: SingleChildScrollView(
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.close,
                          color: primaryTextColor,
                        ),
                      ),
                    ),
                    Image.asset(
                      'assets/icon_success.png',
                      width: 20.w,
                    ),
                    SizedBox(
                      height: 10.sp,
                    ),
                    Text(
                      'Success',
                      style: primaryTextStyle.copyWith(
                        fontSize: 14.sp,
                        fontWeight: semiBold,
                      ),
                    ),
                    SizedBox(
                      height: 10.sp,
                    ),
                    Text(
                      'Produk Berhasil Di Hapus',
                      style: secondaryTextStyle.copyWith(
                        fontSize: 12.sp,
                      ),
                    ),
                    SizedBox(
                      height: 10.sp,
                    ),
                    SizedBox(
                      width: 20.w,
                      height: 30.sp,
                      child: TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        style: TextButton.styleFrom(
                          backgroundColor: primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.sp),
                          ),
                        ),
                        child: Text(
                          'OK',
                          style: primaryTextStyle.copyWith(
                            fontSize: 12.sp,
                            fontWeight: medium,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: secondaryColor,
            content: Text(
              eror.message.toString(),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 12.sp),
            ),
          ),
        );
      }
    }

    Future<void> editQty(int qty) async {
      ErrorModel eror = await Provider.of<UpdateKeranjangProvider>(
        context,
        listen: false,
      ).update(
        token: token.toString(),
        idProduk: widget.keranjang.id.toString(),
        apikey: keyApi,
        idKonsumen: idKonsumen.toString(),
        qty: qty.toString(),
      );

      if (eror.statusCode.toString() == '200') {
        await Provider.of<DetailKeranjangProvider>(context, listen: false)
            .getDetailKeranjang(
          token: token.toString(),
          apikey: keyApi,
          idKonsumen: idKonsumen.toString(),
        );
        Navigator.pushNamed(context, '/cart');
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: secondaryColor,
            content: Text(
              eror.message.toString(),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 12.sp),
            ),
          ),
        );
      }
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.sp),
        ),
        elevation: 2,
        color: backgroundColor1,
        child: Container(
          margin: EdgeInsets.only(
            top: 19.sp,
          ),
          padding: EdgeInsets.symmetric(
            horizontal: 14.sp,
            vertical: 9.sp,
          ),
          decoration: BoxDecoration(
            color: backgroundColor1,
            borderRadius: BorderRadius.circular(12.sp),
          ),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    width: 14.33.w,
                    height: 43.sp,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.sp),
                      image: DecorationImage(
                        image: NetworkImage(
                          '${urlFile}foto_produk/${widget.keranjang.gambar.toString().split(';').first}',
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 3.w,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.keranjang.namaProduk.toString(),
                          // 1.w.toString(),
                          style: primaryTextStyle.copyWith(
                              fontWeight: semiBold, fontSize: 10.sp),
                        ),
                        Text(
                          'Rp.${rupiah(widget.keranjang.hargaJual.toString())}',
                          style: priceTextStyle.copyWith(
                            fontSize: 9.sp,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10.sp,
              ),
              Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      handleRemove();
                    },
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/icon_remove.png',
                          width: 2.5.w,
                        ),
                        SizedBox(
                          width: 0.6.w,
                        ),
                        Text(
                          'Remove',
                          style: alertTextStyle.copyWith(
                            fontSize: 9.sp,
                            fontWeight: light,
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  GestureDetector(
                    onTap: () {
                      var qty = int.parse(qtyController.text) - 1;
                      editQty(qty);
                    },
                    child: SizedBox(
                      width: 18.sp,
                      child: Image.asset(
                        'assets/button_min.png',
                        width: 5.w,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 1.w,
                  ),
                  SizedBox(
                    width: 25.w,
                    child: TextFormField(
                      onEditingComplete: () {
                        var qty = int.parse(qtyController.text);
                        editQty(qty);
                        FocusScope.of(context).requestFocus(FocusNode());
                        // print(' Qty: ${qtyController.text.toString()}');
                      },
                      textAlign: TextAlign.center,
                      controller: qtyController,
                      style: primaryTextStyle.copyWith(fontSize: 10.sp),
                      decoration: InputDecoration.collapsed(
                        hintText: 'Qty',
                        hintStyle: subtitleTextStyle.copyWith(fontSize: 10.sp),
                        border: OutlineInputBorder(
                          // borderSide: const BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.sp),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 1.w,
                  ),
                  GestureDetector(
                    onTap: () {
                      var qty = int.parse(qtyController.text) + 1;
                      editQty(qty);
                    },
                    child: SizedBox(
                      width: 18.sp,
                      child: Image.asset(
                        'assets/button_add.png',
                        width: 5.w,
                      ),
                    ),
                  ),
                ],
              ),
              cekStok(),
            ],
          ),
        ),
      );
    });
  }
}
