import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/global_model.dart';
import 'package:topsonia_mobile/provider/alamat_penerima_provider.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/detail_reseller_provider.dart';
import 'package:topsonia_mobile/provider/kurirdata_provider.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:topsonia_mobile/widgets/checkout_card.dart';

class ResellerList extends StatefulWidget {
  const ResellerList({Key? key}) : super(key: key);

  @override
  State<ResellerList> createState() => _ResellerListState();
}

class _ResellerListState extends State<ResellerList> {
  String? idKonsumen;
  String? namaKonsumen;
  String? username;
  String? token;
  bool isLoading = false;

  @override
  void initState() {
    validate().whenComplete(() => null);
    super.initState();
  }

  Future validate() async {
    final SharedPreferences session = await SharedPreferences.getInstance();
    var idK = session.getString('idKonsumen');
    var nama = session.getString('namaKonsumen');
    var usern = session.getString('username');
    var tkn = session.getString('token');

    setState(() {
      idKonsumen = idK!;
      namaKonsumen = nama!;
      username = usern!;
      token = tkn!;
    });
  }

  @override
  Widget build(BuildContext context) {
    DetailResellerProvider detailReseller =
        Provider.of<DetailResellerProvider>(context);

    final keranjangProvider =
        Provider.of<DetailKeranjangProvider>(context).detailKeranjang;
    final penerima =
        Provider.of<AlamatPenerimaProvider>(context).alamatPenerima;
    KurirDataProvider kurir = Provider.of<KurirDataProvider>(context);

    Widget showKurir(service, serviceCode, tarif, etd, diskonTarif) {
      return Card(
        elevation: 2,
        margin: EdgeInsets.only(bottom: 12.sp),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.sp),
        ),
        // padding: EdgeInsets.only(bottom: 12.sp),
        child: Container(
          padding: EdgeInsets.all(8.sp),
          decoration: BoxDecoration(
            color: backgroundColor1,
            borderRadius: BorderRadius.circular(10.sp),
          ),
          child: Row(
            children: [
              SizedBox(
                width: 9.sp,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      service,
                      style: primaryTextStyle.copyWith(
                        fontWeight: medium,
                        fontSize: 12.sp,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(
                      height: 1.sp,
                    ),
                    Text(
                      'Tarif : Rp.${rupiah(tarif)}',
                      style: priceTextStyle.copyWith(
                        fontSize: 10.sp,
                      ),
                    ),
                    int.parse(diskonTarif) > 0
                        ? Text(
                            'Diskon Rp.${rupiah(diskonTarif)}',
                            style: priceTextStyle.copyWith(
                              color: Colors.red,
                              fontSize: 9.sp,
                            ),
                          )
                        : const SizedBox(),
                  ],
                ),
              ),
              SizedBox(
                width: 7.sp,
              ),
              Text(
                'Estimasi $etd hari',
                style: secondaryTextStyle.copyWith(
                  fontSize: 9.sp,
                ),
              ),
              SizedBox(
                height: 9.sp,
              ),
            ],
          ),
        ),
      );
    }

    Future<void> pengiriman(
      berat,
      kotaPengirim,
      idReseller,
      totalBelanja,
    ) async {
      setState(() {
        isLoading = true;
      });
      if (await kurir.kurir(
        apikey: keyApi,
        kotaPenerima: penerima.idKota.toString(),
        kotaPengirim: kotaPengirim,
        berat: berat,
        idKonsumen: idKonsumen.toString(),
        totalBelanja: totalBelanja.toString(),
      )) {
        setState(() {
          isLoading = false;
        });
        return showDialog(
          context: context,
          builder: (BuildContext context) => Container(
            width: 20.w,
            child: AlertDialog(
              backgroundColor: backgroundColor2,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.sp),
              ),
              content: SingleChildScrollView(
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: SizedBox(
                          width: 40.sp,
                          child: Icon(
                            Icons.close,
                            size: 15.sp,
                            color: primaryTextColor,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10.sp),
                      padding: EdgeInsets.symmetric(
                        vertical: 3.sp,
                        horizontal: 12.sp,
                      ),
                      child: Column(
                        children: kurir.kurirData
                            .map((e) => GestureDetector(
                                  onTap: () {
                                    final ganti = detailReseller.detailReseller
                                        .firstWhere(
                                            (se) => se.id == idReseller);
                                    setState(() {
                                      ganti.service = e.service.toString();
                                      ganti.serviceCode =
                                          e.serviceCode.toString();
                                      ganti.tarif = e.price.toString();
                                      ganti.diskonTarif =
                                          e.diskonPrice.toString();
                                      ganti.etd = e.etd.toString();
                                      // payment();
                                    });

                                    final ganti2 =
                                        detailReseller.detailReseller;

                                    int jmlS = ganti2.toList().length;
                                    setState(() {
                                      for (int i = 0; i < jmlS; i++) {
                                        ganti2[i].biayaAdmin = '';
                                        ganti2[i].pembayaran = '';
                                      }
                                    });

                                    Navigator.pop(context);
                                    Navigator.pushNamed(context, '/checkout');
                                  },
                                  child: showKurir(
                                    e.service.toString(),
                                    e.serviceCode.toString(),
                                    e.price.toString(),
                                    e.etd.toString(),
                                    e.diskonPrice.toString(),
                                  ),
                                ))
                            .toList(),
                      ),
                    ),
                    SizedBox(
                      height: 10.sp,
                    ),
                    SizedBox(
                      width: 300.sp,
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: secondaryColor,
            content: Text(
              'Periksa Koneksi Internet Anda',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 12.sp),
            ),
          ),
        );
        setState(() {
          isLoading = false;
        });
      }
    }

    Widget address(
      nama,
      id,
      kota,
      kotaid,
      berat,
      service,
      tarif,
      etd,
      totalBelanja,
      tarifDiskon,
    ) {
      return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.sp),
        ),
        margin: EdgeInsets.only(
          top: 5.sp,
          bottom: 3.sp,
        ),
        elevation: 1,
        child: Container(
          padding: EdgeInsets.all(10.sp),
          decoration: BoxDecoration(
            color: backgroundColor2,
            borderRadius: BorderRadius.circular(10.sp),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 3.sp),
              Row(
                children: [
                  SizedBox(
                    width: 2.w,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 2.sp,
                      ),
                      isLoading
                          ? const Text('loading')
                          : service == ''
                              ? SizedBox(
                                  height: 25.sp,
                                  width: 78.sp,
                                  child: TextButton(
                                    onPressed: () {
                                      pengiriman(
                                          kotaid.toString(),
                                          berat.toString(),
                                          id.toString(),
                                          totalBelanja);
                                    },
                                    style: TextButton.styleFrom(
                                      backgroundColor: primaryColor,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(8.sp),
                                      ),
                                    ),
                                    child: Text(
                                      'Pilih Pengiriman',
                                      style: primaryTextStyle.copyWith(
                                        fontWeight: semiBold,
                                        fontSize: 8.sp,
                                      ),
                                    ),
                                  ),
                                )
                              : GestureDetector(
                                  onTap: () {
                                    pengiriman(
                                        kotaid.toString(),
                                        berat.toString(),
                                        id.toString(),
                                        totalBelanja);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.only(bottom: 12.sp),
                                    child: Container(
                                      padding: EdgeInsets.all(8.sp),
                                      decoration: BoxDecoration(
                                        color: backgroundColor2,
                                        borderRadius:
                                            BorderRadius.circular(10.sp),
                                      ),
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            width: 9.sp,
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Pengiriman",
                                                style:
                                                    primaryTextStyle.copyWith(
                                                  fontWeight: medium,
                                                  fontSize: 11.sp,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 2.sp,
                                              ),
                                              Text(
                                                service,
                                                style:
                                                    primaryTextStyle.copyWith(
                                                  fontWeight: medium,
                                                  fontSize: 11.sp,
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                              SizedBox(
                                                height: 2.sp,
                                              ),
                                              Text(
                                                'Ongkir : Rp.${rupiah(tarif)}',
                                                style: priceTextStyle.copyWith(
                                                  fontSize: 9.sp,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 1.sp,
                                              ),
                                              Text(
                                                'Diskon Ongkir :Rp.${rupiah(tarifDiskon)}',
                                                style: priceTextStyle.copyWith(
                                                  fontSize: 8.sp,
                                                  color: Colors.red,
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            width: 7.sp,
                                          ),
                                          Text(
                                            'Estimasi $etd hari',
                                            style: secondaryTextStyle.copyWith(
                                              fontSize: 8.sp,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 9.sp,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      );
    }

    Widget item() {
      return Card(
        color: backgroundColor1,
        elevation: 2,
        margin: EdgeInsets.only(
          top: 15.sp,
        ),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: detailReseller.detailReseller
                .map(
                  (reseller) => Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: backgroundColor1,
                          borderRadius: BorderRadius.circular(10.sp),
                        ),
                        padding: EdgeInsets.only(top: 20.sp),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 12.sp),
                              child: Text(
                                reseller.namaReseller.toString(),
                                textAlign: TextAlign.start,
                                style: primaryTextStyle.copyWith(
                                  fontSize: 11.sp,
                                  fontWeight: medium,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 12.sp),
                              child: Text(
                                reseller.kota.toString(),
                                textAlign: TextAlign.left,
                                style: primaryTextStyle.copyWith(
                                  fontSize: 8.sp,
                                  fontWeight: medium,
                                ),
                              ),
                            ),
                            Column(
                                children: keranjangProvider
                                    .where((x) => x.idReseller!
                                        .toLowerCase()
                                        .contains(reseller.id
                                            .toString()
                                            .toLowerCase()))
                                    .map(
                                      (e) => CheckoutCard(e),
                                    )
                                    .toList()),
                            address(
                              reseller.namaReseller,
                              reseller.id,
                              reseller.kota,
                              reseller.berat,
                              reseller.kotaId,
                              reseller.service.toString(),
                              reseller.tarif,
                              reseller.etd,
                              reseller.totalBelanja,
                              reseller.diskonTarif,
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 16.sp),
                              child: Text(
                                'Subtotal : Rp. ${rupiah(int.parse(reseller.totalBelanja.toString()) + int.parse(reseller.tarif == '' ? '0' : reseller.tarif.toString()) + int.parse(reseller.diskonTarif == '' ? '0' : reseller.diskonTarif.toString()))}',
                                textAlign: TextAlign.start,
                                style: priceTextStyle.copyWith(
                                  fontSize: 11.sp,
                                  fontWeight: medium,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20.sp,
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20.sp,
                      )
                    ],
                  ),
                )
                .toList(),
          ),
        ),
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Container(
        child: item(),
      );
    });
  }
}
