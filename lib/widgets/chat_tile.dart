import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/theme.dart';

class ChatTile extends StatelessWidget {
  const ChatTile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, '/detail-chat');
        },
        child: Container(
          margin: EdgeInsets.only(top: 23.sp),
          child: Column(
            children: [
              Row(
                children: [
                  Image.asset(
                    'assets/image_shop_logo.png',
                    width: 18.w,
                  ),
                  SizedBox(
                    width: 4.w,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Shoe Store',
                          style: primaryTextStyle.copyWith(
                            fontSize: 17.sp,
                          ),
                        ),
                        Text(
                          'Hello Can I Help You ?',
                          style: secondaryTextStyle.copyWith(
                            fontWeight: light,
                            fontSize: 13.sp,
                          ),
                          overflow: TextOverflow.ellipsis,
                        )
                      ],
                    ),
                  ),
                  Text(
                    'Now',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 10.sp,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.sp,
              ),
              Divider(
                thickness: 2.sp,
                color: const Color(0xff2B2939),
              ),
            ],
          ),
        ),
      );
    });
  }
}
