// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/category_product_model.dart';
import 'package:topsonia_mobile/theme.dart';

class CategoryProductTile extends StatelessWidget {
  final CategoryProductModel category;
  const CategoryProductTile(this.category, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return GestureDetector(
        onTap: () {},
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 6.sp),
          margin: EdgeInsets.only(right: 10.sp),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.sp), color: primaryColor),
          child: Text(
            'All Shoes',
            style:
                primaryTextStyle.copyWith(fontSize: 10.sp, fontWeight: medium),
          ),
        ),
      );
    });
  }
}
