import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/global_model.dart';
import 'package:topsonia_mobile/models/orders_report_model.dart';
import 'package:topsonia_mobile/theme.dart';

class OrdersReportCard extends StatelessWidget {
  final OrdersReportModel orders;
  final int index;
  const OrdersReportCard(this.orders, this.index, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.sp),
        ),
        margin: EdgeInsets.only(top: 18.sp),
        elevation: 2,
        child: Container(
          padding: EdgeInsets.only(
            top: 11.sp,
            left: 11.sp,
            bottom: 13.sp,
            right: 18.sp,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12.sp),
            color: backgroundColor1,
          ),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(12.sp),
                child: Image.network(
                  '${urlFile}foto_produk/${orders.gambar.toString().split(';').first}',
                  errorBuilder: (context, error, stackTrace) {
                    return Image.asset(
                      'assets/no_image.png',
                      width: 19.1.w,
                    );
                  },
                  width: 19.1.w,
                ),
              ),
              SizedBox(
                width: 4.w,
              ),
              // Expanded(
              //   child:
              SizedBox(
                width: 135.sp,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      index == 6 || index == 8
                          ? orders.kodeTransaksi.toString()
                          : orders.namaReseller.toString(),
                      style: primaryTextStyle.copyWith(
                        fontWeight: semiBold,
                        fontSize: 11.sp,
                      ),
                    ),
                    Text(
                      'Total Belanja : Rp. ${rupiah(orders.totalBelanja)}',
                      style: priceTextStyle.copyWith(
                        fontSize: 10.sp,
                      ),
                    ),
                    Text(
                      'Waktu Order : ${orders.waktuOrder}',
                      style: priceTextStyle.copyWith(
                        fontSize: 9.sp,
                      ),
                    ),
                  ],
                ),
              ),
              // ),
              Expanded(
                child: Text(
                  orders.status.toString(),
                  style: primaryTextStyle.copyWith(
                    fontSize: 10.sp,
                  ),
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}
