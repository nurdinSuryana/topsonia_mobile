import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/detail_keranjang_model.dart';
import 'package:topsonia_mobile/models/global_model.dart';
import 'package:topsonia_mobile/theme.dart';

class CheckoutCard extends StatelessWidget {
  final DetailKeranjangModel keranjang;
  const CheckoutCard(this.keranjang, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.sp),
        ),
        elevation: 1,
        margin: EdgeInsets.only(top: 10.sp),
        child: Container(
          padding: EdgeInsets.symmetric(
            vertical: 3.sp,
            horizontal: 12.sp,
          ),
          decoration: BoxDecoration(
            color: backgroundColor2,
            borderRadius: BorderRadius.circular(10.sp),
          ),
          child: Row(
            children: [
              Container(
                width: 15.w,
                height: 45.sp,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.sp),
                  image: DecorationImage(
                    image: NetworkImage(
                      '${urlFile}foto_produk/${keranjang.gambar.toString().split(';').first}',
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 9.sp,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      keranjang.namaProduk.toString(),
                      style: primaryTextStyle.copyWith(
                        fontWeight: semiBold,
                        fontSize: 10.sp,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(
                      height: 2.sp,
                    ),
                    Text(
                      'Rp.${rupiah(keranjang.hargaJual.toString())}',
                      style: priceTextStyle.copyWith(
                        fontSize: 9.sp,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 9.sp,
              ),
              Text(
                "${keranjang.jumlah.toString()} x",
                style: secondaryTextStyle.copyWith(
                  fontSize: 8.5.sp,
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
