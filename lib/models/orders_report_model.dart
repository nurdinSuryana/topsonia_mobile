class OrdersReportModel {
  String? idPenjualan;
  String? kodeTransaksi;
  String? totalBelanja;
  String? diskonBelanja;
  String? cashbackBelanja;
  String? potonganOngkir;
  String? ongkir;
  String? mdr;
  String? totalBayar;
  String? pembayaran;
  String? namaReseller;
  String? kota;
  String? waktuOrder;
  String? status;
  String? gambar;
  String? nomor;
  String? idAlamat;
  String? awb;

  OrdersReportModel({
    this.idPenjualan,
    this.kodeTransaksi,
    this.totalBelanja,
    this.ongkir,
    this.mdr,
    this.totalBayar,
    this.pembayaran,
    this.namaReseller,
    this.kota,
    this.waktuOrder,
    this.status,
    this.gambar,
    this.nomor,
    this.idAlamat,
    this.cashbackBelanja,
    this.diskonBelanja,
    this.potonganOngkir,
    this.awb,
  });

  OrdersReportModel.fromJson(Map<String, dynamic> json) {
    idPenjualan = json['id_penjualan'];
    kodeTransaksi = json['kode_transaksi'];
    totalBelanja = json['total_belanja'];
    ongkir = json['ongkir'];
    mdr = json['mdr'];
    totalBayar = json['total_bayar'];
    pembayaran = json['pembayaran'];
    namaReseller = json['nama_reseller'];
    kota = json['nama_kota'];
    waktuOrder = json['waktu_order'];
    status = json['status'];
    gambar = json['gambar'];
    nomor = json['nomor'];
    idAlamat = json['id_alamat'];
    cashbackBelanja = json['cashback'];
    diskonBelanja = json['diskon_buyer'];
    potonganOngkir = json['diskon_ongkir'];
    awb = json['awb'];
  }

  Map<String, dynamic> toJson() {
    return {
      'idPenjualan': idPenjualan,
      'kodeTransaksi': kodeTransaksi,
      'totalBelanja': totalBelanja,
      'ongkir': ongkir,
      'mdr': mdr,
      'totalBayar': totalBayar,
      'pembayaran': pembayaran,
      'namaReseller': namaReseller,
      'waktuOrder': waktuOrder,
      'status': status,
      'gambar': gambar,
      'nomor': nomor,
      'idAlamat': idAlamat,
      'cashbackBelanja': cashbackBelanja,
      'diskonBelanja': diskonBelanja,
      'potonganOngkir': potonganOngkir,
      'kota': kota,
      'awb': awb,
    };
  }
}

class UninitializedOrdersReportModel extends OrdersReportModel {}
