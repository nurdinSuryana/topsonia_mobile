class TrackingModel {
  String? id;
  String? refNumber;
  String? status;
  String? timeStamp;
  String? relationName;
  String? recipient;
  String? reasonCode;
  String? reasonNote;
  String? comment;
  String? station;
  String? lastStatus;
  String? nomor;

  TrackingModel({
    this.id,
    this.refNumber,
    this.status,
    this.reasonCode,
    this.reasonNote,
    this.comment,
    this.station,
    this.lastStatus,
    this.relationName,
    this.timeStamp,
    this.recipient,
    this.nomor,
  });

  TrackingModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    refNumber = json['refNumber'];
    reasonCode = json['reasonCode'];
    reasonNote = json['reasonNote'];
    comment = json['comment'];
    station = json['station'];
    lastStatus = json['lastStatus'];
    status = json['status'];
    relationName = json['relationName'];
    timeStamp = json['timeStamp'];
    recipient = json['recipient'];
    nomor = json['nomor'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'refNumber': refNumber,
      'reasonCode': reasonCode,
      'reasonNote': reasonNote,
      'comment': comment,
      'station': station,
      'lastStatus': lastStatus,
      'status': status,
      'relationName': relationName,
      'timeStamp': timeStamp,
      'recipient': recipient,
      'nomor': nomor,
    };
  }
}

class UninitializedTrackingModel extends TrackingModel {}
