class CategoryProductModel {
  String? id;
  String? namaCategory;

  CategoryProductModel({
    this.id,
    this.namaCategory,
  });

  CategoryProductModel.fromJson(Map<String, dynamic> json) {
    id = json['id_kategori_produk'];
    namaCategory = json['nama_kategori'];
  }

  CategoryProductModel.all() {
    id = '0';
    namaCategory = 'Semua Kategori';
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'namaCategory': namaCategory,
    };
  }
}

class UninitializedCategoryProductModel extends CategoryProductModel {}
