class KurirDataModel {
  String? price;
  String? diskonPrice;
  String? service;
  String? serviceCode;
  String? etd;

  KurirDataModel({
    this.price,
    this.service,
    this.serviceCode,
    this.etd,
    this.diskonPrice,
  });

  KurirDataModel.fromJson(Map<String, dynamic> json) {
    price = json['Price'];
    diskonPrice = json['diskon'];
    service = json['Service'];
    serviceCode = json['ServiceCode'];
    etd = json['Etd'];
  }

  KurirDataModel.kosong() {
    price = '';
    service = '';
    serviceCode = '';
    etd = '';
    diskonPrice = '';
  }

  Map<String, dynamic> toJson() {
    return {
      'price': price,
      'service': service,
      'serviceCode': serviceCode,
      'etd': etd,
      'diskonPrice': diskonPrice,
    };
  }
}

class UninitializedKurirDataModel extends KurirDataModel {}
