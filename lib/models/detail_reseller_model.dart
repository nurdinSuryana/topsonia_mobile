class DetailResellerModel {
  String? id;
  String? namaReseller;
  String? idAlamatReseller;
  String? kota;
  String? kotaId;
  String? berat;
  String? service;
  String? serviceCode;
  String? tarif;
  String? diskonTarif;
  String? etd;
  String? pembayaran;
  String? biayaAdmin;
  String? cashback;
  String? diskon;
  String? komisiTopsonia;
  String? totalBelanja;

  DetailResellerModel({
    this.id,
    this.namaReseller,
    this.kota,
    this.kotaId,
    this.berat,
    this.service,
    this.serviceCode,
    this.tarif,
    this.diskonTarif,
    this.etd,
    this.pembayaran,
    this.biayaAdmin,
    this.idAlamatReseller,
    this.cashback,
    this.diskon,
    this.komisiTopsonia,
    this.totalBelanja,
  });

  DetailResellerModel.fromJson(Map<String, dynamic> json) {
    id = json['id_reseller'];
    namaReseller = json['nama_reseller'];
    idAlamatReseller = json['id_alamat_reseller'];
    kota = json['nama_kota'];
    kotaId = json['kota_id'];
    berat = json['total_berat'];
    cashback = json['cashback'];
    diskon = json['diskon'];
    komisiTopsonia = json['komisi_topsonia'];
    totalBelanja = json['total_belanja'];
    service = '';
    serviceCode = '';
    tarif = '';
    diskonTarif = '';
    etd = '';
    pembayaran = '';
    biayaAdmin = '';
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'namaReseller': namaReseller,
      'kota': kota,
      'kotaId': kotaId,
      'berat': berat,
      'service': service,
      'serviceCode': serviceCode,
      'tarif': tarif,
      'diskonTarif': diskonTarif,
      'etd': etd,
      'pembayaran': pembayaran,
      'biayaAdmin': biayaAdmin,
      'idAlamatReseller': idAlamatReseller,
      'cashback': cashback,
      'diskon': diskon,
      'komisiTopsonia': komisiTopsonia,
      'totalBelanja': totalBelanja,
    };
  }
}

class UninitializedDetailResellerModel extends DetailResellerModel {}
