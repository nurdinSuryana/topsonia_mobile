class IntruksiPaymentModel {
  String? id;
  String? via;
  String? intruksi;

  IntruksiPaymentModel({
    this.id,
    this.intruksi,
    this.via,
  });

  IntruksiPaymentModel.fromJson(Map<String, dynamic> json) {
    id = json['id_intruksi_payment'];
    via = json['via'];
    intruksi = json['intruksi'];
  }

  IntruksiPaymentModel.kosong() {
    id = '';
    intruksi = '';
    via = '';
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'intruksi': intruksi,
      'via': via,
    };
  }
}

class UninitializedIntruksiPaymentModel extends IntruksiPaymentModel {}
