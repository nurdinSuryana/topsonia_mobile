class PaymentModel {
  String? id;
  String? jenisPayment;
  String? namaPayment;
  String? valuePayment;
  String? mdr;
  String? status;
  String? logo;

  PaymentModel({
    this.id,
    this.jenisPayment,
    this.namaPayment,
    this.valuePayment,
    this.mdr,
    this.status,
    this.logo,
  });

  PaymentModel.fromJson(Map<String, dynamic> json) {
    id = json['id_payment'];
    jenisPayment = json['jenis_payment'];
    namaPayment = json['nama_payment'];
    valuePayment = json['value_payment'];
    mdr = json['mdr'];
    status = json['status'];
    logo = json['logo'];
  }

  PaymentModel.kosong() {
    id = '';
    jenisPayment = '';
    namaPayment = '';
    valuePayment = '';
    mdr = '';
    status = '';
    logo = '';
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'jenisPayment': jenisPayment,
      'namaPayment': namaPayment,
      'valuePayment': valuePayment,
      'mdr': mdr,
      'status': status,
    };
  }
}

class UninitializedPaymentModel extends PaymentModel {}
