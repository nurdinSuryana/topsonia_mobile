class DetailKeranjangModel {
  String? id;
  String? idPenjualanDetail;
  String? namaProduk;
  String? namaReseller;
  String? idReseller;
  String? diskon;
  String? hargaJual;
  String? jumlah;
  String? satuan;
  String? berat;
  String? gambar;
  String? stok;
  String? rating;
  String? ulasan;

  DetailKeranjangModel({
    this.id,
    this.idPenjualanDetail,
    this.namaProduk,
    this.diskon,
    this.hargaJual,
    this.jumlah,
    this.satuan,
    this.namaReseller,
    this.idReseller,
    this.berat,
    this.gambar,
    this.stok,
    this.rating,
    this.ulasan,
  });

  DetailKeranjangModel.fromJson(Map<String, dynamic> json) {
    id = json['id_produk'];
    namaProduk = json['nama_produk'];
    namaReseller = json['nama_reseller'];
    idReseller = json['id_reseller'];
    diskon = json['harga_konsumen'];
    hargaJual = json['harga_jual'];
    jumlah = json['jumlah'];
    satuan = json['satuan'];
    berat = json['berat'];
    gambar = json['gambar'];
    stok = json['stock'];
    idPenjualanDetail = json['id_penjualan_detail'];
    rating = json['rating'];
    ulasan = json['ulasan'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'idPenjualanDetail': idPenjualanDetail,
      'namaProduk': namaProduk,
      'namaReseller': namaReseller,
      'diskon': diskon,
      'hargaJual': hargaJual,
      'jumlah': jumlah,
      'satuan': satuan,
      'berat': berat,
      'gambar': gambar,
      'stok': stok,
      'idReseller': idReseller,
      'rating': rating,
      'ulasan': ulasan,
    };
  }
}

class UninitializedDetailKeranjangModel extends DetailKeranjangModel {}
