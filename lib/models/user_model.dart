class UserModel {
  String? username;
  String? nama;
  String? token;
  String? idKonsumen;

  UserModel({
    this.username,
    this.nama,
    this.token,
    this.idKonsumen,
  });

  UserModel.fromJson(Map<String, dynamic> json) {
    username = json['user']['username'];
    nama = json['user']['nama_lengkap'];
    idKonsumen = json['user']['id_konsumen'];
    token = json['token'];
  }

  UserModel.kosong() {
    username = '';
    nama = '';
    idKonsumen = '';
    token = '';
  }

  Map<String, dynamic> toJson() {
    return {
      'username': username,
      'nama': nama,
      'token': token,
      'idKonsumen': idKonsumen,
    };
  }
}

class UninitializedUserModel extends UserModel {}
