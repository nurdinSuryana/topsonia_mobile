class ErrorModel {
  late int statusCode;
  late String message;

  ErrorModel({
    required this.statusCode,
    required this.message,
  });

  ErrorModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
  }

  ErrorModel.error() {
    statusCode = 7000;
    message = 'Periksa Koneksi Internet Anda !';
  }

  Map<String, dynamic> toJson() {
    return {
      'statusCode': statusCode,
      'message': message,
    };
  }
}
