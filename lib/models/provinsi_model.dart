class ProvinsiModel {
  String? idProvinsi;
  String? namaProvinsi;
  String? idKota;
  String? namaKota;

  ProvinsiModel({
    this.idProvinsi,
    this.namaProvinsi,
    this.idKota,
    this.namaKota,
  });

  ProvinsiModel.fromJson(Map<String, dynamic> json) {
    idProvinsi = json['provinsi_id'];
    namaProvinsi = json['nama_provinsi'];
    idKota = json['kota_id'];
    namaKota = json['nama_kota'];
  }

  ProvinsiModel.kosong() {
    idProvinsi = '';
    namaProvinsi = '';
    idKota = '';
    namaKota = '';
  }

  Map<String, dynamic> toJson() {
    return {
      'idProvinsi': idProvinsi,
      'namaProvinsi': namaProvinsi,
      'idKota': idKota,
      'namaKota': namaKota,
    };
  }
}

class UninitializedProvinsiModel extends ProvinsiModel {}
