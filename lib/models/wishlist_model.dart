class WishlistModel {
  String? idWishlist;
  String? idProduk;
  String? namaProduk;
  String? harga;
  String? gambar;

  WishlistModel({
    this.idWishlist,
    this.idProduk,
    this.namaProduk,
    this.harga,
    this.gambar,
  });

  WishlistModel.fromJson(Map<String, dynamic> json) {
    idWishlist = json['id_favorite'];
    idProduk = json['id_produk'];
    namaProduk = json['nama_produk'];
    harga = json['harga_konsumen'];
    gambar = json['gambar'];
  }

  WishlistModel.kosong() {
    idWishlist = '';
    idProduk = '';
    namaProduk = '';
    harga = '';
    gambar = '';
  }

  Map<String, dynamic> toJson() {
    return {
      'idWishlist': idWishlist,
      'idProduk': idProduk,
      'namaProduk': namaProduk,
      'harga': harga,
      'gambar': gambar,
    };
  }
}

class UninitializedWishlistModel extends WishlistModel {}
