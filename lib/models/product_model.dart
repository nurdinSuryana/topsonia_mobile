class ProductModel {
  String? id;
  String? namaProduk;
  String? gambar;
  String? hargaProduk;
  String? namaKota;
  String? stock;
  String? terjual;
  String? keterangan;
  String? berat;
  String? kategori;
  String? satuan;
  String? rating;

  ProductModel({
    this.id,
    this.namaProduk,
    this.hargaProduk,
    this.namaKota,
    this.stock,
    this.terjual,
    this.gambar,
    this.keterangan,
    this.berat,
    this.kategori,
    this.satuan,
    this.rating,
  });

  ProductModel.fromJson(Map<String, dynamic> json) {
    id = json['id_produk'];
    namaProduk = json['nama_produk'];
    gambar = json['gambar'];
    hargaProduk = json['harga_konsumen'];
    namaKota = json['nama_kota'];
    stock = json['stock'];
    terjual = json['terjual'];
    keterangan = json['keterangan'];
    berat = json['berat'];
    kategori = json['nama_kategori'];
    satuan = json['satuan'];
    rating = json['rating'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'namaProduk': namaProduk,
      'gambar': gambar,
      'hargaProduk': hargaProduk,
      'namaKota': namaKota,
      'stock': stock,
      'terjual': terjual,
      'keterangan': keterangan,
      'berat': berat,
      'kategori': kategori,
      'satuan': satuan,
      'rating': rating,
    };
  }
}

class UninitializedProductModel extends ProductModel {}
