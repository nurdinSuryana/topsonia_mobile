import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

rupiah(nominal) {
  final rupiah = NumberFormat('#,##0', 'ID');
  if (nominal is int) {
    return rupiah.format(nominal);
  } else {
    return rupiah.format(int.parse(nominal));
  }
}

tinggi(context, tinggi) {
  return MediaQuery.of(context).size.height / tinggi;
}

lebar(context, lebar) {
  return MediaQuery.of(context).size.width / lebar;
}

// tinggi(context, tinggi) {
//   return (((MediaQuery.of(context).size.height / 100) /
//           (MediaQuery.of(context).size.height / 100)) *
//       tinggi);
// }

// lebar(context, lebar) {
//   return (((MediaQuery.of(context).size.width / 100) /
//           (MediaQuery.of(context).size.width / 100)) *
//       lebar);
// }
