class AlamatPenerimaModel {
  String? idAlamat;
  String? namaAlamat;
  String? pic;
  String? alamatLengkap;
  String? idKota;

  AlamatPenerimaModel(
      {this.idAlamat,
      this.namaAlamat,
      this.pic,
      this.alamatLengkap,
      this.idKota});

  AlamatPenerimaModel.fromJson(Map<String, dynamic> json) {
    idAlamat = json['id_alamat'];
    namaAlamat = json['nama_alamat'];
    pic = json['pic'];
    alamatLengkap = json['alamat_lengkap'];
    idKota = json['kota_id'];
  }

  AlamatPenerimaModel.kosong() {
    idAlamat = '';
    namaAlamat = '';
    pic = '';
    alamatLengkap = '';
    idKota = '';
  }

  Map<String, dynamic> toJson() {
    return {
      'idAlamat': idAlamat,
      'namaAlamat': namaAlamat,
      'pic': pic,
      'alamatLengkap': alamatLengkap,
      'idKota': idKota,
    };
  }
}

class UninitializedAlamatPenerimaModel extends AlamatPenerimaModel {}
