import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

double defaultMargin = 30.0;
// String urlApi = 'http://172.17.0.165/topsonia_web/restapi/';
// String urlFile = 'http://172.17.0.165/topsonia_web/asset/';
String urlApi = 'https://topsonia.com/sandbox_it/restapi/';
String urlFile = 'https://topsonia.com/sandbox_it/asset/';
String keyApi = 'wpu123';

Color primaryColor = const Color(0xfffb8c00);
Color secondaryColor = const Color(0xff38ABBE);
Color alertColor = const Color(0xffED6363);
Color priceColor = const Color(0xff00bcd4);

// Color topsoniaColor = Const Color();

Color backgroundColor1 = const Color(0xffECEDEF);
Color backgroundColor2 = const Color(0xffF1F0F2);
Color backgroundColor3 = const Color(0xff999999);
Color backgroundColor4 = const Color(0xff504F5E);
Color backgroundColor5 = const Color(0xff2E2E2E);

Color backgroundColor6 = const Color(0xff1F1D2B);
Color primaryTextColor = const Color(0xff2B2B44);
Color secondaryTextColor = const Color(0xff242231);
Color subtitleColor = const Color(0xff252836);
Color transparentColor = Colors.transparent;
Color blackColor = const Color(0xff2B2B44);

// Color backgroundColor1 = const Color(0xff1F1D2B);
// Color backgroundColor2 = const Color(0xff2B2937);
// Color backgroundColor3 = const Color(0xff242231);
// Color backgroundColor4 = const Color(0xff252836);
// Color backgroundColor5 = const Color(0xff2B2B44);
// Color backgroundColor6 = const Color(0xffECEDEF);
// Color primaryTextColor = const Color(0xffF1F0F2);
// Color secondaryTextColor = const Color(0xff999999);
// Color subtitleColor = const Color(0xff504F5E);
// Color transparentColor = Colors.transparent;
// Color blackColor = const Color(0xff2E2E2E);

TextStyle primaryTextStyle = GoogleFonts.poppins(color: primaryTextColor);

TextStyle secondaryTextStyle = GoogleFonts.poppins(color: secondaryTextColor);

TextStyle subtitleTextStyle = GoogleFonts.poppins(color: subtitleColor);

TextStyle priceTextStyle = GoogleFonts.poppins(color: priceColor);

TextStyle purpleTextStyle = GoogleFonts.poppins(color: primaryColor);

TextStyle blackTextStyle = GoogleFonts.poppins(color: blackColor);

TextStyle alertTextStyle = GoogleFonts.poppins(color: alertColor);

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
